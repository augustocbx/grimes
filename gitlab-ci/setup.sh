#!/bin/bash -xe

# Setup ssh key
mkdir -p -m 700 ~/.ssh

#########################
set +x

echo -e "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa

set -x
#########################

chmod 600 ~/.ssh/id_rsa

cat << EOS > ~/.ssh/config
Host *
    StrictHostKeyChecking no
EOS

cat gitlab-ci/database.yml
cp gitlab-ci/database.yml config/database.yml
cp gitlab-ci/redis.yml config/redis.yml
cp gitlab-ci/cache.yml config/cache.yml
cp gitlab-ci/secrets.yml config/secrets.yml
cp gitlab-ci/config.yml config/config.yml
cp gitlab-ci/social_keys.yml config/social_keys.yml

# docker run --name grimes_development --publish=5432:5432 -e POSTGRES_PASSWORD=123456 -e POSTGRES_USER=grimes -d postgres:9.5

# bundle check --path=${BUNDLE_CACHE} || bundle install --path=${BUNDLE_CACHE} --jobs=2 --retry=3
bundle check || bundle install --jobs=2 --retry=3

# bundle exec rake db:create
# bundle exec rake db:migrate:reset