# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

Rails.application.config.assets.precompile += %w(font-awesome/css/font-awesome.css font-awesome/fonts/fontawesome-webfont.woff2 font-awesome/fonts/fontawesome-webfont.woff)

Rails.application.config.assets.precompile += %w(unify-v1.9.1/assets/img/logo2-default.png unify-v1.9.1/assets/css/pages/page_log_reg_v1.css unify-v1.9.1/assets/css/pages/pricing/pricing_v1.css  unify-v1.9.1/assets/css/pages/shortcode_timeline1.css unify-v1.9.1/assets/css/headers/header-default.css unify-v1.9.1/assets/css/footers/footer-v1.css unify-v1.9.1/assets/plugins/animate.css unify-v1.9.1/assets/plugins/line-icons/line-icons.css unify-v1.9.1/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css unify-v1.9.1/assets/plugins/layer-slider/layerslider/css/layerslider.css unify-v1.9.1/assets/plugins/brand-buttons/brand-buttons.css /unify-v1.9.1/assets/plugins/brand-buttons/brand-buttons.css unify-v1.9.1/assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css unify-v1.9.1/assets/plugins/brand-buttons/brand-buttons-inversed.css unify-v1.9.1/assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css unify-v1.9.1/assets/css/pages/profile.css unify-v1.9.1/assets/css/style.css unify-v1.9.1/assets/css/custom.css unify-v1.9.1/assets/css/app.css unify-v1.9.1/E-Commerce/assets/plugins/jquery-steps/build/jquery.steps.js unify-v1.9.1/E-Commerce/assets/js/shop.app unify-v1.9.1/E-Commerce/assets/css/shop.style unify-v1.9.1/E-Commerce/assets/js/plugins/stepWizard unify-v1.9.1/E-Commerce/assets/js/custom unify-v1.9.1/E-Commerce/assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min unify-v1.9.1/E-Commerce/assets/js/plugins/style-switcher unify-v1.9.1/E-Commerce/assets/plugins/jquery-steps/css/custom-jquery.steps.css logo-horizontal2.png)

Rails.application.config.assets.precompile += %w(unify-v1.9.1/assets/img/testimonials/img6.jpg unify-v1.9.1/assets/img/testimonials/img1.jpg unify-v1.9.1/assets/img/main/img22.jpg unify-v1.9.1/E-Commerce/assets/img/patterns/breadcrumbs.png unify-v1.9.1/assets/img/bg/18.jpg unify-v1.9.1/assets/img/patterns/breadcrumbs.png)

Rails.application.config.assets.precompile += %w(shopping_cart/shopping_cart-jquery.steps)
Rails.application.config.assets.precompile += %w(images/no_image.svg images/no_image.png unify-v1.9.1/assets/img/map-img.png)
Rails.application.config.assets.precompile += %w(unify-v1.9.1/assets/plugins/line-icons/fonts/Simple-Line-Icons.woff unify-v1.9.1/assets/plugins/line-icons/fonts/Simple-Line-Icons.eot unify-v1.9.1/assets/plugins/line-icons/fonts/Simple-Line-Icons.svg unify-v1.9.1/assets/plugins/line-icons/fonts/Simple-Line-Icons.ttf)

Rails.application.config.assets.precompile += %w(jquery/dist/jquery.js loadcss/src/loadCSS loadcss/src/cssrelpreload)

Rails.application.config.assets.precompile += %w(memorial_site/application_memorial.scss frontend/application_memorial.js)

Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets')


# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
