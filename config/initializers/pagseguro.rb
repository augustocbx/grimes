# frozen_string_literal: true
PagSeguro.configure do |config|
  if Rails.env.production?
    config.token = '2810DE90A1284C418C2FA0939E287247'
  else
    config.token = '6F43FF13E35B4D58B3FFE6DB492EDE77'
    config.environment = :sandbox
  end

  config.email = 'augustocbx@gmail.com'
  if Rails.env.development?
    OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
  end
end
