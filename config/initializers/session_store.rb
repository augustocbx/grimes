# frozen_string_literal: true
# Be sure to restart your server when you modify this file.

Rails.application.config.session_store :active_record_store, key: '_grimes_app_session', secure: Rails.env.production?
# Rails.application.config.session_store :active_record_store, key: '_grimes_app_session', same_site: true, secure: Rails.env.production?
Rails.application.config.action_controller.session_store = :active_record_store
