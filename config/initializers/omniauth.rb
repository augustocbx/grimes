# frozen_string_literal: true
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, ENV['GOOGLE_KEY'], ENV['GOOGLE_SECRET']
  provider :facebook, ENV['FACEBOOK_KEY'], ENV['FACEBOOK_SECRET'], {
      callback_url: 'https://www.memorialvivo.com.br/users/auth/facebook/callback',
      scope: 'email,public_profile'
  }
end
