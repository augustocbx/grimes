require "google/cloud/firestore"
if !Rails.env.test?
  FIRESTORE = Google::Cloud::Firestore.new(
    project_id: "api-project-1033394134810",
    credentials: File.join(Rails.root, 'config', 'api-project-1033394134810-72f32ed8590e.json').to_s
  )
end
