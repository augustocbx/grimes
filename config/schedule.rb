# frozen_string_literal: true
# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
every 1.day do
  runner "grimes:death_birth"
end
every 12.hour do
  rake "firestore:update_memoriais_force_all"
end
every 1.hour do
  rake "firestore:listen_memorials_changes"
end

# Learn more: http://github.com/javan/whenever
