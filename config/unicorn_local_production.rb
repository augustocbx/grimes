# app_path = "/var/rails_app/grimes/production"

# Fill path to your app
# working_directory "#{app_path}/current"

# Set unicorn options
worker_processes 5
preload_app true
timeout 30
# listen "/tmp/unicorn.grimes-production.sock", backlog: 64

# Spawn unicorn master worker for user apps (group: apps)
# user 'deploy', 'deploy'

# Should be 'production' by default, otherwise use other env
# rails_env 'production'

# Log everything to one file
# stderr_path "log/unicorn.stderr.log"
# stdout_path "log/unicorn.stdout.log"

# Set master PID location
# pid "#{app_path}/shared/tmp/pids/unicorn.pid"

# before_exec do |server|
#   ENV['BUNDLE_GEMFILE'] = "#{app_path}/current/Gemfile"
# end

# before_fork do |server, worker|
#   # the following is highly recomended for Rails + "preload_app true"
#   # as there's no need for the master process to hold a connection
#   if defined?(ActiveRecord::Base)
#     ActiveRecord::Base.connection.disconnect!
#   end
#
#   # Before forking, kill the master process that belongs to the .oldbin PID.
#   # This enables 0 downtime deploys.
#   old_pid = "#{server.config[:pid]}.oldbin"
#   if File.exists?(old_pid) && server.pid != old_pid
#     begin
#       Process.kill("QUIT", File.read(old_pid).to_i)
#     rescue Errno::ENOENT, Errno::ESRCH
#       # someone else did our job for us
#     end
#   end
# end
#
# after_fork do |server, worker|
#   if defined?(ActiveRecord::Base)
#     ActiveRecord::Base.establish_connection
#   end
# end