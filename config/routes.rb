# frozen_string_literal: true

# == Route Map
#
#                                    Prefix Verb           URI Pattern                                                                              Controller#Action
# super_admin_memorial_plan_discounts_index GET            /super_admin/memorial_plan_discounts/index(.:format)                                     super_admin/memorial_plan_discounts#index
#                         memorial_memorial GET            /memorial/memorials/:id(.:format)                                                        memorial/memorials#show
#                       memorial_main_photo GET            /memorial/memorials/:id/main_photo(.:format)                                             memorial/memorials#main_photo
#                          new_user_session GET            /users/sign_in(.:format)                                                                 users/sessions#new
#                              user_session POST           /users/sign_in(.:format)                                                                 users/sessions#create
#                      destroy_user_session GET            /users/sign_out(.:format)                                                                users/sessions#destroy
#     user_google_oauth2_omniauth_authorize GET|POST       /omniauth/google_oauth2(.:format)                                                        users/omniauth_callbacks#passthru
#      user_google_oauth2_omniauth_callback GET|POST       /omniauth/google_oauth2/callback(.:format)                                               users/omniauth_callbacks#google_oauth2
#          user_facebook_omniauth_authorize GET|POST       /omniauth/facebook(.:format)                                                             users/omniauth_callbacks#passthru
#           user_facebook_omniauth_callback GET|POST       /omniauth/facebook/callback(.:format)                                                    users/omniauth_callbacks#facebook
#                         new_user_password GET            /users/password/new(.:format)                                                            users/passwords#new
#                        edit_user_password GET            /users/password/edit(.:format)                                                           users/passwords#edit
#                             user_password PATCH          /users/password(.:format)                                                                users/passwords#update
#                                           PUT            /users/password(.:format)                                                                users/passwords#update
#                                           POST           /users/password(.:format)                                                                users/passwords#create
#                  cancel_user_registration GET            /users/cancel(.:format)                                                                  users/registrations#cancel
#                     new_user_registration GET            /users/sign_up(.:format)                                                                 users/registrations#new
#                    edit_user_registration GET            /users/edit(.:format)                                                                    users/registrations#edit
#                         user_registration PATCH          /users(.:format)                                                                         users/registrations#update
#                                           PUT            /users(.:format)                                                                         users/registrations#update
#                                           DELETE         /users(.:format)                                                                         users/registrations#destroy
#                                           POST           /users(.:format)                                                                         users/registrations#create
#                           memorial_photos GET            /memorial/:memorial_id/photos(.:format)                                                  memorial/photos#index
#                        memorial_histories GET            /memorial/:memorial_id/histories(.:format)                                               memorial/histories#index
#                      memorial_guest_books GET            /memorial/:memorial_id/guest_books(.:format)                                             memorial/guest_books#index
#                                           POST           /memorial/:memorial_id/guest_books(.:format)                                             memorial/guest_books#create
#                       memorial_guest_book DELETE         /memorial/:memorial_id/guest_books/:id(.:format)                                         memorial/guest_books#destroy
#                   memorial_invite_friends GET            /memorial/:memorial_id/invite_friends(.:format)                                          memorial/invite_friends#index
#                                           POST           /memorial/:memorial_id/invite_friends(.:format)                                          memorial/invite_friends#create
#                new_memorial_invite_friend GET            /memorial/:memorial_id/invite_friends/new(.:format)                                      memorial/invite_friends#new
#               edit_memorial_invite_friend GET            /memorial/:memorial_id/invite_friends/:id/edit(.:format)                                 memorial/invite_friends#edit
#                    memorial_invite_friend GET            /memorial/:memorial_id/invite_friends/:id(.:format)                                      memorial/invite_friends#show
#                                           PATCH          /memorial/:memorial_id/invite_friends/:id(.:format)                                      memorial/invite_friends#update
#                                           PUT            /memorial/:memorial_id/invite_friends/:id(.:format)                                      memorial/invite_friends#update
#                                           DELETE         /memorial/:memorial_id/invite_friends/:id(.:format)                                      memorial/invite_friends#destroy
#                           memorial_events GET            /memorial/:memorial_id/events(.:format)                                                  memorial/events#index
#                                           POST           /memorial/:memorial_id/events(.:format)                                                  memorial/events#create
#                        new_memorial_event GET            /memorial/:memorial_id/events/new(.:format)                                              memorial/events#new
#                       edit_memorial_event GET            /memorial/:memorial_id/events/:id/edit(.:format)                                         memorial/events#edit
#                            memorial_event GET            /memorial/:memorial_id/events/:id(.:format)                                              memorial/events#show
#                                           PATCH          /memorial/:memorial_id/events/:id(.:format)                                              memorial/events#update
#                                           PUT            /memorial/:memorial_id/events/:id(.:format)                                              memorial/events#update
#                                           DELETE         /memorial/:memorial_id/events/:id(.:format)                                              memorial/events#destroy
#                                    bootsy                /bootsy                                                                                  Bootsy::Engine
#                                home_index GET            /home/index(.:format)                                                                    home#index {:format=>/html/}
#                                user_photo GET            /home/user_photo/:id(.:format)                                                           home#user_photo
#                                  about_us GET            /about_us(.:format)                                                                      home#about_us
#                              new_memorial GET            /new_memorial(.:format)                                                                  home#new_memorial
#                        new_memorial_step2 POST|PUT|PATCH /new_memorial_step2(.:format)                                                            home#new_memorial_step2
#                        new_memorial_step3 POST|PUT|PATCH /new_memorial_step3(.:format)                                                            home#new_memorial_step3
#                                     plans GET            /plans(.:format)                                                                         home#plans
#                                     login GET            /login(.:format)                                                                         home#login
#                                   contact GET            /contact(.:format)                                                                       home#contact
#                     memorial_photos_image GET            /memorial_photos/image/:id(/:version)(.:format)                                          memorial_photos#image
#              memorial_photos_image_remove GET            /memorial_photos/image_remove/:id(.:format)                                              memorial_photos#image_remove
#                            financial_cart GET            /cart(.:format)                                                                          carts#show
#                      financial_cart_items POST           /cart_items(.:format)                                                                    cart_items#create
#                       financial_cart_item PATCH          /cart_items/:id(.:format)                                                                cart_items#update
#                                           PUT            /cart_items/:id(.:format)                                                                cart_items#update
#                                           DELETE         /cart_items/:id(.:format)                                                                cart_items#destroy
#                              admin_painel GET            /admin/painel/:memorial_id(.:format)                                                     admin/painel#index {:format=>/html/}
#                        admin_profile_edit GET            /admin/profile(.:format)                                                                 admin/profile#edit {:format=>/html/}
#                      admin_profile_update PUT            /admin/profile(.:format)                                                                 admin/profile#update {:format=>/html/}
#                admin_profile_upload_photo POST           /admin/profile/upload_photo(.:format)                                                    admin/profile#upload_photo {:format=>/html/}
#                       admin_settings_edit GET            /admin/settings(.:format)                                                                admin/settings#edit {:format=>/html/}
#                    admin_financeiro_index GET            /admin/financeiro/index(.:format)                                                        admin/financeiro#index {:format=>/html/}
#                   admin_plans_select_plan GET            /admin/plans/select_plan/:memorial_id(.:format)                                          admin/plans#select_plan {:format=>/html/}
#                  admin_memorial_memorials GET            /admin/memorials/:memorial_id/memorials(.:format)                                        admin/memorials#index {:format=>/html/}
#                                           POST           /admin/memorials/:memorial_id/memorials(.:format)                                        admin/memorials#create {:format=>/html/}
#               new_admin_memorial_memorial GET            /admin/memorials/:memorial_id/memorials/new(.:format)                                    admin/memorials#new {:format=>/html/}
#              edit_admin_memorial_memorial GET            /admin/memorials/:memorial_id/memorials/:id/edit(.:format)                               admin/memorials#edit {:format=>/html/}
#                   admin_memorial_memorial GET            /admin/memorials/:memorial_id/memorials/:id(.:format)                                    admin/memorials#show {:format=>/html/}
#                                           PATCH          /admin/memorials/:memorial_id/memorials/:id(.:format)                                    admin/memorials#update {:format=>/html/}
#                                           PUT            /admin/memorials/:memorial_id/memorials/:id(.:format)                                    admin/memorials#update {:format=>/html/}
#                                           DELETE         /admin/memorials/:memorial_id/memorials/:id(.:format)                                    admin/memorials#destroy {:format=>/html/}
#              admin_memorial_memorial_user PATCH          /admin/memorials/:memorial_id/memorial_users/:id(.:format)                               admin/memorial_users#update {:format=>/html/}
#                                           PUT            /admin/memorials/:memorial_id/memorial_users/:id(.:format)                               admin/memorial_users#update {:format=>/html/}
#                                           DELETE         /admin/memorials/:memorial_id/memorial_users/:id(.:format)                               admin/memorial_users#destroy {:format=>/html/}
#                     admin_memorial_photos GET            /admin/memorials/:memorial_id/photos(.:format)                                           admin/photos#index {:format=>/html/}
#                                           POST           /admin/memorials/:memorial_id/photos(.:format)                                           admin/photos#create {:format=>/html/}
#                  new_admin_memorial_photo GET            /admin/memorials/:memorial_id/photos/new(.:format)                                       admin/photos#new {:format=>/html/}
#                 edit_admin_memorial_photo GET            /admin/memorials/:memorial_id/photos/:id/edit(.:format)                                  admin/photos#edit {:format=>/html/}
#                      admin_memorial_photo GET            /admin/memorials/:memorial_id/photos/:id(.:format)                                       admin/photos#show {:format=>/html/}
#                                           PATCH          /admin/memorials/:memorial_id/photos/:id(.:format)                                       admin/photos#update {:format=>/html/}
#                                           PUT            /admin/memorials/:memorial_id/photos/:id(.:format)                                       admin/photos#update {:format=>/html/}
#                                           DELETE         /admin/memorials/:memorial_id/photos/:id(.:format)                                       admin/photos#destroy {:format=>/html/}
#                     admin_memorial_videos GET            /admin/memorials/:memorial_id/videos(.:format)                                           admin/videos#index {:format=>/html/}
#                                           POST           /admin/memorials/:memorial_id/videos(.:format)                                           admin/videos#create {:format=>/html/}
#                  new_admin_memorial_video GET            /admin/memorials/:memorial_id/videos/new(.:format)                                       admin/videos#new {:format=>/html/}
#                 edit_admin_memorial_video GET            /admin/memorials/:memorial_id/videos/:id/edit(.:format)                                  admin/videos#edit {:format=>/html/}
#                      admin_memorial_video GET            /admin/memorials/:memorial_id/videos/:id(.:format)                                       admin/videos#show {:format=>/html/}
#                                           PATCH          /admin/memorials/:memorial_id/videos/:id(.:format)                                       admin/videos#update {:format=>/html/}
#                                           PUT            /admin/memorials/:memorial_id/videos/:id(.:format)                                       admin/videos#update {:format=>/html/}
#                                           DELETE         /admin/memorials/:memorial_id/videos/:id(.:format)                                       admin/videos#destroy {:format=>/html/}
#                    admin_memorial_friends GET            /admin/memorials/:memorial_id/friends(.:format)                                          admin/friends#index {:format=>/html/}
#                                           POST           /admin/memorials/:memorial_id/friends(.:format)                                          admin/friends#create {:format=>/html/}
#                 new_admin_memorial_friend GET            /admin/memorials/:memorial_id/friends/new(.:format)                                      admin/friends#new {:format=>/html/}
#                edit_admin_memorial_friend GET            /admin/memorials/:memorial_id/friends/:id/edit(.:format)                                 admin/friends#edit {:format=>/html/}
#                     admin_memorial_friend GET            /admin/memorials/:memorial_id/friends/:id(.:format)                                      admin/friends#show {:format=>/html/}
#                                           PATCH          /admin/memorials/:memorial_id/friends/:id(.:format)                                      admin/friends#update {:format=>/html/}
#                                           PUT            /admin/memorials/:memorial_id/friends/:id(.:format)                                      admin/friends#update {:format=>/html/}
#                                           DELETE         /admin/memorials/:memorial_id/friends/:id(.:format)                                      admin/friends#destroy {:format=>/html/}
#            admin_memorial_memorial_events GET            /admin/memorials/:memorial_id/memorial_events(.:format)                                  admin/memorial_events#index {:format=>/html/}
#                                           POST           /admin/memorials/:memorial_id/memorial_events(.:format)                                  admin/memorial_events#create {:format=>/html/}
#         new_admin_memorial_memorial_event GET            /admin/memorials/:memorial_id/memorial_events/new(.:format)                              admin/memorial_events#new {:format=>/html/}
#        edit_admin_memorial_memorial_event GET            /admin/memorials/:memorial_id/memorial_events/:id/edit(.:format)                         admin/memorial_events#edit {:format=>/html/}
#             admin_memorial_memorial_event GET            /admin/memorials/:memorial_id/memorial_events/:id(.:format)                              admin/memorial_events#show {:format=>/html/}
#                                           PATCH          /admin/memorials/:memorial_id/memorial_events/:id(.:format)                              admin/memorial_events#update {:format=>/html/}
#                                           PUT            /admin/memorials/:memorial_id/memorial_events/:id(.:format)                              admin/memorial_events#update {:format=>/html/}
#                                           DELETE         /admin/memorials/:memorial_id/memorial_events/:id(.:format)                              admin/memorial_events#destroy {:format=>/html/}
#                    admin_memorial_invites GET            /admin/memorials/:memorial_id/invites(.:format)                                          admin/invites#index {:format=>/html/}
#                                           POST           /admin/memorials/:memorial_id/invites(.:format)                                          admin/invites#create {:format=>/html/}
#                 new_admin_memorial_invite GET            /admin/memorials/:memorial_id/invites/new(.:format)                                      admin/invites#new {:format=>/html/}
#                     admin_memorial_invite PATCH          /admin/memorials/:memorial_id/invites/:id(.:format)                                      admin/invites#update {:format=>/html/}
#                                           PUT            /admin/memorials/:memorial_id/invites/:id(.:format)                                      admin/invites#update {:format=>/html/}
#                                           DELETE         /admin/memorials/:memorial_id/invites/:id(.:format)                                      admin/invites#destroy {:format=>/html/}
#               upload_photo_admin_memorial POST           /admin/memorials/:id/upload_photo(.:format)                                              admin/memorials#upload_photo {:format=>/html/}
#               remove_photo_admin_memorial DELETE         /admin/memorials/:id/remove_photo(.:format)                                              admin/memorials#remove_photo {:format=>/html/}
#                troca_plano_admin_memorial GET            /admin/memorials/:id/troca_plano(.:format)                                               admin/memorials#troca_plano {:format=>/html/}
#          upload_main_photo_admin_memorial POST           /admin/memorials/:id/upload_main_photo(.:format)                                         admin/memorials#upload_main_photo {:format=>/html/}
#                    restore_admin_memorial POST           /admin/memorials/:id/restore(.:format)                                                   admin/memorials#restore {:format=>/html/}
#                           admin_memorials GET            /admin/memorials(.:format)                                                               admin/memorials#index {:format=>/html/}
#                                           POST           /admin/memorials(.:format)                                                               admin/memorials#create {:format=>/html/}
#                        new_admin_memorial GET            /admin/memorials/new(.:format)                                                           admin/memorials#new {:format=>/html/}
#                       edit_admin_memorial GET            /admin/memorials/:id/edit(.:format)                                                      admin/memorials#edit {:format=>/html/}
#                            admin_memorial GET            /admin/memorials/:id(.:format)                                                           admin/memorials#show {:format=>/html/}
#                                           PATCH          /admin/memorials/:id(.:format)                                                           admin/memorials#update {:format=>/html/}
#                                           PUT            /admin/memorials/:id(.:format)                                                           admin/memorials#update {:format=>/html/}
#                                           DELETE         /admin/memorials/:id(.:format)                                                           admin/memorials#destroy {:format=>/html/}
#                              admin_orders GET            /admin/orders(.:format)                                                                  admin/orders#index {:format=>/html/}
#                                           POST           /admin/orders(.:format)                                                                  admin/orders#create {:format=>/html/}
#                           new_admin_order GET            /admin/orders/new(.:format)                                                              admin/orders#new {:format=>/html/}
#                          edit_admin_order GET            /admin/orders/:id/edit(.:format)                                                         admin/orders#edit {:format=>/html/}
#                               admin_order GET            /admin/orders/:id(.:format)                                                              admin/orders#show {:format=>/html/}
#                                           PATCH          /admin/orders/:id(.:format)                                                              admin/orders#update {:format=>/html/}
#                                           PUT            /admin/orders/:id(.:format)                                                              admin/orders#update {:format=>/html/}
#                                           DELETE         /admin/orders/:id(.:format)                                                              admin/orders#destroy {:format=>/html/}
#                  users_sign_in_with_token POST           /api/v1/users/sign_in_with_token(.:format)                                               users/device_authentication#sign_in_with_token {:format=>/json/}
#                  users_sign_up_with_email POST           /api/v1/users/sign_up_with_email(.:format)                                               users/device_authentication#sign_up_with_email {:format=>/json/}
#                  users_sign_in_with_email POST           /api/v1/users/sign_in_with_email(.:format)                                               users/device_authentication#sign_in_with_email {:format=>/json/}
#                        memorial_memorials GET            /api/v1/memorial/memorials(.:format)                                                     memorial/memorials#index {:format=>/json/}
#                                           GET            /api/v1/memorial/memorials/:id(.:format)                                                 memorial/memorials#show {:format=>/json/}
#                     memorial_follow_index POST           /api/v1/memorial/follow(.:format)                                                        memorial/follow#create {:format=>/json/}
#                           memorial_follow DELETE         /api/v1/memorial/follow/:id(.:format)                                                    memorial/follow#destroy {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/photos(.:format)                                           memorial/photos#index {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/histories(.:format)                                        memorial/histories#index {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/guest_books(.:format)                                      memorial/guest_books#index {:format=>/json/}
#                                           POST           /api/v1/memorial/:memorial_id/guest_books(.:format)                                      memorial/guest_books#create {:format=>/json/}
#                                           DELETE         /api/v1/memorial/:memorial_id/guest_books/:id(.:format)                                  memorial/guest_books#destroy {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/invite_friends(.:format)                                   memorial/invite_friends#index {:format=>/json/}
#                                           POST           /api/v1/memorial/:memorial_id/invite_friends(.:format)                                   memorial/invite_friends#create {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/invite_friends/new(.:format)                               memorial/invite_friends#new {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/invite_friends/:id/edit(.:format)                          memorial/invite_friends#edit {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/invite_friends/:id(.:format)                               memorial/invite_friends#show {:format=>/json/}
#                                           PATCH          /api/v1/memorial/:memorial_id/invite_friends/:id(.:format)                               memorial/invite_friends#update {:format=>/json/}
#                                           PUT            /api/v1/memorial/:memorial_id/invite_friends/:id(.:format)                               memorial/invite_friends#update {:format=>/json/}
#                                           DELETE         /api/v1/memorial/:memorial_id/invite_friends/:id(.:format)                               memorial/invite_friends#destroy {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/events(.:format)                                           memorial/events#index {:format=>/json/}
#                                           POST           /api/v1/memorial/:memorial_id/events(.:format)                                           memorial/events#create {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/events/new(.:format)                                       memorial/events#new {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/events/:id/edit(.:format)                                  memorial/events#edit {:format=>/json/}
#                                           GET            /api/v1/memorial/:memorial_id/events/:id(.:format)                                       memorial/events#show {:format=>/json/}
#                                           PATCH          /api/v1/memorial/:memorial_id/events/:id(.:format)                                       memorial/events#update {:format=>/json/}
#                                           PUT            /api/v1/memorial/:memorial_id/events/:id(.:format)                                       memorial/events#update {:format=>/json/}
#                                           DELETE         /api/v1/memorial/:memorial_id/events/:id(.:format)                                       memorial/events#destroy {:format=>/json/}
#                                           PATCH          /api/v1/admin/memorials/:memorial_id/photos/:id(.:format)                                admin/photos#update {:format=>/json/}
#                                           PUT            /api/v1/admin/memorials/:memorial_id/photos/:id(.:format)                                admin/photos#update {:format=>/json/}
#                                           POST           /api/v1/admin/memorials/:id/upload_photo(.:format)                                       admin/memorials#upload_photo {:format=>/json/}
#                                           DELETE         /api/v1/admin/memorials/:id/remove_photo(.:format)                                       admin/memorials#remove_photo {:format=>/json/}
#                                           GET            /api/v1/admin/memorials(.:format)                                                        admin/memorials#index {:format=>/json/}
#                                           POST           /api/v1/admin/memorials(.:format)                                                        admin/memorials#create {:format=>/json/}
#                                           GET            /api/v1/admin/memorials/new(.:format)                                                    admin/memorials#new {:format=>/json/}
#                                           GET            /api/v1/admin/memorials/:id/edit(.:format)                                               admin/memorials#edit {:format=>/json/}
#                                           GET            /api/v1/admin/memorials/:id(.:format)                                                    admin/memorials#show {:format=>/json/}
#                                           PATCH          /api/v1/admin/memorials/:id(.:format)                                                    admin/memorials#update {:format=>/json/}
#                                           PUT            /api/v1/admin/memorials/:id(.:format)                                                    admin/memorials#update {:format=>/json/}
#                                           DELETE         /api/v1/admin/memorials/:id(.:format)                                                    admin/memorials#destroy {:format=>/json/}
#                 new_security_user_session GET            /api/v1/auth/sign_in(.:format)                                                           devise_token_auth/sessions#new {:format=>/json/}
#                     security_user_session POST           /api/v1/auth/sign_in(.:format)                                                           devise_token_auth/sessions#create {:format=>/json/}
#             destroy_security_user_session GET            /api/v1/auth/sign_out(.:format)                                                          devise_token_auth/sessions#destroy {:format=>/json/}
#                new_security_user_password GET            /api/v1/auth/password/new(.:format)                                                      devise_token_auth/passwords#new {:format=>/json/}
#               edit_security_user_password GET            /api/v1/auth/password/edit(.:format)                                                     devise_token_auth/passwords#edit {:format=>/json/}
#                    security_user_password PATCH          /api/v1/auth/password(.:format)                                                          devise_token_auth/passwords#update {:format=>/json/}
#                                           PUT            /api/v1/auth/password(.:format)                                                          devise_token_auth/passwords#update {:format=>/json/}
#                                           POST           /api/v1/auth/password(.:format)                                                          devise_token_auth/passwords#create {:format=>/json/}
#         cancel_security_user_registration GET            /api/v1/auth/cancel(.:format)                                                            devise_token_auth/registrations#cancel {:format=>/json/}
#            new_security_user_registration GET            /api/v1/auth/sign_up(.:format)                                                           devise_token_auth/registrations#new {:format=>/json/}
#           edit_security_user_registration GET            /api/v1/auth/edit(.:format)                                                              devise_token_auth/registrations#edit {:format=>/json/}
#                security_user_registration PATCH          /api/v1/auth(.:format)                                                                   devise_token_auth/registrations#update {:format=>/json/}
#                                           PUT            /api/v1/auth(.:format)                                                                   devise_token_auth/registrations#update {:format=>/json/}
#                                           DELETE         /api/v1/auth(.:format)                                                                   devise_token_auth/registrations#destroy {:format=>/json/}
#                                           POST           /api/v1/auth(.:format)                                                                   devise_token_auth/registrations#create {:format=>/json/}
#                api_v1_auth_validate_token GET            /api/v1/auth/validate_token(.:format)                                                    devise_token_auth/token_validations#validate_token
#                       api_v1_auth_failure GET            /api/v1/auth/failure(.:format)                                                           devise_token_auth/omniauth_callbacks#omniauth_failure
#                                           GET            /api/v1/auth/:provider/callback(.:format)                                                devise_token_auth/omniauth_callbacks#omniauth_success
#                                           GET|POST       /omniauth/:provider/callback(.:format)                                                   devise_token_auth/omniauth_callbacks#redirect_callbacks
#                          omniauth_failure GET|POST       /omniauth/failure(.:format)                                                              devise_token_auth/omniauth_callbacks#omniauth_failure
#                                           GET            /api/v1/auth/:provider(.:format)                                                         redirect(301)
#  pagseguro_pagseguro_notifications_create POST           /pagseguro/notifications(.:format)                                                       pagseguro/notifications#create
#                 pagseguro_checkout_create POST           /pagseguro/checkout(.:format)                                                            pagseguro/checkout#create
#                            privacy_policy GET            /legal/privacy-policy(.:format)                                                          legal#privacy_policy
#                             service_terms GET            /legal/service-terms(.:format)                                                           legal#service_terms
#                                     admin GET            /admin(.:format)                                                                         redirect(301, /users/sign_up)
#                   test_exception_notifier GET            /test_exception_notifier(.:format)                                                       application#test_exception_notifier
#                                                          /422(.:format)                                                                           errors#rejected
#                                                          /404(.:format)                                                                           errors#not_found
#                                                          /500(.:format)                                                                           errors#internal_server_error
#                                      root GET            /                                                                                        home#index
#                        rails_service_blob GET            /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
#                 rails_blob_representation GET            /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#                        rails_disk_service GET            /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
#                 update_rails_disk_service PUT            /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#                      rails_direct_uploads POST           /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create
# 
# Routes for Bootsy::Engine:
# image_gallery_images GET    /image_galleries/:image_gallery_id/images(.:format)     bootsy/images#index
#                      POST   /image_galleries/:image_gallery_id/images(.:format)     bootsy/images#create
#  image_gallery_image DELETE /image_galleries/:image_gallery_id/images/:id(.:format) bootsy/images#destroy
#               images GET    /images(.:format)                                       bootsy/images#index
#                      POST   /images(.:format)                                       bootsy/images#create
#                image DELETE /images/:id(.:format)                                   bootsy/images#destroy

Rails.application.routes.draw do

  def routes_to_web_and_api
    get 'home/index'
  end

  namespace :super_admin do
    get 'memorial_plan_discounts/index'
  end

  namespace :memorial do
    get 'memorials/:id' => 'memorials#show', as: :memorial
    get 'memorials/:id/main_photo' => 'memorials#main_photo', as: :main_photo
  end

  devise_for :users, class_name: 'Security::User', controllers: {
      omniauth_callbacks: 'users/omniauth_callbacks',
      sessions: 'users/sessions',
      registrations: 'users/registrations',
      passwords: 'users/passwords'
  }

  namespace :memorial, path: '/memorial/:memorial_id' do
    resources :photos, only: :index
    resources :histories, only: :index
    resources :guest_books, only: [:index, :create, :destroy]
    resources :invite_friends
    resources :events
  end

  mount Bootsy::Engine => '/bootsy', as: 'bootsy'

  scope '/', { :format => 'html' } do
    routes_to_web_and_api
  end

  get 'home/user_photo/:id' => 'home#user_photo', as: :user_photo
  get 'about_us' => 'home#about_us', as: :about_us
  match 'new_memorial' => 'home#new_memorial', as: :new_memorial, via: [:get]
  match 'new_memorial_step2' => 'home#new_memorial_step2', as: :new_memorial_step2, via: [:post, :put, :patch]
  match 'new_memorial_step3' => 'home#new_memorial_step3', as: :new_memorial_step3, via: [:post, :put, :patch]
  get 'plans' => 'home#plans', as: :plans
  get 'login' => 'home#login', as: :login
  get 'contact' => 'home#contact', as: :contact
  get 'memorial_photos/image/:id(/:version)' => 'memorial_photos#image', as: :memorial_photos_image
  get 'memorial_photos/image_remove/:id' => 'memorial_photos#image_remove', as: :memorial_photos_image_remove

  resource :cart, only: [:show], as: :financial_cart
  resources :cart_items, only: [:create, :update, :destroy], as: :financial_cart_items

  namespace :admin,  :format => 'html' do
    get 'painel/:memorial_id' => 'painel#index', as: :painel
    get 'profile' => 'profile#edit', as: :profile_edit
    put 'profile' => 'profile#update', as: :profile_update
    post 'profile/upload_photo' => 'profile#upload_photo', as: :profile_upload_photo
    get 'settings' => 'settings#edit', as: :settings_edit
    get 'financeiro/index'
    get 'plans/select_plan/:memorial_id' => 'plans#select_plan', as: :plans_select_plan
    resources :memorials do
      resources :memorials
      resources :memorial_users, only: [:update, :destroy]
      resources :photos
      resources :videos
      resources :friends
      resources :memorial_events
      resources :invites, only: [:create, :new, :index, :update, :destroy]
      member do
        post 'upload_photo'
        delete 'remove_photo'
        get 'troca_plano'
        post 'upload_main_photo'
        post 'restore'
      end
    end
    resources :orders
  end

  scope :api,  :format => 'json'  do
    scope :v1 do
      post 'users/sign_in_with_token' => 'users/device_authentication#sign_in_with_token'
      post 'users/sign_up_with_email' => 'users/device_authentication#sign_up_with_email'
      post 'users/sign_in_with_email' => 'users/device_authentication#sign_in_with_email'
      post 'users/sign_up_with_firebase' => 'users/device_authentication#sign_up_with_firebase'
      namespace :memorial do
        resources :memorials, only: [:index, :show]
        resources :follow, only: [:create, :destroy]
      end
      namespace :memorial, path: '/memorial/:memorial_id' do
        resources :photos, only: :index
        resources :histories, only: :index
        resources :guest_books, only: [:index, :create, :destroy]
        resources :invite_friends
        resources :events
      end
      namespace :admin do
        resources :memorials do
          resources :photos, only: [:update]
          member do
            post 'upload_photo'
            delete 'remove_photo'
          end
        end
      end
      mount_devise_token_auth_for 'Security::User', at: 'auth'
    end
  end

  namespace :pagseguro do
    post 'notifications' => 'notifications#create', as: :pagseguro_notifications_create
    post 'checkout' => 'checkout#create', as: :checkout_create
  end

  get 'legal/privacy-policy' => 'legal#privacy_policy', as: :privacy_policy
  get 'legal/service-terms' => 'legal#service_terms', as: :service_terms

  get '/admin', to: redirect('/users/sign_up')

  get '/test_exception_notifier', :controller => 'application', :action => 'test_exception_notifier'

  match "/422", :to => "errors#rejected", :via => :all
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all

  root 'home#index'
end
