Sitemap.configure do |config|
  if Rails.env.development?
    config.save_path = Rails.root.join('public')
  else
    config.max_urls = 50000
    config.save_path = Rails.root.join('..', '..', 'shared', 'public')
  end
end

Sitemap::Generator.instance.load host: "www.memorialvivo.com.br", protocol: 'https' do

  literal "/", priority: 0.2, :change_frequency => 'monthly'

  Memorial::Memorial.all.each do |memorial|
    literal Rails.application.routes.url_helpers.memorial_memorial_path(memorial), priority: 0.7, :change_frequency => 'weekly'
  end

  # Sample path with params:
  #   path :faq, :params => { :format => "html", :filter => "recent" }
  # Depending on the route, the resolved url could be http://mywebsite.com/frequent-questions.html?filter=recent.

  # Sample resource:
  #   resources :articles

  # Sample resource with custom objects:
  #   resources :articles, :objects => proc { Article.published }

  # Sample resource with search options:
  #   resources :articles, :priority => 0.8, :change_frequency => "monthly"

  # Sample resource with block options:
  #   resources :activities,
  #             :skip_index => true,
  #             :updated_at => proc { |activity| activity.published_at.strftime("%Y-%m-%d") }
  #             :params => { :subdomain => proc { |activity| activity.location } }

end
