require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GrimesApp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.0
    config.time_zone = 'Brasilia'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = 'pt-BR'

    # Do not swallow errors in after_commit/after_rollback callbacks.
    # config.active_record.raise_in_transactional_callbacks = true

    social_keys = File.join(Rails.root, 'config', 'social_keys.yml')
    if File.exist?social_keys
      CONFIG = HashWithIndifferentAccess.new(YAML.load(IO.read(social_keys)))[Rails.env]
      CONFIG.each do |k, v|
        ENV[k.upcase] ||= v
      end
    end

    config.generators.stylesheets = false
    config.generators.javascripts = false
    config.middleware.use Rack::ContentLength

    config.exceptions_app = self.routes

    config.generators do |g|
      g.test_framework nil # to skip test framework
      g.helper false
      g.stylesheets false
    end

    config.to_prepare do
      Devise::RegistrationsController.send(:include, Devise::DeviseRegistrationsControllerDecorator)
    end

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
