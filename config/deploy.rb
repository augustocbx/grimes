# frozen_string_literal: true
set :application, 'grimes'
set :stages, ['staging', 'production']
set :default_stage, 'staging'
set :rvm_ruby_version, '2.5.7@grimes-prod'
set :rvm_type, :system

set :rollbar_token, '18eedd3e4bb8497db7b125fa9f4c54b6'
set :rollbar_env, Proc.new { fetch :stage }
set :rollbar_role, Proc.new { :app }

set :deploy_root, '/var/rails_app'
set :deploy_to, "#{fetch(:deploy_root)}/#{fetch(:application)}/#{fetch(:stage)}"
set :repo_url, 'git@gitlab.com:ConectaCard/grimes.git'

set :puma_conf, "#{shared_path}/config/puma/#{fetch(:stage)}.rb"
set :puma_threads, [0, 16]
set :puma_workers, 2

set(:linked_files, ['config/database.yml', 'config/secrets.yml', 'config/social_keys.yml', 'config/config.yml', 'config/storage.yml', 'public/sitemap.xml', 'config/api-project-1033394134810-72f32ed8590e.json'])
set :linked_dirs, %w(log public/uploads public/assets tmp/cache tmp/pids tmp/sockets uploaded_files public/.well-known)

set :use_sudo, false

namespace :deploy do
  task :setup do
    run "mkdir #{fetch(:shared_path)}/log -p"
    run "mkdir #{fetch(:releases_path)} -p"
    run "chown #{fetch(:user)}:www-data #{fetch(:deploy_to)} -R"
  end
  task :seta_versao do
  end
  task :cucumber do
    on roles(:app, on_error: :continue) do
      run "cd #{fetch(:current_path)} && RAILS_ENV=cucumber bundle exec ruby script/cucumber features --expand --color -r features --format html -o #{fetch(:current_path)}/public/cucumber.html"
    end
  end
  task :get_cucumber_json do
    get("#{fetch(:current_path)}/public/cucumber.html", 'cucumber.html')
  end
  task :svn_cleanup do
    run "cd #{fetch(:shared_path)}/cached-copy && svn cleanup"
  end
end

namespace :bower do
  desc 'Install bower'
  task :install do
    on roles(:web) do
      within release_path do
        execute :rake, 'bower:install CI=true'
      end
    end
  end
end

namespace :thin do
  desc 'Restart Application Thin'
  task :restart do
    run 'sudo /etc/init.d/thin restart'
  end
end

namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{release_path}"
  end
end

# after 'deploy:publishing', 'deploy:restart'
# namespace :deploy do
#   task :restart do
#     invoke 'unicorn:legacy_restart'
#   end
# end

before 'deploy:assets:precompile', 'bower:install'

namespace :deploy do
  namespace :db do
    desc 'Runs rake db:seed'
    task :seed => [:set_rails_env] do
      on primary fetch(:migration_role) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, "db:seed"
          end
        end
      end
    end
  end
end

# after 'deploy:updated', 'passenger:restart', 'deploy:seta_versao', 'deploy:cleanup' #, 'deploy:cucumber'
# after 'deploy:migrate', 'passenger:restart'
