# frozen_string_literal: true
# server '162.243.172.36', :app, :application, :web, :db, :primary => true
server '162.243.172.36', user: 'deploy', roles: %w(app web db)

set :ssh_options, forward_agent: true, verify_host_key: :never

# set_settings(YAML.load_file("/var/lib/jenkins/conectacard/grimes/grimes.yml"))

set :rails_env,   'production'

set :keep_releases, 5
