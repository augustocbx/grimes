# frozen_string_literal: true
server '162.243.172.36', :app, :application, :web, :db, primary: true

set :ssh_options, forward_agent: true, verify_host_key: :never

set_settings(YAML.load_file('/var/lib/jenkins/conectacard/grimes/grimes.yml'))

set :scm, :subversion
set :deploy_via, :remote_cache

set :use_sudo, false

set :keep_releases, 2
