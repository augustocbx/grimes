namespace :firestore do
  task :update_memoriais_force_all => :environment do
    Memorial::Memorial.unscoped.all.each do |memorial|
      FIRESTORE.collection('memorials').doc(memorial.id).set(memorial.to_builder(nil))
    end
    if Rails.env.production?
      FIRESTORE.collection('memorials').list_documents.each do |doc_ref|
        if Memorial::Memorial.unscoped.where(id: doc_ref.document_id).first.nil?
          doc_ref.delete
        end
      end
    end
  end
end