namespace :firestore do
  task :listen_memorials_changes => :environment do
    query = FIRESTORE.collection('memorials')
    listener = query.listen do |snapshot|
      `touch tmp/#{DateTime.now}`
      puts 'listem:'
      puts "The query snapshot has #{snapshot.docs.count} documents "
      # puts "and has #{snapshot.changes.count} changes. (#{DateTime.now})"
      #
      snapshot.changes.each do |document_change|
        memorial_attributes = {
          security_user_id: document_change.doc.data[:security_user_id],
          mmry_firstname: document_change.doc.data[:mmry_firstname],
          mmry_lastname: document_change.doc.data[:mmry_lastname],
          mmry_nicknames: document_change.doc.data[:mmry_nicknames],
          mmry_gender: document_change.doc.data[:mmry_gender],
          mmry_biografy: document_change.doc.data[:mmry_biografy],
          mmry_birth_date: document_change.doc.data[:mmry_birth_date],
          mmry_birth_location: document_change.doc.data[:mmry_birth_location],
          mmry_death_date: document_change.doc.data[:mmry_death_date],
          mmry_death_location: document_change.doc.data[:mmry_death_location],
          mmry_cause_of_death: document_change.doc.data[:mmry_cause_of_death],
          mmry_tombstone_geo: document_change.doc.data[:mmry_tombstone_geo],
          mmry_privacy: document_change.doc.data[:mmry_privacy],
          photo: document_change.doc.data[:photo],
          enabled: document_change.doc.data[:enabled],
          mmry_visits: document_change.doc.data[:mmry_visits],
        }
        memorial = Memorial::Memorial.where(id: document_change.doc.data[:id]).first
        if memorial.nil?
          memorial = Memorial::Memorial.new(memorial_attributes)
          memorial.id = document_change.doc.data[:id]
        else
          memorial.attributes = memorial_attributes
        end
        puts memorial.save!
        # puts snapshot
        # puts snapshot.inspect
        # puts snapshot.id
      end
      puts snapshot.changes.size
      # puts listener.stopped?
    end
    listener.on_error do |error|
      puts error
    end
    puts listener.last_error
    puts listener.start
    puts listener.stopped?
    puts 'yyyy'
    sleep 3550
    listener.stop
  end
end