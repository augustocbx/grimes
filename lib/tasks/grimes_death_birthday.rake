namespace :grimes do
  task :death_birthday => :environment do
    Memorial::Memorial.where(["mmry_death_date + MAKE_INTERVAL(YEARS := #{Date.today.year} - EXTRACT(YEAR FROM mmry_death_date)::INTEGER) = :mmry_death_date and deleted_at IS NULL", mmry_death_date: Date.today]).each do |memorial|
      options = {
        notification: {
          "title": "Hoje faz #{(memorial - Date.today).years} anos que #{memorial.title} se foi",
          "body": "Clique para se lembrar"
        }
      }
      FCM_CLIENT.send(['dQcn5GDhT2-VPaWfSYZNAu:APA91bF4SFVN_eGpw7xDofeY0bg3SlHA_pAQl5Dy4hy14tPX-wlFGHaPekgwWriJbwIAVu4JhHyOytoz-MSJfTSYEDIWZEWIJbm1eWhYof3QuunLcIUZUN_EMwMdjdXgNXFog4Z5zfEw'], options)
    end
  end
end