//= require jquery/dist/jquery
//= require jquery_ujs
//= require jquery-ui/jquery-ui
//= require bootstrap
//= require bootsy
//= require bootstrap-switch

//= require bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar

//= require jquery-ui/ui/widget
//= require dropzone/dist/dropzone


//  JS Implementing Plugins -->
//= require layout/unify-v1.9.1/assets/plugins/back-to-top.js
//= require layout/unify-v1.9.1/assets/plugins/smoothScroll.js
//= require layout/unify-v1.9.1/assets/plugins/jquery.parallax.js
// require layout/unify-v1.9.1/assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js
//= require layout/unify-v1.9.1/assets/js/plugins/datepicker
//= require jquery-ui/ui/i18n/datepicker-pt-BR.js

//= require layout/unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/js/jquery.maskedinput.min.js
//= require layout/unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min
//= require layout/unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min

// best_in_place
//= require best_in_place
//= require best_in_place.jquery-ui

//  JS Customization -->
//= require layout/unify-v1.9.1/assets/js/custom.js

//  JS Page Level -->
//= require layout/unify-v1.9.1/assets/js/app.js

//= require_self

jQuery(document).ready(function () {
    App.init();


    myDropzoneObj = null;
    $( document ).ready(function() {
        if (document.getElementById('previewTemplatePhotoUpload') != null) {
            $('#photoUploadModal').dropzone({
                uploadMultiple: false,
                previewTemplate: document.getElementById('previewTemplatePhotoUpload').innerHTML,
                maxFiles: 1,
                autoProcessQueue: false,
                acceptedFiles: 'image/*',
                maxfilesexceeded: function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                },
                init: function () {
                    myDropzoneObj = this;
                    this.on("success", function (file, response) {
                        window.location.reload();
                    });
                }

            });

            $(function () {
                $('.modal').on('hidden.bs.modal', function () {
                    myDropzoneObj.removeAllFiles();
                });
            });
        }
    });

    //Dropzone.options.photoUploadModal = {
    //    paramName: "file", // The name that will be used to transfer the file
    //    maxFilesize: 5, // MB
    //    acceptedFiles: "image/*",
    //    //addRemoveLinks: true,
    //    parallelUploads: 20,
    //    //dictRemoveFile: 'Remover',
    //    //dictRemoveFile: 'Remover',
    //    autoProcessQueue: false
    //};
    Dropzone.autoDiscover = false;


    $("#memorial_memorial_mmry_birth_date").mask('9999-99-99', {placeholder:'X'});
    $('#memorial_memorial_mmry_birth_date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        yearRange: "-120:+0",
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>'
    });

    $("#memorial_memorial_mmry_death_date").mask('9999-99-99', {placeholder:'X'});
    $('#memorial_memorial_mmry_death_date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        yearRange: "-120:+0",
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>'
    });

});