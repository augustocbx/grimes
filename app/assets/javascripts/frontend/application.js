
//= require jquery
//= require jquery_ujs
//= require jquery.steps/build/jquery.steps
//= require bootstrap

//  JS Implementing Plugins -->
//= require layout/unify-v1.9.1/assets/js/plugins/datepicker
//= require layout/unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/js/jquery.maskedinput.min.js
//= require layout/unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min
//= require layout/unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min
//= require jquery-ui/ui/i18n/datepicker-pt-BR.js


//  JS Customization -->
//= require layout/unify-v1.9.1/assets/js/custom.js

//  JS Page Level -->

//= require layout/unify-v1.9.1/assets/js/app

//= require frontend/stepWizardShoppingCart

//= require blueimp-gallery/js/blueimp-gallery
//= require blueimp-gallery/js/blueimp-gallery-fullscreen
//= require blueimp-gallery/js/blueimp-gallery-indicator
//= require blueimp-gallery/js/jquery.blueimp-gallery
//= require social-share-button
//= require bootsy

//= require_self



$('.nav-tabs a').click(function (e) {
    if (this.getAttribute('aria-disabled')){
        return false;
    }
    $(this).tab('show');
    var scrollmem = $('body').scrollTop();
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);
});

$(function(){
    var hash = window.location.hash;

    obj_nav_tab = $('ul.nav a[href="' + hash + '"]')[0];
    if (obj_nav_tab == undefined){
        return false;
    }
    if (obj_nav_tab.getAttribute('aria-disabled')){
        return false;
    }

    hash && $('ul.nav a[href="' + hash + '"]').tab('show');
});


jQuery(document).ready(function() {
    App.init();

    $("#memorial_memorial_mmry_birth_date").mask('9999-99-99', {placeholder:'X'});
    $('#memorial_memorial_mmry_birth_date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        yearRange: "-120:+0",
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>'
    });

    $("#memorial_memorial_mmry_death_date").mask('9999-99-99', {placeholder:'X'});
    $('#memorial_memorial_mmry_death_date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        yearRange: "-120:+0",
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>'
    });


    //Validation.initValidation();
    //StyleSwitcher.initStyleSwitcher();
});

StepWizard.initStepWizard();