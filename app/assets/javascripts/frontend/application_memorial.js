//= require jquery
//= require jquery_ujs
//= require bootstrap

//  JS Implementing Plugins -->
//= require layout/unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/js/jquery.maskedinput.min.js
//= require layout/unify-v1.9.1/assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min

//  JS Customization -->
//= require layout/unify-v1.9.1/assets/js/custom.js

//  JS Page Level -->

//= require layout/unify-v1.9.1/assets/js/app

//= require blueimp-gallery/js/blueimp-gallery
//= require blueimp-gallery/js/blueimp-gallery-fullscreen
//= require blueimp-gallery/js/blueimp-gallery-indicator
//= require blueimp-gallery/js/jquery.blueimp-gallery
//= require social-share-button

//= require formvalidation/js/formValidation.min
//= require formvalidation/js/framework/bootstrap.min
//= require formvalidation/js/language/pt_BR

//=require pickadate/lib/picker
//=require pickadate/lib/picker.date
//=require pickadate/lib/picker.time
//=require pickadate/lib/translations/pt_BR

//= require_self


$('.nav-tabs a').click(function (e) {
    if (this.getAttribute('aria-disabled')) {
        return false;
    }
    $(this).tab('show');
    var scrollmem = $('body').scrollTop();
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);
});

$(function () {
    var hash = window.location.hash;

    obj_nav_tab = $('ul.nav a[href="' + hash + '"]')[0];
    if (obj_nav_tab == undefined) {
        return false;
    }
    if (obj_nav_tab.getAttribute('aria-disabled')) {
        return false;
    }

    hash && $('ul.nav a[href="' + hash + '"]').tab('show');
});
