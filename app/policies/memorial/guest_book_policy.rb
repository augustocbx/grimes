class Memorial::GuestBookPolicy < ApplicationPolicy

  def initialize(user, guest_book)
    @user = user
    @guest_book = guest_book
  end

  def destroy?
    @guest_book.user == @user
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
