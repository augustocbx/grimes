class Memorial::MemorialPolicy < ApplicationPolicy
  attr_reader :user, :memorial

  def initialize(user, memorial)
    @user = user
    @memorial = memorial
  end

  def index?
    user_admin?
  end

  def edit?
    user_admin?
  end

  def update?
    user_admin?
  end

  def destroy?
    user_admin?
  end

  def restore?
    user_admin?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end

  private

  def user_admin?
    return false if !user.present?
    return true if memorial.user == user
    return true if user.superuser?
    user_from_memorial = memorial.security_memorial_users.where(user_id: user.id).first
    user_from_memorial.present? && user_from_memorial.admin?
  end
end
