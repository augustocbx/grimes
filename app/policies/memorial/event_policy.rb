class Memorial::EventPolicy < ApplicationPolicy
  attr_reader :user, :event

  def initialize(user, event)
    @user = user
    @event = event
  end

  def index?
    user_admin?
  end

  def edit?
    user_admin?
  end

  def update?
    user_admin?
  end

  def destroy?
    user_admin?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end

  private

  def user_admin?
    memorial = event.memorial
    return false if !user.present?
    return true if memorial.user == user
    user_from_memorial = memorial.security_memorial_users.where(user_id: user.id).first
    user_from_memorial.present? && user_from_memorial.admin?
  end
end
