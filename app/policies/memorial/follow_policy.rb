class Memorial::FollowPolicy < ApplicationPolicy
  include Pundit
  attr_reader :user, :event

  def initialize(user, event)
    @user = user
    @event = event
  end
  class Scope < Scope
    def resolve
      scope
    end
  end
end