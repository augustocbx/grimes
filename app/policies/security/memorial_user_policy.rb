class Security::MemorialUserPolicy < ApplicationPolicy
  attr_reader :user, :memorial_user

  def initialize(user, memorial_user)
    @user = user
    @memorial_user = memorial_user
  end

  def update?
    Pundit.authorize(user, memorial_user.memorial, :update?) && memorial_user.user != user
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
