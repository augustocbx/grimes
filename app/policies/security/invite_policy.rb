class Security::InvitePolicy < ApplicationPolicy
  attr_reader :user, :invite

  def initialize(user, invite)
    @user = user
    @invite = invite
  end

  def update?
    Pundit.authorize(user, invite.memorial, :update?)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
