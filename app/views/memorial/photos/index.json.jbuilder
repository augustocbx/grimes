json.photos do
  json.array! @memorial.memorial_photos do |memorial_photo|
    json.partial! 'memorial/photos/show', memorial_photo: memorial_photo
  end
end
