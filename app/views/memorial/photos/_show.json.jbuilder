json.extract!(memorial_photo, :id, :mmry_phto_title, :mmry_phto_description, :created_at, :updated_at)
json.image_url memorial_photo.image.public_url('image_mobile').gsub('localhost', CONFIG['host'])
json.memorial do
  json.id memorial_photo.memorial.id
end