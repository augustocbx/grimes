json.extract!(memorial, *memorial.attribute_names.select { |an| an != 'photo' })
json.mmry_biografy Html2Text.convert(memorial.mmry_biografy)
json.url memorial_memorial_url(id: memorial.id)
json.card_thumb_url memorial.card_thumb_url.gsub('localhost', CONFIG['host'])
json.social_thumb_url memorial.social_thumb_url.gsub('localhost', CONFIG['host'])
json.thumb_url memorial.thumb_url.gsub('localhost', CONFIG['host'])
json.photos_count memorial.memorial_photos.size
json.messages_count memorial.memorial_guest_books.size
json.photos do
  json.array! memorial.memorial_photos do |memorial_photo|
    json.partial! 'memorial/photos/show', memorial_photo: memorial_photo
  end
end
json.owners memorial.owners
json.followings memorial.followings
json.guest_books do
  #   json.array! memorial.memorial_guest_books.select{|gb| gb.user.name.present?} do |guest_book|
  #     json.partial! 'memorial/guest_books/show', guest_book: guest_book
  #   end
  json.array! memorial.memorial_guest_books do |guest_book|
    json.partial! 'memorial/guest_books/show', guest_book: guest_book
  end
end

json.following current_user.present? && current_user.memorial_added?(memorial)
json.admin_permission current_user.present? && current_user.memorial_admin?(memorial)
