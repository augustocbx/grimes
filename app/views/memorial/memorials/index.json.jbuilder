json.array! @memorials_added do |memorial|
  json.partial! 'memorial/memorials/show', memorial: memorial
end
json.array! @memorials do |memorial|
  json.partial! 'memorial/memorials/show', memorial: memorial
end
