json.extract!(guest_book, :id, :user_id, :guest_book_message, :created_at, :updated_at, :deleted_at)
json.memorial do
  json.id guest_book.memorial.id
end
json.memorial_user do
  json.id guest_book.user.id
  json.name guest_book.user.name
  json.image_url guest_book.user.image.public_url
end
json.memorial do
  json.id guest_book.memorial.id
end