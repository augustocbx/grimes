json.array! @guest_books do |guest_book|
  json.partial! 'memorial/guest_books/show', guest_book: guest_book
end