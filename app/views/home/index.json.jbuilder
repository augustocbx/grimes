json.array! @memorials do |memorial|
  json.partial! 'memorial/memorials/show', memorial: memorial
end
