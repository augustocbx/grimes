json.followed_memorials do
  json.array! @memorials_added do |memorial|
    json.partial! 'memorial/memorials/show', memorial: memorial
  end
end
