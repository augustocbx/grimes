class ErrorsController < ApplicationController
  respond_to :html

  def not_found
  end

  def internal_server_error
  end

  def rejected
  end
end
