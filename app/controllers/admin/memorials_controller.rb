# frozen_string_literal: true
class Admin::MemorialsController < ApplicationAdminMemorialController
  respond_to :html
  before_action :authenticate_user!
  skip_before_action :authenticate_user!, if: -> {
    request.format.json?
  }
  before_action :set_memorial, only: [:show, :edit, :update, :destroy, :upload_photo, :remove_photo, :upload_main_photo]

  def new
    @memorial = Memorial::Memorial.new
  end

  def edit
    authorize @memorial
  end

  def create
    @memorial = Memorial::Memorial.new(memorial_params[:memorial_memorial])
    @memorial.security_user_id = current_user.id

    saved = @memorial.save
    respond_to do |format|
      format.html do
        if saved
          FIRESTORE.collection('memorials').doc(@memorial.id).set(@memorial.to_builder())
          flash[:notice] = 'Deu tudo certo!'
          redirect_to action: :index
        else
          flash[:error] = 'Falha ao salvar!'
          render :new
        end
      end
      format.json do
        render 'memorial/memorials/show'
      end
    end
  end

  def update
    authorize @memorial
    saved = @memorial.update(memorial_params[:memorial_memorial])
    if saved
      @message = t('user.updated')
      respond_to do |format|
        format.html do
          redirect_to action: :edit
        end
        format.json do
          render 'memorial/memorials/show'
        end
      end
    end
  end

  def destroy
    authorize @memorial
    removed = @memorial.destroy
    respond_to do |format|
      format.html do
        if removed
          flash[:notice] = 'Deu tudo certo!'
        else
          flash[:fail] = 'Falha na remoção do Memorial!'
        end
        redirect_to action: :index
      end
      format.json do
        render 'memorial/memorials/show'
      end
    end
  end

  def restore
    authorize @memorial
    if @memorial.restore
      flash[:notice] = 'Deu tudo certo!'
    else
      flash[:fail] = 'Falha na recuperação do Memorial!'
    end
    redirect_to edit_admin_memorial_path(id: @memorial.id)
  end

  def index
    @memorials = if !params[:search_str].blank?
                   if params[:show_removed] == 'true'
                     Memorial::Memorial.unscoped.search(current_user, params[:search_str])
                   else
                     Memorial::Memorial.search(current_user, params[:search_str])
                   end
                 else
                   if params[:show_removed] == 'true'
                     Memorial::Memorial.unscoped.where(security_user_id: current_user).order(:mmry_firstname).all
                   else
                     Memorial::Memorial.where(security_user_id: current_user).order(:mmry_firstname).all
                   end
                 end
    render :index
  end

  def upload_photo
    puts "checando permissão"
    authorize @memorial, :update?
    puts "Permissão concedida"
    uploaded_io = memorial_upload_params[:file]
    @memorial.memorial_photos.build(image: uploaded_io)
    photo_saved = @memorial.save!
    puts "Atualizando firebase"
    FIRESTORE.collection('memorials').doc(@memorial.id).set(@memorial.to_builder(nil))

    respond_to do |format|
      format.html do
        if photo_saved
          render status: :ok, nothing: true
        else
          render status: :bad_request, nothing: true
        end
      end
      format.json do
        if photo_saved
          render json: {
            success: true,
            id: @memorial.memorial_photos.last.id
          }
        else
          render json: {
            success: false
          }
        end
      end
    end
  end

  def upload_main_photo
    authorize @memorial, :update?
    uploaded_io = memorial_upload_params[:file]
    @memorial.photo = uploaded_io
    if @memorial.save!
      head :ok
    else
      render status: :bad_request, nothing: true
    end
  end

  def remove_photo
    authorize @memorial, :update?
    removed = false
    if params[:memorial_photo_id]
      if @memorial.memorial_photos.where(id: params[:memorial_photo_id]).present?
        removed = @memorial.memorial_photos.destroy(params[:memorial_photo_id])
      end
    end

    respond_to do |format|
      format.html do
        redirect_to admin_memorial_photos_path(memorial_id: @memorial.id)
      end
      format.json do
        render json: {
          success: removed.present?
        }
      end
    end
  end

  def troca_plano
    authorize @memorial, :update?
    if params[:id]
      @memorial_plans = Memorial::MemorialPlan.where(mmrl_plan_active: true).where(':hoje between mmrl_plan_vigence_start_date and mmrl_plan_vigence_end_date', hoje: Date.today).order('mmrl_plan_free_default desc, mmrl_plan_price asc')
      @memorial = Memorial::Memorial.find(params[:id])
    else
      flash[:error] = 'Memorial não informado'
      redirect_to admin_memorials_path
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def memorial_params
    formatted_params = params.clone
    fields = [:mmry_firstname, :mmry_lastname, :mmry_nicknames, :mmry_gender, :mmry_biografy, :mmry_birth_date, :mmry_birth_location, :mmry_death_date, :mmry_cause_of_death, :mmry_death_location, :mmry_tombstone_geo, :mmry_privacy, :bootsy_image_gallery_id]
    formatted_params.permit(:id, memorial_memorial: fields)
  end

  def memorial_upload_params
    params.permit(:file)
  end

  def set_memorial
    @memorial ||= Memorial::Memorial.unscoped.find(params[:id])
    if @memorial.nil?
      redirect_to admin_profile_edit_path
      return
    end
  end
end
