# frozen_string_literal: true
class Admin::PlansController < ApplicationController
  before_action :set_memorial, :set_plans

  def select_plan
    render :select_plan
  end

  private

  def set_memorial
    @memorial ||= Memorial::Memorial.unscoped.find(params[:memorial_id])
  end

  def set_plans
    memorial_plans_condition = [':hoje between mmrl_plan_vigence_start_date and mmrl_plan_vigence_end_date and memorial_memorial_plans.id != :memorial_plan_id', hoje: Date.today, memorial_plan_id: @memorial.current_memorial_plan.id]
    @memorial_plans = Memorial::MemorialPlan.where(mmrl_plan_active: true).where(memorial_plans_condition).order('mmrl_plan_free_default desc, mmrl_plan_price asc')
  end
end
