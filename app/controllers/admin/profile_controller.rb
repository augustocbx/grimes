# frozen_string_literal: true
class Admin::ProfileController < ApplicationAdminController
  before_action :authenticate_user!
  before_action :set_user, only: [:edit, :update]

  def edit
    @user = Security::User.find(current_user.id)
    render 'edit'
  end

  def update
    saved = if !user_params[:password].blank? || !user_params[:password_confirmation].blank?
              @user.update(user_params)
            else
              @user.update_without_password(user_params)
            end
    if saved
      flash[:notice] = t('user.updated')
      render :edit, status: :ok
    else
      flash[:alert] = @user.errors.full_messages
      render :edit, status: :unprocessable_entity
    end
  end

  def upload_photo
    uploaded_io = upload_params[:file]
    current_user.image = uploaded_io
    if current_user.save!
      head :ok
    else
      render status: :bad_request, nothing: true
    end
  end

  private

  def set_user
    @user = Security::User.find(current_user.id)
  end

  def user_params
    params[:security_user].permit(:username, :name, :email, :about, :password, :password_confirmation)
  end

  def upload_params
    params.permit(:file)
  end
end
