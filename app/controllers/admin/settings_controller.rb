# frozen_string_literal: true
class Admin::SettingsController < ApplicationAdminController
  before_action :authenticate_user!

  def edit
  end
end
