# frozen_string_literal: true
class Admin::OrdersController < ApplicationAdminMemorialController
  before_action :authenticate_user!

  def index
    @orders = Financial::Order.all.joins(:financial_order_items).where(security_user_id: current_user).order(:created_at)
  end

  def show
  end

  def create
  end

  def update
  end

  def destroy
  end
end
