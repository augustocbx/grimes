# frozen_string_literal: true
class Admin::PainelController < ApplicationAdminMemorialController
  before_action :authenticate_user!

  def index
    authorize @memorial
  end
end
