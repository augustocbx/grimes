# frozen_string_literal: true
class Admin::PhotosController < ApplicationAdminMemorialController
  skip_before_action :authenticate_user!, if: -> {
    request.format.json?
  }
  before_action :set_memorial, only: [:index]
  before_action :set_memorial_photo, only: [:update, :destroy]

  def index
    authorize @memorial
    render :index
  end

  def update
    authorize @memorial, :update?
    if @memorial_photo.update(memorial_photo_params[:memorial_memorial_photo])
      @memorial.touch
      head :ok
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @memorial, :update?
    if @memorial_photo.destroy
      @memorial.touch
      flash[:notice] = 'Foto removida com sucesso!'
      redirect_to action: :index
    end
  end

  private

  def set_memorial
    @memorial ||= Memorial::Memorial.find(params[:memorial_id])
  end

  def set_memorial_photo
    @memorial_photo = Memorial::MemorialPhoto.find(params[:id])
  end

  def memorial_photo_params
    params.permit(memorial_memorial_photo: [:mmry_phto_title, :mmry_phto_description])
  end
end
