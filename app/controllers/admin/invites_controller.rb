# frozen_string_literal: true
class Admin::InvitesController < ApplicationAdminMemorialController

  before_action :authenticate_user!
  before_action :prepare_invite_for_create, only: :create
  before_action :set_memorial

  before_action :store_current_location, :unless => :devise_controller?

  def index
    authorize @memorial
    @invite = Security::Invite.new(memorial_id: @memorial.id)
    @invites = Security::Invite.where('memorial_id = :memorial_id and accepted_at is null and rejected_at is null', memorial_id: @memorial.id).includes(:sender).all
  end

  def create
    errors = []
    notices = []
    @invites ||= []
    if !@invite.nil?
      if @invite.alert.blank? && @invite.save
        flash[:notice] = "Convite enviado com sucesso para: #{@invite.email}"
        # Send the invite data to our mailer to deliver the email
        url = new_user_registration_url(invite_token: @invite.token)
        InviteMailer.new_user_invite(@invite, url).deliver
      else
        flash[:alert] = @invite.alert
        # notices << @invite.error_message_for_create
        redirect_to admin_memorial_invites_path(memorial_id: @invite.memorial.id)
        return
      end
    elsif !@invites.nil? && !@invites.empty?
      @invites.each do |invite|
        if !invite.alert.blank? || !invite.errors.empty?
          if !invite.alert.blank?
            errors << invite.alert
          # elsif !invite.errors.empty?
          #   errors << invite.errors.full_messages
          end
        else
          notices << invite.notice
        end
      end
      flash[:alert] = errors.join(", ")
    else
      flash[:alert] = 'Você não pode criar um convite sem informar o email'
    end
    if !notices.empty?
      flash[:notice] = 'Convite(s) enviado(s) com sucesso para: ' + notices.select { |notice| !notice.blank? }.join(", ")
    end

    redirect_back(fallback_location: admin_memorial_invites_path)
  end

  def update
    invite = Security::Invite.find_by(memorial_id: params[:memorial_id], id: params[:id])

    if authorize(invite) && invite.update(admin: params[:admin])
      head :no_content
    # else
    #   head status: 422
    end
  end

  def destroy
    authorize @memorial, :update?
    invite = Security::Invite.find(params[:id])
    invite.destroy
    redirect_to admin_memorial_invites_path(memorial_id: invite.memorial.id)
  end

  private

  def invite_params
    params.permit(:id, :memorial_id, emails: [], security_invite: [:memorial_id, :email])
  end

  def prepare_invite_for_create
    if invite_params[:security_invite][:email].present?
      @invite = Security::Invite.create_invite(@memorial, invite_params[:security_invite][:email], current_user)
      if !@invite.alert.blank?
        flash[:error] = @invite.alert
      end
    elsif invite_params[:emails].present?
      @invites = []
      invite_params[:emails].split(/[\r\n\;\,\ ]/i).select { |str| !str.blank? }.each do |email|
        @invites << Security::Invite.create_invite(@memorial, email, current_user)
      end
    end
  end

  def set_memorial
    @memorial ||= Memorial::Memorial.find(params[:memorial_id])
  end
end
