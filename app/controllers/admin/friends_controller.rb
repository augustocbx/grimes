# frozen_string_literal: true
#
class Admin::FriendsController < ApplicationAdminMemorialController
  before_action :authenticate_user!
  before_action :set_memorial

  # before_filter :store_current_location, :unless => :devise_controller?

  def index
    @memorial_users = Security::MemorialUser.where(memorial_id: params[:memorial_id]).joins(:user).includes(:user).order('security_memorial_users.admin desc, security_users.name, security_users.username')
    authorize @memorial, :update?
  end

  def create
    if params[:security_user_id].present?
      security_user_id = params[:security_user_id]
    else
      security_user_id = current_user.id
    end
    memorial_user = Security::MemorialUser.create(memorial_id: params[:memorial_id], user_id: security_user_id)
    if memorial_user.persisted?
      redirect_to stored_location_for(:user) || request.referer || root_path
    else
      memorial_user.save!
      raise memorial_user.errors.full_messages
    end

  end

  def destroy
    invite = Security::MemorialUser.find(params[:id])
    invite.destroy
    redirect_to stored_location_for(:user) || (policy(@memorial).edit? ? admin_memorial_invites_path(memorial_id: invite.memorial.id) : memorial_memorial_path(memorial_id: invite.memorial.id))
  end

  private

  def set_memorial
    @memorial ||= Memorial::Memorial.find(params[:memorial_id])
  end

  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(root_path)
  end
end
