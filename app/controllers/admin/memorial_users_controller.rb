class Admin::MemorialUsersController < ApplicationAdminMemorialController

  def update
    memorial_user = Security::MemorialUser.find_by(memorial_id: params[:memorial_id], id: params[:id])
    authorize memorial_user
    if memorial_user.update(admin: params[:admin])
      head :no_content
    else
      head status: 422
    end
  end

  def destroy
    memorial_user = Security::MemorialUser.find_by(memorial_id: params[:memorial_id], id: params[:id])
    memorial_user.destroy
    redirect_back(fallback_location: admin_memorial_friends_path)
  end

end
