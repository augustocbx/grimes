# frozen_string_literal: true
class ApplicationAdminMemorialController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit
  protect_from_forgery with: :exception, if: -> {
    !Rails.env.development?
  }
  layout 'application_admin_memorial'
  before_action :authenticate, if: -> {
    request.format.json?
  }
  before_action :authenticate_user!
  before_action :set_memorial_global, :set_user_memorials

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def set_memorial_global
    if current_user.present?
      @memorial = if params[:memorial_id].present?
                    Memorial::Memorial.unscoped.where(id: params[:memorial_id]).first
                  end
      if !@memorial.present? && params[:id]
        @memorial = Memorial::Memorial.unscoped.find_by(id: params[:id])
      end
    end
  end

  def set_user_memorials
    if current_user.present?
      @memorials_user = Memorial::Memorial.where(['security_user_id = :security_user_id ', security_user_id: current_user.id]).order(:mmry_firstname)
    end
  end

  def store_current_location
    store_location_for(:user, request.url)
  end

  def user_not_authorized
    flash[:alert] = "Você não está autorizado a editar este Memorial Vivo."
    redirect_to(root_path)
  end

  def authenticate
    authenticate_with_http_token do |token, options|
      # Compare the tokens in a time-constant manner, to mitigate
      # timing attacks.
      @current_user = Security::User.user_with_token(token)
      # ActiveSupport::SecurityUtils.secure_compare(token, TOKEN)
    end
    @current_user
  end
end
