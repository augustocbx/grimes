module Devise::DeviseRegistrationsControllerDecorator
  extend ActiveSupport::Concern

  included do
    alias :devise_new :new
  end

  def email(token = nil, sign_in = false)
    user = Security::User.new
    if token.blank?
      return ''
    else
      invite = Security::Invite.find_by(token: token)
      if invite.nil?
        return ''
      end
      email = invite.email
      user_exists = Security::User.find_by(email: email).present?
      if sign_in && !user_exists
        email = ''
      elsif !sign_in && user_exists
        email = ''
      end
      email
    end
  end
end
