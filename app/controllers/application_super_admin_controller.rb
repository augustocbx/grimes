# frozen_string_literal: true
class ApplicationSuperAdminController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  layout 'application_admin_memorial'
  before_action :authenticate_user!, :set_user_memorials

  private

  def set_user_memorials
    if current_user.present?
      @memorials_user = Memorial::Memorial.where(security_user_id: current_user.id).order(:mmry_firstname)
    end
  end
end
