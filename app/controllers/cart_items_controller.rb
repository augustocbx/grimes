# frozen_string_literal: true
class CartItemsController < ApplicationController
  include CartHelper

  def create
    @cart = current_cart
    @cart.add_item_cart(source, 1, receiver)
    @cart.save

    redirect_to financial_cart_path
  end

  def update
    @cart = current_cart
    @cart_item = @cart.financial_cart_items.find(params[:id])
    @cart_item.update_attributes(cart_item_params)
    @cart_items = @cart.financial_cart_items
    if @cart.financial_order.approved?
      admin_memorials_path
    else
      redirect_to financial_cart_path
    end
  end

  def destroy
    @cart = current_cart
    @cart_item = @cart.financial_cart_items.find(params[:id])
    @cart_item.destroy
    @cart_items = @cart.financial_cart_items
  end

  private

  def cart_item_params
    params.require(:financial_cart_item).permit(:financial_cart_id, :memorial_plan_id, :memorial_id)
  end

  def source
    var = if !cart_item_params[:memorial_plan_id].nil?
            Memorial::MemorialPlan.active.where(['id = :id and :today between mmrl_plan_vigence_start_date and mmrl_plan_vigence_end_date', today: Date.today, id: cart_item_params[:memorial_plan_id]]).first
          else
            raise 'Invalid source'
          end
    var
  end

  def receiver
    if !cart_item_params[:memorial_id].nil?
      @first = Memorial::Memorial.where(id: cart_item_params[:memorial_id], user: current_user).first
      @first
    else
      raise 'Invalid receiver'
    end
  end
end
