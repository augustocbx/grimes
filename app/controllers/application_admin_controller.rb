# frozen_string_literal: true
class ApplicationAdminController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit
  protect_from_forgery with: :exception
  layout 'application_admin_memorial'
  before_action :authenticate_user!, :set_user_memorials

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def set_user_memorials
    if current_user.present?
      @memorials_user = Memorial::Memorial.where(security_user_id: current_user.id).order(:mmry_firstname)
    end
  end

  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end
end
