# frozen_string_literal: true
class ApplicationMemorialController < ActionController::Base
  include Pundit
  layout 'application_memorial'
  protect_from_forgery with: :exception
  before_action :authenticate
  before_action :set_memorial, if: proc {|c| c.class.name != 'Memorial::MemorialsController'}

  before_action :store_current_location, :unless => :devise_controller?, except: :main_photo

  private

  def set_memorial
    begin
      @memorial = Memorial::Memorial.find(params[:memorial_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: {message: 'Memorial inexistente', memorial_id: params[:memorial_id]}, status: :not_found
      return
    end
  end

  def store_current_location
    store_location_for(:user, request.url)
  end

  def authenticate
    authenticate_with_http_token do |token, options|
      # Compare the tokens in a time-constant manner, to mitigate
      # timing attacks.
      @current_user = Security::User.user_with_token(token)
      # ActiveSupport::SecurityUtils.secure_compare(token, TOKEN)
    end
    @current_user
  end
end
