# frozen_string_literal: true
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit
  # protect_from_forgery with: :null_session
  before_action :set_user_memorials, :log_additional_data
  protect_from_forgery unless: -> { request.format.json? }


  def test_exception_notifier
    raise 'This is a test. This is only a test.'
  end

  private

  def after_sign_out_path_for(_resource_or_scope)
    if params[:back_to]
      # if params[:back_to].include?(/admin/memorials/a9d10ceb-b172-410b-af15-1e05e346f26f/photos/)
      if params[:back_to].match(/\/admin\/memorials\/[0-9abcdef\-]{36}/)
        id = params[:back_to].match(/\/admin\/memorials\/([0-9abcdef\-]{36})/)[1]
        memorial_memorial_path(id)
      else
        params[:back_to]
      end

    else
      root_path
    end
  end

  def after_sign_in_path_for(_resource_or_scope)
    if params[:back_to]
      params[:back_to]
    else
      if request.referer == login_url && current_user.present?
        memorials_user = Memorial::Memorial.where(['security_user_id = :security_user_id ', security_user_id: current_user.id]).order(:mmry_firstname)
        unless memorials_user.empty?
          return edit_admin_memorial_path(memorials_user.first.id, anchor: 'Biografia')
        end
      end
      if current_user.present?
        stored_location_for(:user) || new_memorial_path(anchor: 'new_memorial') || root_path
      else
        super
      end

    end
  end

  def set_user_memorials
    if current_user.present?
      @memorials_user ||= Memorial::Memorial.where(['security_user_id = :security_user_id ', security_user_id: current_user.id]).order(:mmry_firstname)
    end
  end

  def log_additional_data
    request.env["exception_notifier.exception_data"] = {
      :current_user => current_user
    }
  end
end
