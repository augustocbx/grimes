# frozen_string_literal: true
class Pagseguro::NotificationsController < ApplicationController
  # skip_before_action :verify_authenticity_token, only: :create

  def create
    transaction = PagSeguro::Transaction.find_by_notification_code(params[:notificationCode])

    if transaction.errors.empty?
      # Processa a notificacao. A melhor maneira de se fazer isso eh realizar
      # o processamento em background. Uma boa alternativa para isso eh a
      # biblioteca Sidekiq.
    end

    order = Financial::Order.includes(:financial_order_items).find(transaction.reference)

    all_itens_ok = transaction.items.map(&:id).sort == order.financial_order_items.map(&:id).sort

    case transaction.status.id
    when '3', '4' # Paid
      if all_itens_ok
        order.approve!
        order.activate_coupons!
      end
    when '6', '7' # Not paid
      order.reprove!
      order.inactivate_coupons!
    end

    head :ok
  end
end
