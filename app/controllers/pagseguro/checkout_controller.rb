# frozen_string_literal: true
class Pagseguro::CheckoutController < ApplicationController
  include CartHelper
  before_action :authenticate_user!, only: [:create]

  def create
    # O modo como voce ira armazenar os produtos que estao sendo comprados
    # depende de voce. Neste caso, temos um modelo Order que referencia os
    # produtos que estao sendo comprados.
    cart = current_cart
    order = cart.generate_order
    if order.approved?
      cart.cnnl_cart_current = false
      cart.save

      if session[:new_memorial]['id'] == cart.financial_cart_items.first.fnnl_cart_item_receiver.id
        session[:new_memorial] = {}
      end

      select_redirection_target(cart, order)
      return
    end

    payment = PagSeguro::PaymentRequest.new
    payment.reference = order.id
    # payment.notification_url = pagseguro_pagseguro_notifications_create_url
    if !order.financial_order_items.empty?
      if session[:new_memorial] || session[:new_memorial]['id'] == order.financial_order_items.first.fnnl_ordr_item_receiver.id
        session[:new_memorial] = {}
      end
      payment.redirect_url = admin_painel_url(memorial_id: order.financial_order_items.first.fnnl_ordr_item_receiver.id)

      payment.sender = {
        name: current_user.name,
        email: current_user.email
      }


      order.financial_order_items.each do |item|
        payment.items << {
          id: item.id,
          description: item.title,
          amount: item.final_price,
          quantity: item.quantity
        }
      end

      # Caso voce precise passar parametros para a api que ainda nao foram mapeados na gem,
      # voce pode fazer de maneira dinamica utilizando um simples hash.
      # payment.extra_params << { paramName: 'paramValue' }
      # payment.extra_params << { senderBirthDate: '07/05/1981' }
      # payment.extra_params << { extraAmount: '-15.00' }

      response = payment.register

      # Caso o processo de checkout tenha dado errado, lanca uma excecao.
      # Assim, um servico de rastreamento de excecoes ou ate mesmo a gem
      # exception_notification podera notificar sobre o ocorrido.
      #
      # Se estiver tudo certo, redireciona o comprador para o PagSeguro.
      if response.errors.any?
        raise response.errors.join("\n")
      else
        redirect_to response.url
      end
    end
  end

  private

  def select_redirection_target(current_cart, order)
    if current_cart.financial_cart_items.size > 1
      redirect_to admin_memorials_path
    elsif order.financial_order_items.size == 1
      redirect_to edit_admin_memorial_path(id: order.financial_order_items.first.fnnl_ordr_item_receiver.id)
    elsif current_cart.financial_cart_items.empty?
      flash[:notice] = 'Pedido invalido, confira o memorial e selecione um plano'
      redirect_to new_memorial_step2_path
    else
      order.cancel!
      raise "Should not be here! You have no itens for the order: #{order.fnnl_ordr_number}"
    end
  end
end
