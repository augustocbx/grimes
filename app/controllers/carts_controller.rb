# frozen_string_literal: true
class CartsController < ApplicationController
  before_action :authenticate_user!, only: [:show]
  include CartHelper
  def show
    @cart = current_cart
    if !@cart.present? || @cart.financial_cart_items.empty?
      redirect_to controller: :home, action: :new_memorial, anchor: 'new_memorial'
    end
  end
end
