# frozen_string_literal: true
class MemorialPhotosController < ApplicationController
  before_action :authenticate_user!, only: [:image_remove]

  def update
    thumb = Memorial::MemorialPhoto.find(params[:id]).image
    send_file thumb.url(version), type: thumb.content_type, disposition: 'inline'
  end

  def image
    version = (params[:version] if params[:version])
    photo = Memorial::MemorialPhoto.find(params[:id])
    thumb = photo.image
    http_cache_forever(public: true) do
      send_file thumb.url(version), type: thumb.content_type, disposition: 'inline'
    end

  end

  def image_remove
    memorial_photo = Memorial::MemorialPhoto.find(params[:id])
    memorial_photo.destroy if memorial_photo.memorial.user == current_user
    redirect_to new_memorial_path(anchor: 'biografia_fotos')
  end
end
