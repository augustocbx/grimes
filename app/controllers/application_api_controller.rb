# frozen_string_literal: true
class ApplicationApiController < ActionController::API
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit
  before_action :set_user_memorials
  before_action :log_additional_data
  # acts_as_token_authentication_handler_for Security::User, as: :user

  def set_user_memorials
    puts 'set_user_memorials'
    if current_user.present?
      @memorials_user ||= Memorial::Memorial.where(['security_user_id = :security_user_id ', security_user_id: current_user.id]).order(:mmry_firstname)
    end
  end

  def log_additional_data
    if current_user.present?
      request.env["exception_notifier.exception_data"] = {
        :current_security_user => current_user
      }
    end
  end

  def authorize_request
    header = request.headers['Authorization']
    token = header.split(' ').last if header
    begin
      @current_user = Security::User.user_with_token(token)
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end
  end

  def authenticate
    authenticate_with_http_token do |token, options|
      # Compare the tokens in a time-constant manner, to mitigate
      # timing attacks.
      @current_user = Security::User.user_with_token(token)
      # ActiveSupport::SecurityUtils.secure_compare(token, TOKEN)
    end
    @current_user
  end
end
