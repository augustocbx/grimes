class Memorial::EventsController < ApplicationMemorialController
  before_action :authenticate_user!, only: [:create, :destroy, :update]

  def index
    @events = Memorial::Event.where(memorial_id: params[:memorial_id])
    @event = Memorial::Event.new(memorial_id: params[:memorial_id])
  end

  def create
    @event = Memorial::Event.new(event_params[:memorial_event].merge(user_id: current_user.id, memorial_id: params[:memorial_id]))
    if @event.save
      flash[:notice] = "O evento foi criado"
    else
      flash[:alert] = @event.error_messages
    end
    redirect_back(fallback_location: memorial_events_path(memoria_id: params[:memorial_id]))
  end

  def update
    event = Memorial::Event.find_by(memorial_id: params[:memorial_id], id: params[:id])
    authorize event
    if event.update(event_params[:memorial_event])
      head :no_content
    else
      head status: 422
    end
  end

  private

  def event_params
    params.permit(:id, memorial_event: [:memorial_id, :user_id, :mmrl_evnt_title, :mmrl_evnt_headline, :mmrl_evnt_description, :mmrl_evnt_where, :mmrl_evnt_address, :mmrl_evnt_when_date, :mmrl_evnt_when_time])
  end
end
