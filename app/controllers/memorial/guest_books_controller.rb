# frozen_string_literal: true
class Memorial::GuestBooksController < ApplicationMemorialController
  before_action :authenticate_user!, only: :create
  def index
    @guest_books = Memorial::GuestBook.where(memorial_id: params[:memorial_id]).includes(:user)
    @guest_book = Memorial::GuestBook.new(memorial_id: params[:memorial_id])
  end

  def create
    @guest_book = Memorial::GuestBook.new(guest_book_params[:memorial_guest_book].merge(user_id: current_user.id, memorial_id: params[:memorial_id]))
    if @guest_book.save
      flash[:notice] = "Livro de visitas assinado por você"
    else
      flash[:alert] = @guest_book.error_messages
    end
    redirect_back(fallback_location: memorial_guest_books_path)
  end

  def destroy
    guest_book = Memorial::GuestBook.find(params[:id])
    authorize guest_book
    guest_book.destroy
    redirect_to memorial_guest_books_path(memorial_id: guest_book.memorial.id)
  end

  private

  def guest_book_params
    params.permit(:id, memorial_guest_book: [:memorial_id, :guest_book_message])
  end
end
