# frozen_string_literal: true
class Memorial::MemorialsController < ApplicationMemorialController

  def main_photo
    version = params[:version] if !params[:version].blank?
    thumb = Memorial::Memorial.find(params[:id]).photo
    version = version.to_s.gsub(/[^0-9a-z\_\-]/i, '').downcase
    begin
      if !File.exist?(thumb.url(version))
        file_not_found  = Rails.root.join('app/assets/images/no_image.png')
        send_file file_not_found, type: thumb.content_type, disposition: 'inline'
        return
      end
      send_file thumb.url(version), type: thumb.content_type, disposition: 'inline'
    rescue ArgumentError => e
      head :not_found
    end
  end

  def show
    begin
      @memorial = Memorial::Memorial.unscoped.find(params[:id])
      @memorial.mmry_visits = @memorial.mmry_visits + 1
      @memorial.save(validate: false)
    rescue ActiveRecord::RecordNotFound => e
      @memorial = Memorial::Memorial.unscoped.find(params[:id])
      if !@memorial.present?
        raise e
      end
      redirect_to edit_admin_memorial_memorial_path(memorial_id: @memorial.id) and return
    end
  end

  def index
    @memorials_following = []
    if current_user.present?
      if params[:filter_my_memorials]
        @memorials_following = Memorial::Memorial.left_joins(:security_memorial_users).where('((security_memorial_users.user_id = :current_user and security_memorial_users.admin) or memorial_memorials.security_user_id = :current_user)', current_user: current_user, security_memorial_users: {user_id: current_user.id}).order("memorial_memorials.mmry_firstname asc, memorial_memorials.mmry_lastname asc").includes(:user).uniq
      elsif params[:filter_following]
        @memorials_following = Memorial::Memorial.left_joins(:security_memorial_users).where('(security_memorial_users.user_id = :current_user or memorial_memorials.security_user_id = :current_user) and photo is not null', current_user: current_user, security_memorial_users: {user_id: current_user.id}).order("memorial_memorials.mmry_firstname asc, memorial_memorials.mmry_lastname asc").includes(:user).uniq
      end
    end
    respond_to do |f|
      f.html do
        if !@memorials_following.empty?
          @memorials = @memorials_following
        else
          @memorials = Memorial::Memorial.includes(:user).where('memorial_memorials.photo is not null').order('memorial_memorials.mmry_firstname asc, memorial_memorials.mmry_lastname asc')
        end
      end
      f.json do
        if params[:filter_following].blank? && params[:filter_my_memorials].blank?
          @memorials = Memorial::Memorial.includes(:user).where('memorial_memorials.photo is not null').order('memorial_memorials.mmry_firstname asc, memorial_memorials.mmry_lastname asc')
        else
          @memorials = @memorials_following
        end
      end
    end
    render :index
  end
end
