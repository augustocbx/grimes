# frozen_string_literal: true
class Memorial::PhotosController < ApplicationMemorialController

  def index
    fresh_when last_modified: @memorial.updated_at.utc, strong_etag: @memorial
  end
end
