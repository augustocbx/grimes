class Memorial::FollowController < ApplicationApiController
  before_action :authorize_request, only: [:create, :destroy]

  def create
    security_user_id = current_user.id
    memorial_user = Security::MemorialUser.create(memorial_id: params[:id], user_id: security_user_id)
    if memorial_user.persisted?
      render json: {success: true} and return
    end
    render json: {success: false} and return
  end

  def destroy
    security_user_id = current_user.id
    memorial = Memorial::Memorial.where(id: params[:id]).first
    if memorial && memorial.security_user_id == security_user_id
      render json: {success: false, message: 'Você não pode deixar de seguir o seu próprio memorial'} and return
    end
    memorial_user = Security::MemorialUser.where(memorial_id: params[:id], user_id: security_user_id).first
    memorial_user.destroy
    if memorial_user.deleted?
      render json: {success: true} and return
    end
    render json: {success: false} and return
  end

  private

  def follow_params
    params.permit(:id)
  end

end
