# frozen_string_literal: true
class Memorial::InviteFriendsController < ApplicationMemorialController
  def create
    @invite = Security::Invite.new(invite_params[:security_invite]) # Make a new Invite
    @invite.sender_id = current_user.id # set the sender to the current user
    if @invite.save
      InviteMailer.new_user_invite(@invite, new_user_registration_url(invite_token: @invite.token)).deliver # send the invite data to our mailer to deliver the email
      flash[:notice] = "Convite enviado com sucesso para: #{@invite.email}"
    else
      flash[:alert] = "Não foi possível enviar um convite para #{@invite.email}. "
      flash[:alert] << @invite.errors.full_messages.join(' ')
    end
    redirect_back(fallback_location: admin_memorial_friends_path)
  end

  def index
    @invite = Security::Invite.new(memorial_id: @memorial.id)
  end

  private

  def invite_params
    params.permit(:id, security_invite: [:memorial_id, :email])
  end
end
