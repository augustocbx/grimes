# frozen_string_literal: true
class Users::RegistrationsController < Devise::RegistrationsController
  # before_filter :configure_sign_up_params, only: [:create]
  # before_filter :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    @token = params[:invite_token]
    super
  end

  # POST /resource
  def create
    build_resource(sign_up_params)
    invite_token = params[:invite_token]

    if resource.save && invite_token.present?
      resource.add_to_memorial_by_token(invite_token)
    end

    yield resource if block_given?
    if resource.persisted?

      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      flash[:alert] = resource.errors.full_messages.join(', ') if is_flashing_format?
      # redirect_back(fallback_location: new_user_registration_path)
      #


      respond_to do |f|
        f.html do
          redirect_back(fallback_location: new_memorial_path)
        end
        f.json do
          render json: {message: 'Usuário não criado'}, status: :forbidden
        end
      end

    end
  end

  # def login
  #
  #   render :login
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:user) << :name
  end

  def build_resource(hash=nil)
    super
    # Create an instance var to use just for the sign up form
    @sign_up_user = self.resource.dup
    @sign_up_user.email = email(params[:invite_token], false)
    @sign_in_user = self.resource.dup
    @sign_in_user.email = email(params[:invite_token], true)
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.for(:account_update) << :attribute
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
