# frozen_string_literal: true
require 'google/apis/oauth2_v2'
class Users::DeviceAuthenticationController < ApplicationApiController

  skip_before_action :set_user_memorials, only: [:sign_in_with_token, :sign_up_with_firebase]

  def sign_in_with_token
    if params[:provider] == 'firebase'
      password = params[:password] if params[:password].present?
      client = FirebaseTokenAuth.build
      begin
        token_info = client.verify_id_token(params[:token]).id_token.payload.symbolize_keys
        if token_info.present? && token_info[:user_id] == params[:client_id]
          user = Security::User.from_firebase_by_token(params[:token], token_info, params[:name], params[:device_id], password)
        end
      rescue Google::Apis::ClientError => e
        render json: {
          message: 'not found!',
          error: true,
        }, status: :not_acceptable and return
      end
    elsif params[:provider] == 'google_oauth2'
      oauth_service = Google::Apis::Oauth2V2::Oauth2Service.new
      begin
      token_info = oauth_service.tokeninfo(access_token: params[:token]).new
      rescue Google::Apis::ClientError => e
        render json: {
            message: 'not found!',
            error: true,
        }, status: :not_acceptable and return
      end
      if token_info.present? && token_info.user_id == params[:client_id]
        flutter_login_params = Hashie::Mash.new({
                                                    client_id: params[:client_id],
                                                    provider: params[:provider],
                                                    name: params[:name],
                                                    email: params[:email],
                                                    photo: params[:photo],
                                                    token: params[:token],
                                                })
        user = Security::User.from_flutter_by_token(flutter_login_params)
      end
    elsif params[:provider] == 'email'
      flutter_login_params = Hashie::Mash.new({
                                                  client_id: params[:client_id],
                                                  provider: params[:provider],
                                                  name: params[:name],
                                                  email: params[:email],
                                                  photo: params[:photo],
                                                  token: params[:token],
                                              })
      user = Security::User.from_flutter_by_token(flutter_login_params)
    elsif params[:provider] == 'facebook'
      face_graph_url = "https://graph.facebook.com/v8.0/me?fields=name,first_name,last_name,email&access_token=#{params[:token]}"
      body = Net::HTTP.get(URI(face_graph_url))
      face_obj = eval(body)
      if face_obj.present? && face_obj[:id] == params[:client_id]
        flutter_login_params = Hashie::Mash.new({
                                                    client_id: params[:client_id],
                                                    provider: params[:provider],
                                                    name: params[:name],
                                                    email: params[:email],
                                                    photo: params[:photo],
                                                    token: params[:token],
                                                })
        user = Security::User.from_flutter_by_token(flutter_login_params)
      end
    end
    if user
      render json: user.as_json(only: [:id, :username, :name, :about, :location, :email, :superuser], methods: [:image_url, :memorials_owner, :memorials_following]).merge({
                                                                                                                                       message: 'Usuário válido!',
                                                                                                                                       client_id: params[:client_id],
                                                                                                                                       token: params[:token],
                                                                                                                                       provider: params[:provider],
                                                                                                                                   }), status: :ok
      return
    end

    render json: {
        message: 'not found!',
        error: true,
    }, status: :not_acceptable

  end

  def sign_up_with_firebase
    password = params[:password] if params[:password].present?
    client = FirebaseTokenAuth.build
    begin
      token_info = client.verify_id_token(params[:token]).id_token.payload.symbolize_keys
      if token_info.present? && token_info[:user_id] == params[:client_id]
        user = Security::User.from_firebase_by_token(params[:token], token_info, params[:name], params[:device_id], password)
      end
    rescue Google::Apis::ClientError => e
      render json: {
        message: 'not found!',
        error: true,
      }, status: :not_acceptable and return
    end
    if user
      render json: user.as_json(only: [:id, :username, :name, :about, :location, :email, :superuser], methods: [:image_url, :memorials_owner, :memorials_following]).merge({
                                                                                                                                     message: 'Usuário válido!',
                                                                                                                                     client_id: token_info[:user_id],
                                                                                                                                     token: params[:token],
                                                                                                                                     provider: 'firebase',
                                                                                                                                   }), status: :ok
      return
    end

    render json: {
      message: 'not found!',
      error: true,
    }, status: :not_acceptable

  end

  def sign_up_with_email
    sleep 3
    if params[:provider] == 'email'
      if sign_up_with_email_params[:name].present? && sign_up_with_email_params[:email].present? && sign_up_with_email_params[:password].present?
        flutter_signup = Hashie::Mash.new({
                                                    provider: params[:provider],
                                                    name: params[:name],
                                                    email: params[:email],
                                                    password: params[:password],
                                                })
        begin
          user = Security::User.create_from_flutter_by_email(flutter_signup)
        rescue StandardError => e
          puts e
          p e
          puts e.message
          puts e.backtrace
          render json: {
              error: true,
              message: e.message,
              email: params[:email],
          }, status: :unauthorized
          return
        end

        if user.valid?
          render json: user.as_json(only: [:id, :username, :name, :about, :location, :email, :superuser], methods: [:image_url]).merge({
                                                                                                                                           message: 'Usuário válido!',
                                                                                                                                           client_id: params[:client_id],
                                                                                                                                           token: user.last_token,
                                                                                                                                           provider: params[:provider],
                                                                                                                                       }), status: :ok
        else
          render json: {
              error: true,
              message: user.errors.full_messages,
              email: params[:email],
          }, status: :unauthorized
          return

        end
        return
      end
    end

    render json: {
        message: 'not found!',
        error: true,
    }, status: :not_acceptable

  end

  def sign_in_with_email
    if params[:provider] == 'email'
      if sign_in_with_email_params[:email].present? && sign_in_with_email_params[:password].present?
        flutter_signin = Hashie::Mash.new({
                                                    provider: sign_in_with_email_params[:provider],
                                                    email: sign_in_with_email_params[:email],
                                                    device_id: sign_in_with_email_params[:device_id],
                                                    password: sign_in_with_email_params[:password],
                                                })
        begin
          user = Security::User.signin_from_flutter_by_email(flutter_signin)
        rescue StandardError => e
          puts e
          p e
          puts e.message
          puts e.backtrace
          render json: {
              error: true,
              message: e.message,
              email: params[:email],
          }, status: :unauthorized
          return
        end

        if user.valid?
          render json: user.as_json(only: [:id, :username, :name, :about, :location, :email, :superuser], methods: [:image_url]).merge({
                                                                                                                                           message: 'Usuário válido!',
                                                                                                                                           client_id: params[:client_id],
                                                                                                                                           token: user.last_token,
                                                                                                                                           provider: params[:provider],
                                                                                                                                       }), status: :ok
        else
          render json: {
              error: true,
              message: user.errors.full_messages,
              email: params[:email],
          }, status: :unauthorized
          return

        end
        return
      end
    end

    render json: {
        message: 'not found!',
        error: true,
    }, status: :not_acceptable

  end

  def sign_up_with_email_params
    params.permit(:provider, :name, :email, :password, :device_id)
  end

  def sign_in_with_email_params
    params.permit(:provider, :email, :password, :device_id)
  end
end
