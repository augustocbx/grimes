# frozen_string_literal: true
class Users::SessionsController < Devise::SessionsController
  respond_to :html, :json
  include DeviseTokenAuth::Concerns::SetUserByToken
  # skip_before_action :verify_authenticity_token, if: Proc.new { |c| c.request.format.json? }
  # before_filter :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  #   # redirect_to new_memorial_path
  # end

  # POST /resource/sign_in
  def create
    p params
    sign_out(resource_name)
    super
    if params[:invite_token]
      self.resource.add_to_memorial_by_token(params[:invite_token])
    end
    respond_to do |f|
      f.html
      f.json do
        if self.resource
        else
          render json: {error: 'Usuario/senha inválida'}
        end
      end
    end
  end

  # DELETE /resource/sign_out
  def destroy
    super
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
