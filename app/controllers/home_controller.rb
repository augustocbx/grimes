# frozen_string_literal: true
class HomeController < ApplicationController
  include UserHelper

  respond_to :html

  before_action :authenticate_user!, only: [:new_memorial_step2, :new_memorial_step3]
  before_action :set_plans, only: [:new_memorial, :new_memorial_step2, :new_memorial_step3]
  before_action :set_invite, only: :login

  def index
    @memorials_added = []
    if current_user.present?
      @memorials_added = Memorial::Memorial.left_joins(:security_memorial_users).where('security_memorial_users.user_id = :current_user or memorial_memorials.security_user_id = :current_user and photo is not null', current_user: current_user, security_memorial_users: {user_id: current_user.id}).order("memorial_memorials.mmry_firstname asc, memorial_memorials.mmry_lastname asc").includes(:user).uniq
    end
    if !@memorials_added.empty?
      @memorials = Memorial::Memorial.where('memorial_memorials.id not in(:ids) and photo is not null', ids: @memorials_added.map(&:id)).includes(:user).order('memorial_memorials.mmry_firstname asc, memorial_memorials.mmry_lastname asc')
    else
      @memorials = Memorial::Memorial.includes(:user).where('memorial_memorials.photo is not null').order('memorial_memorials.mmry_firstname asc, memorial_memorials.mmry_lastname asc')
    end
    respond_to do |f|
      f.html
      f.json
    end

  end

  def about_us
    render 'home/about_us'
  end

  def new_memorial
    session[:new_memorial] ||= {}
    memorial_id = session[:new_memorial]['id']
    if memorial_id.blank?
      @memorial = Memorial::Memorial.new(session[:new_memorial])
    elsif memorial_id
      begin
        @memorial = Memorial::Memorial.find(memorial_id)
        if @memorial.memorial_plan.present?
          session[:new_memorial] = {}
          @memorial = Memorial::Memorial.new(session[:new_memorial])
        end
      rescue ActiveRecord::RecordNotFound
        session.delete(:new_memorial)
        redirect_to new_memorial_path
        return
      end
    end
    5.times do
      @memorial.memorial_photos.build
    end
    render :new_memorial
  end

  def new_memorial_step2
    memorial_id = session[:new_memorial]['id']
    session[:new_memorial] = memorial_params.to_h

    if memorial_id.blank?
      @memorial = Memorial::Memorial.new(session[:new_memorial].merge(security_user_id: current_user.id))
      session[:new_memorial] = @memorial.attributes
      salvo = @memorial.save
    else
      @memorial = Memorial::Memorial.find(memorial_id)
      salvo = @memorial.update(memorial_params)
    end

    if salvo
      session[:new_memorial] = @memorial.attributes
      redirect_to new_memorial_path(anchor: 'biografia_fotos')
    else
      render :new_memorial
    end
  end

  def new_memorial_step3
    memorial_id = session[:new_memorial]['id']
    @memorial = Memorial::Memorial.find(memorial_id)
    if @memorial.update(memorial_params) && @memorial.ready_to_publish?
      @memorial.reload
      redirect_to new_memorial_path(anchor: 'memorial_plan')
    else
      if @memorial.mmry_biografy.to_s.strip.blank?
        flash[:alert] = 'Você deve colocar uma biografia mínima'
      end
      redirect_to new_memorial_path(anchor: 'biografia_fotos')
    end
  end

  def plans
    @memorial_plans = Memorial::MemorialPlan.where(mmrl_plan_active: true).where(':hoje between mmrl_plan_vigence_start_date and mmrl_plan_vigence_end_date', hoje: Date.today).order('mmrl_plan_free_default desc, mmrl_plan_price asc')
    free_plan = false
    @memorial_plans.each do |memorial_plan|
      if !memorial_plan.mmrl_plan_free_default && memorial_plan.price == 0.00
        free_plan = true
      end
    end
    if free_plan
      @memorial_plans = @memorial_plans.select{|memorial_plan| !memorial_plan.mmrl_plan_free_default}
    end
    @memorial_plans
  end

  def login
    @token = params[:invite_token]
    render :login
  end

  def contact
    render :contact
  end

  def image_url
    render :image_url
  end

  def user_photo
    version = (params[:version] if params[:version])
    thumb = Security::User.find(params[:id]).image
    if !thumb.url.nil?
      send_file thumb.url(version), type: thumb.content_type, disposition: 'inline'
    else
      send_file Rails.root.join('app', 'assets', 'images', 'no_image.png'), type: 'image/png', disposition: 'inline'
    end
  end

  private

  def memorial_params
    params.require(:memorial_memorial).permit(:mmry_firstname, :mmry_lastname, :mmry_gender, :mmry_birth_date, :mmry_birth_location, :mmry_death_date, :mmry_death_location, :mmry_biografy, :mmry_cause_of_death, memorial_photos_attributes: [:image])
  end

  def memorial_params_upload
    params.require(:memorial_memorial).permit(memorial_photos_attributes: [:image])
  end

  def set_plans
    @memorial_plans = Memorial::MemorialPlan.where(mmrl_plan_active: true).where(':hoje between mmrl_plan_vigence_start_date and mmrl_plan_vigence_end_date', hoje: Date.today).order('mmrl_plan_free_default desc, mmrl_plan_price asc')
  end

  def set_invite
    @invite = Security::Invite.find_by(token: params[:invite_token])
  end
end
