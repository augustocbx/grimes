# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorial_photos
#
#  id                    :uuid             not null, primary key
#  memorial_id           :uuid             not null
#  mmry_phto_title       :string
#  mmry_phto_description :string
#  image                 :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class Memorial::MemorialPhoto < ApplicationRecord
  belongs_to :memorial, -> { with_deleted }, class_name: 'Memorial::Memorial', foreign_key: :memorial_id, optional: true

  mount_uploader :image, MemorialPhotosUploader

  validates_presence_of :image
end
