# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorials
#
#  id                  :uuid             not null, primary key
#  security_user_id    :uuid             not null
#  mmry_firstname      :string           not null
#  mmry_lastname       :string           not null
#  mmry_nicknames      :string
#  mmry_gender         :string
#  mmry_biografy       :text
#  mmry_birth_date     :date             not null
#  mmry_birth_location :string           not null
#  mmry_death_date     :date             not null
#  mmry_death_location :string           not null
#  mmry_cause_of_death :string
#  mmry_tombstone_geo  :string
#  mmry_privacy        :integer
#  photo               :string
#  enabled             :boolean          default(TRUE), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  deleted_at          :datetime
#  mmry_visits         :integer          default(0), not null
#
# Indexes
#
#  index_memorial_memorials_on_deleted_at  (deleted_at)
#

require 'jbuilder'

class Memorial::Memorial < ApplicationRecord
  include Bootsy::Container

  has_many :memorial_photos, -> { order('memorial_memorial_photos.created_at') }, class_name: 'Memorial::MemorialPhoto'
  has_many :memorial_photos, -> { order('memorial_memorial_photos.created_at') }, class_name: 'Memorial::MemorialPhoto'
  has_many :memorial_guest_books, -> { order('memorial_guest_books.created_at desc') }, :class_name => 'Memorial::GuestBook'

  has_one :memorial_plan_coupon, -> { where(['mmrl_plan_cpon_used and mmrl_plan_cpon_active and :today between mmrl_plan_cpon_vigence_start_date and mmrl_plan_cpon_vigence_end_date', today: Date.today]) }, class_name: 'Memorial::MemorialPlanCoupon'
  has_many :memorial_plan_coupons_unused, -> { where(['not mmrl_plan_cpon_used and mmrl_plan_cpon_active', today: Date.today]) }, class_name: 'Memorial::MemorialPlanCoupon'
  has_one :memorial_plan, class_name: 'Memorial::MemorialPlan', through: :memorial_plan_coupon

  # INVITES
  has_many :invites, class_name: 'Security::Invite'

  # EVENTS
  has_many :events, :class_name => 'Memorial::Event'

  belongs_to :user, class_name: 'Security::User', foreign_key: :security_user_id, optional: true
  has_many :security_memorial_users, class_name: 'Security::MemorialUser'
  has_many :users, class_name: 'Security::User', through: :security_memorial_users

  accepts_nested_attributes_for :memorial_photos, allow_destroy: false

  validates_presence_of :mmry_firstname, :mmry_lastname, :mmry_birth_date, :mmry_birth_location, :mmry_death_date, :mmry_death_location, :mmry_gender

  acts_as_paranoid

  mount_uploader :photo, MemorialPhotosUploader

  def current_memorial_plan
    if memorial_plan.present?
      memorial_plan
    elsif !memorial_plan_coupons_unused.empty?
      memorial_plan_coupom = memorial_plan_coupons_unused.first
      memorial_plan_coupom.apply_to!(self)
      reload
      memorial_plan_coupom.memorial_plan
    else
      Memorial::MemorialPlan.free_plan
    end
  end

  def use_coupom(memorial_plan_coupon)
  end

  def ready_to_publish?
    !mmry_biografy.to_s.strip.blank?
  end

  def title
    "#{mmry_firstname} #{mmry_lastname}"
  end

  def main_photo_thumb_url
    if present? && photo.present?
      photo.public_url(:thumb)
    else
      ActionController::Base.helpers.asset_url('no_image.png')
    end
  end

  def social_description
    "#{I18n.l(mmry_birth_date)} - #{I18n.l(mmry_death_date)}. #{mmry_biografy}"
  end

  def social_thumb_url
    if present? && photo.present?
      photo.public_url(:social_share)
    elsif !memorial_photos.empty?
      memorial_photos.first.image.public_url(:social_share)
    else
      ActionController::Base.helpers.asset_url('no_image.png')
    end
  end

  def card_thumb_url
    if present? && photo.present?
      photo.public_url(:card_thumb)
    elsif !memorial_photos.empty?
      memorial_photos.first.image.public_url(:card_thumb)
    else
      ActionController::Base.helpers.asset_url('no_image.png')
    end
  end

  def thumb_url
    if present? && photo.present?
      photo.public_url(:thumb)
    elsif !memorial_photos.empty?
      memorial_photos.first.image.public_url(:thumb)
    else
      ActionController::Base.helpers.asset_url('no_image.png')
    end
  end

  def email_added?(email)
    self.users.map(&:email).include?(email)
  end

  def email_added?(email)
    self.users.map(&:email).include?(email)
  end

  def self.search(current_user, str)
    if current_user.superuser?
      Memorial::Memorial.where('mmry_firstname ilike :str OR mmry_lastname ilike :str OR mmry_nicknames ilike :str OR mmry_biografy ilike :str', str: "%#{str}%")
    else
      Memorial::Memorial.where(security_user_id: current_user).where('mmry_firstname ilike :str OR mmry_lastname ilike :str OR mmry_nicknames ilike :str OR mmry_biografy ilike :str', str: "%#{str}%")
    end
  end

  def owners
    ([self.user] + self.users.where('security_memorial_users.admin')).map do |user|
      user.security_authorizations.map(&:uid)
    end.flatten.compact.uniq
  end

  def followings(remove_owners = true)
    followings = ([self.user] + self.users).map do |user|
      user.security_authorizations.map(&:uid)
    end.flatten.compact.uniq
    if remove_owners
      followings = followings - self.owners
    end
    followings
  end

  def to_builder(user)
    context = ApplicationController.view_context_class.new("#{Rails.root}/app/views")
    JSON.parse(JbuilderTemplate.encode(context) do |json|
      json.partial! 'memorial/memorials/show', memorial: self, current_user: user
    end)
  end
end
