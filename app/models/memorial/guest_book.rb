# == Schema Information
#
# Table name: memorial_guest_books
#
#  memorial_id        :uuid             not null
#  user_id            :uuid             not null
#  guest_book_message :text
#  deleted_at         :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  id                 :uuid             not null, primary key
#

class Memorial::GuestBook < ApplicationRecord

  belongs_to :user, class_name: 'Security::User'
  belongs_to :memorial, -> { with_deleted }, class_name: 'Memorial::Memorial'

  validates :user, presence: true
  validates :memorial, presence: true

  validate :check_just_one_guest_book_by_day

  def error_messages
    message = String.new
    message << errors.full_messages.join(' ')
    message
  end

  private

  def check_just_one_guest_book_by_day
    guest_book = Memorial::GuestBook.where("memorial_id = :memorial_id and user_id = :user_id and created_at between :start_of_day and :end_of_day and (guest_book_message is null or guest_book_message = '')", memorial_id: memorial_id, user_id: user_id, start_of_day: DateTime.now.beginning_of_day, end_of_day: DateTime.now.end_of_day)
    if guest_book.present? && guest_book_message.blank?
      errors.add(:user_id, 'Você só pode assinar o livro de visitas uma vez por dia. Deixe uma mensagem.')
    end
  end
end
