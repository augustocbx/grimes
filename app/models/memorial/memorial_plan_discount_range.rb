# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorial_plan_discount_ranges
#
#  id                           :uuid             not null, primary key
#  memorial_plan_id             :uuid             not null
#  mmrl_plan_dsct_rnge_status   :boolean          not null
#  mmrl_plan_dsct_rnge_min      :integer          not null
#  mmrl_plan_dsct_rnge_discount :float            not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#

class Memorial::MemorialPlanDiscountRange < ApplicationRecord
  belongs_to :memorial_plan, class_name: 'Memorial::MemorialPlan', foreign_key: :memorial_plan_id

  # usar .unscoped para pegar tudo
  default_scope -> { where(mmrl_plan_dsct_rnge_status: true) }
  scope :inactive, -> { where(mmrl_plan_dsct_rnge_status: true) }

  validates_presence_of :memorial_plan_id, :mmrl_plan_dsct_rnge_min, :mmrl_plan_dsct_rnge_discount
  validates_numericality_of :mmrl_plan_dsct_rnge_discount, greater_than_or_equal_to: 1.00
  validates_numericality_of :mmrl_plan_dsct_rnge_min, greater_than_or_equal_to: 1
  validates_uniqueness_of :mmrl_plan_dsct_rnge_min, scope: :memorial_plan_id, conditions: -> { where(mmrl_plan_dsct_rnge_status: true) }
end
