# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorial_plans
#
#  id                           :uuid             not null, primary key
#  mmrl_plan_free_default       :boolean          default(FALSE), not null
#  mmrl_plan_title              :string           not null
#  mmrl_plan_description        :text             not null
#  mmrl_plan_validade_days      :integer          not null
#  mmrl_plan_activation_days    :integer          not null
#  mmrl_plan_videos_limit       :integer          not null
#  mmrl_plan_photos_limit       :integer          not null
#  mmrl_plan_support            :boolean          default(FALSE), not null
#  mmrl_plan_price              :float            not null
#  mmrl_plan_active             :boolean          not null
#  mmrl_plan_vigence_start_date :date             not null
#  mmrl_plan_vigence_end_date   :date
#  mmrl_plan_color              :string           default("green"), not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#

class Memorial::MemorialPlan < ApplicationRecord
  has_many :memorial_memorial_plan_discount_ranges, -> { order 'mmrl_plan_dsct_rnge_min' }, class_name: 'Memorial::MemorialPlanDiscountRange'

  validates_presence_of :mmrl_plan_title, :mmrl_plan_description, :mmrl_plan_validade_days, :mmrl_plan_activation_days, :mmrl_plan_videos_limit, :mmrl_plan_photos_limit, :mmrl_plan_price
  validates_length_of :mmrl_plan_title, minimum: 4
  validates_length_of :mmrl_plan_description, minimum: 4
  validates_numericality_of :mmrl_plan_validade_days, greater_than_or_equal_to: -1
  validates_numericality_of :mmrl_plan_activation_days, greater_than_or_equal_to: -1
  validates_numericality_of :mmrl_plan_videos_limit, greater_than_or_equal_to: -1
  validates_numericality_of :mmrl_plan_photos_limit, greater_than_or_equal_to: -1
  validates_numericality_of :mmrl_plan_price, greater_than_or_equal_to: 0.00
  validates_each :mmrl_plan_vigence_end_date do |record, attr, value|
    if !value.nil? && value < record.mmrl_plan_vigence_start_date
      record.errors.add attr, 'invalid.'
    end
  end

  scope :active, -> { where(mmrl_plan_active: true) }

  validate :check_min_discount

  before_update :check_update_permission

  before_validation :set_basic_values

  def set_status
    if !mmrl_plan_vigence_end_date.present?
      end_date = Date.today
    else
      end_date = mmrl_plan_vigence_end_date
    end

    self.mmrl_plan_active = Date.today.between?(mmrl_plan_vigence_start_date, end_date)
  end

  def disable!
    if mmrl_plan_vigence_start_date <= Date.today
      self.mmrl_plan_vigence_end_date = Date.today
      save!
    else
      raise "You can't disable a plan with a start date in the future."
    end
  end

  def price_with_initial_discount
    value = mmrl_plan_price
    memorial_memorial_plan_discount_ranges.where(mmrl_plan_dsct_rnge_status: true, mmrl_plan_dsct_rnge_min: 1).each do |discount|
      if value > (mmrl_plan_price - discount.mmrl_plan_dsct_rnge_discount)
        value = mmrl_plan_price - discount.mmrl_plan_dsct_rnge_discount
      end
    end
    value
  end

  def price
    mmrl_plan_price
  end

  def actived?
    mmrl_plan_active?
  end

  def discount?
    !memorial_memorial_plan_discount_ranges.where(mmrl_plan_dsct_rnge_status: true, mmrl_plan_dsct_rnge_min: 1).empty?
  end

  def mmrl_plan_videos_limit_str
    if mmrl_plan_videos_limit == -1
      'Unlimited'
    else
      mmrl_plan_videos_limit
    end
  end

  def mmrl_plan_photos_limit_str
    if mmrl_plan_photos_limit == -1
      'Unlimited'
    else
      mmrl_plan_photos_limit
    end
  end

  def title
    mmrl_plan_title
  end

  def description
    mmrl_plan_description
  end

  def self.free_plan
    Memorial::MemorialPlan.active.where(mmrl_plan_free_default: true).order(:created_at).first
  end

  def discount
    memorial_memorial_plan_discount_ranges.where(mmrl_plan_dsct_rnge_status: true, mmrl_plan_dsct_rnge_min: 1)
  end

  private

  def set_basic_values
    self.mmrl_plan_vigence_start_date ||= Date.today
    set_status
    true
  end

  def check_update_permission
    if mmrl_plan_vigence_end_date_changed? || mmrl_plan_active_changed?
      true
    else
      errors.add 'something', 'invalid.'
      throw(:abort)
    end
  end

  def check_min_discount
    return true if memorial_memorial_plan_discount_ranges.size <= 1
    (1..(memorial_memorial_plan_discount_ranges.size - 1)).each do |i|
      if memorial_memorial_plan_discount_ranges[i].mmrl_plan_dsct_rnge_discount < memorial_memorial_plan_discount_ranges[i - 1].mmrl_plan_dsct_rnge_discount
        errors.add(:memorial_memorial_plan_discount_ranges, 'inválido, você não deve colocar um desconto menor para uma quantidade mínima maior.')
        throw(:abort)
      end
    end
  end
end
