# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorial_plan_coupons
#
#  id                                :uuid             not null, primary key
#  security_user_id                  :uuid             not null
#  financial_order_id                :uuid
#  memorial_plan_id                  :uuid             not null
#  memorial_id                       :uuid
#  mmrl_plan_cpon_used               :boolean          not null
#  mmrl_plan_cpon_used_date          :date
#  mmrl_plan_cpon_active             :boolean          not null
#  mmrl_plan_cpon_active_date        :date
#  mmrl_plan_cpon_vigence_start_date :date
#  mmrl_plan_cpon_vigence_end_date   :date
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#

class Memorial::MemorialPlanCoupon < ApplicationRecord
  belongs_to :security_user, class_name: 'Security::User'
  belongs_to :financial_order, class_name: 'Financial::Order'
  belongs_to :memorial_plan, class_name: 'Memorial::MemorialPlan'
  belongs_to :memorial, class_name: 'Memorial::Memorial', optional: true

  validates :security_user, presence: true
  validates :financial_order, presence: true
  validates :memorial_plan, presence: true

  validate :set_default_values, :check_vigence_dates

  def apply_to!(memorial)
    raise 'This coupom is already used' if mmrl_plan_cpon_used?
    raise 'This coupom is inactive' unless mmrl_plan_cpon_active?
    unless memorial_plan.mmrl_plan_free_default?
      self.mmrl_plan_cpon_used = true
      self.mmrl_plan_cpon_used_date = Date.today
      self.memorial = memorial
      self.mmrl_plan_cpon_vigence_start_date = Date.today
      if memorial_plan.mmrl_plan_validade_days.positive?
        self.mmrl_plan_cpon_vigence_end_date = Date.today + memorial_plan.mmrl_plan_validade_days
      else
        self.mmrl_plan_cpon_vigence_end_date = Date.today + 730000.days
      end
      save!
    end
  end

  def set_default_activated_values!
    self.mmrl_plan_cpon_active = true
    self.mmrl_plan_cpon_active_date = Date.today
  end

  private

  def set_default_values
    self.mmrl_plan_cpon_active ||= false
    self.mmrl_plan_cpon_used ||= false
  end

  def check_vigence_dates
    if some_date_is_set_and_the_other_not?
      message = 'Vigence date must have start and end values'
      errors.add(:mmrl_plan_cpon_vigence_start_date, message)
    end
  end

  def some_date_is_set_and_the_other_not?
    !mmrl_plan_cpon_vigence_start_date.nil? ^ !mmrl_plan_cpon_vigence_end_date.nil?
  end
end
