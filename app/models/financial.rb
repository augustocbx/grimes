# frozen_string_literal: true
module Financial
  def self.table_name_prefix
    'financial_'
  end
end
