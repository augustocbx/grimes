# frozen_string_literal: true
# == Schema Information
#
# Table name: geonames_features
#
#  id                      :uuid             not null, primary key
#  geonameid               :integer
#  name                    :string
#  asciiname               :string
#  alternatenames          :text
#  latitude                :float
#  longitude               :float
#  feature_class           :string
#  feature_code            :string
#  country_code            :string
#  cc2                     :string
#  admin1_code             :string
#  admin2_code             :string
#  admin3_code             :string
#  admin4_code             :string
#  population              :integer
#  elevation               :integer
#  dem                     :integer
#  timezone                :string
#  modification            :datetime
#  type                    :string
#  asciiname_first_letters :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_geonames_features_on_admin1_code              (admin1_code)
#  index_geonames_features_on_asciiname                (asciiname)
#  index_geonames_features_on_asciiname_first_letters  (asciiname_first_letters)
#  index_geonames_features_on_country_code             (country_code)
#  index_geonames_features_on_geonameid                (geonameid)
#  index_geonames_features_on_name                     (name)
#  index_geonames_features_on_population               (population)
#  index_geonames_features_on_type                     (type)
#

module Geonames
  class GeonamesFeature < ActiveRecord::Base
    validates_uniqueness_of :geonameid
    before_save :set_asciiname_first_letters

    has_many :geonames_alternate_names,
             inverse_of: :geonames_feature,
             primary_key: 'geonameid',
             foreign_key: 'geonameid'
    alias alternate_names geonames_alternate_names

    ##
    # Search for feature, searches might include country (separated by ',')
    #
    scope :geo_search, lambda { |query|
                         virgule = query.include? ','

                         splited = virgule ? query.split(',') : [query]

                         country = virgule ? splited.last.strip : nil

                         queries = virgule ? splited[0...-1] : splited
                         queries = queries.join(' ').split(' ')

                         ret = by_name(*queries)

                         unless country.nil?
                           geonames_country = Geonames::GeonamesCountry.search(country).first
                           ret = ret.where(country_code: geonames_country.iso) unless geonames_country.nil?
                         end

                         ret
                       }

    ##
    # Find by names
    #
    scope :by_name, lambda { |*queries|
                      ret = all
                      count = queries.count
                      queries.collect.with_index do |q, idx|
                        query = idx == 0 ? q.to_s : "%#{q}%"
                        ret = ret.where('asciiname_first_letters = ?', q[0...3].downcase)
                        ret = ret.where('name LIKE ? or asciiname LIKE ?', query, query)
                      end
                      ret
                    }

    protected

    ##
    # Set first letters of name into index column
    #
    def set_asciiname_first_letters
      self.asciiname_first_letters = asciiname[0...3].downcase unless asciiname.nil?
    end
  end
end
