# frozen_string_literal: true
# == Schema Information
#
# Table name: geonames_features
#
#  id                      :uuid             not null, primary key
#  geonameid               :integer
#  name                    :string
#  asciiname               :string
#  alternatenames          :text
#  latitude                :float
#  longitude               :float
#  feature_class           :string
#  feature_code            :string
#  country_code            :string
#  cc2                     :string
#  admin1_code             :string
#  admin2_code             :string
#  admin3_code             :string
#  admin4_code             :string
#  population              :integer
#  elevation               :integer
#  dem                     :integer
#  timezone                :string
#  modification            :datetime
#  type                    :string
#  asciiname_first_letters :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_geonames_features_on_admin1_code              (admin1_code)
#  index_geonames_features_on_asciiname                (asciiname)
#  index_geonames_features_on_asciiname_first_letters  (asciiname_first_letters)
#  index_geonames_features_on_country_code             (country_code)
#  index_geonames_features_on_geonameid                (geonameid)
#  index_geonames_features_on_name                     (name)
#  index_geonames_features_on_population               (population)
#  index_geonames_features_on_type                     (type)
#

module Geonames
  class GeonamesCity < Geonames::GeonamesFeature
  end
end
