# frozen_string_literal: true
# == Schema Information
#
# Table name: geonames_countries
#
#  id                   :uuid             not null, primary key
#  iso                  :string
#  iso3                 :string
#  iso_numeric          :string
#  fips                 :string
#  country              :string
#  capital              :string
#  area                 :integer
#  population           :integer
#  continent            :string
#  tld                  :string
#  currency_code        :string
#  currency_name        :string
#  phone                :string
#  postal_code_format   :string
#  postal_code_regex    :string
#  languages            :string
#  geonameid            :integer
#  neighbours           :string
#  equivalent_fips_code :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_geonames_countries_on_geonameid  (geonameid)
#

module Geonames
  class GeonamesCountry < ActiveRecord::Base
    validates_uniqueness_of :geonameid

    ##
    # search by iso first, then by name if not found
    #
    scope :geo_search, lambda { |q|
                         by_iso(q).count > 0 ? by_iso(q) : by_name(q)
                       }

    ##
    # search by iso code
    #
    scope :by_iso, lambda { |q|
                     where('iso = ? or iso3 = ?', q.upcase, q.upcase)
                   }

    ##
    # search by name
    #
    scope :by_name, lambda { |q|
                      where('country LIKE ?', "#{q}%")
                    }
  end
end
