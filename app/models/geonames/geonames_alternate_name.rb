# frozen_string_literal: true
# == Schema Information
#
# Table name: geonames_alternate_names
#
#  id                           :uuid             not null, primary key
#  alternate_name_id            :integer
#  geonameid                    :integer
#  isolanguage                  :string
#  alternate_name               :string
#  is_preferred_name            :boolean
#  is_short_name                :boolean
#  is_colloquial                :boolean
#  is_historic                  :boolean
#  alternate_name_first_letters :string
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#
# Indexes
#
#  index_geonames_alternate_names_on_alternate_name_first_letters  (alternate_name_first_letters)
#  index_geonames_alternate_names_on_alternate_name_id             (alternate_name_id)
#  index_geonames_alternate_names_on_geonameid                     (geonameid)
#  index_geonames_alternate_names_on_isolanguage                   (isolanguage)
#

module Geonames
  class GeonamesAlternateName < ActiveRecord::Base
    validates_uniqueness_of :alternate_name_id
    before_save :set_alternate_name_first_letters

    belongs_to :geonames_feature,
               inverse_of: :geonames_alternate_names,
               primary_key: 'geonameid',
               foreign_key: 'geonameid'
    alias feature geonames_feature

    ##
    # default search (by alternate name)
    #
    scope :geo_search, lambda { |q|
                         by_alternate_name_featured(q)
                       }

    ##
    # search by isolanguage code
    #
    scope :by_isolanguage, lambda { |q|
                             where('isolanguage = ?', q)
                           }

    ##
    # search prefered names
    #
    scope :prefered, lambda {
                       where(is_preferred_name: true)
                     }

    ##
    # search by name for available features
    #
    scope :by_alternate_name_featured, lambda { |q|
                                         joins(:geonames_feature).by_alternate_name(q).where(Geonames::GeonamesFeature.arel_table[:id].not_eq(nil))
                                       }

    ##
    # search by name
    #
    scope :by_alternate_name, lambda { |q|
                                ret = all
                                ret = ret.where('alternate_name_first_letters = ?', q[0...3].downcase)
                                ret = ret.where('alternate_name LIKE ?', "#{q}%")
                              }

    protected

    ##
    # Set first letters of name into index column
    #
    def set_alternate_name_first_letters
      self.alternate_name_first_letters = alternate_name[0...3].downcase unless alternate_name.nil?
    end
  end
end
