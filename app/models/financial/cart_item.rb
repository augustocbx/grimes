# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_cart_items
#
#  id                              :uuid             not null, primary key
#  financial_cart_id               :uuid             not null
#  memorial_plan_discount_range_id :uuid
#  fnnl_cart_item_source_type      :string           not null
#  fnnl_cart_item_source_id        :uuid             not null
#  fnnl_cart_item_receiver_type    :string
#  fnnl_cart_item_receiver_id      :uuid
#  fnnl_cart_item_quantity         :integer          not null
#  fnnl_cart_item_unity_price      :float            not null
#  fnnl_cart_item_discount         :float            not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

module Financial
  class CartItem < ApplicationRecord
    belongs_to :financial_cart, class_name: 'Financial::Cart'
    belongs_to :memorial_plan_discount_range, class_name: 'Memorial::MemorialPlanDiscountRange', optional: true
    belongs_to :fnnl_cart_item_source, polymorphic: true, optional: true
    belongs_to :fnnl_cart_item_receiver, polymorphic: true, optional: true

    validates :fnnl_cart_item_source_id, uniqueness: { scope: [:fnnl_cart_item_source_type, :financial_cart_id] }
    validates :fnnl_cart_item_receiver_id, uniqueness: { scope: [:fnnl_cart_item_receiver_type, :financial_cart_id] }, if: proc { |cart_item| !cart_item.fnnl_cart_item_receiver.nil? }

    validates :fnnl_cart_item_quantity, numericality: { greater_than_or_equal_to: 1 } # This quantity is set to 1 because plans just can be selled one by one
    validates :fnnl_cart_item_unity_price, numericality: { greater_than_or_equal_to: 0.00 } # This quantity is set to 1 because plans just can be selled one by one
    validates :fnnl_cart_item_discount, numericality: { greater_than_or_equal_to: 0.00 } # This quantity is set to 1 because plans just can be selled one by one

    before_validation :check_basic_rules
    before_validation :set_basic_values

    def subtotal
      final_price * fnnl_cart_item_quantity
    end

    def discount
      fnnl_cart_item_discount
    end

    def item_price
      price
    end

    def final_price
      item_price - fnnl_cart_item_discount
    end

    def quantity
      fnnl_cart_item_quantity
    end

    def price
      fnnl_cart_item_source.price
    end

    def free?
      fnnl_cart_item_source.mmrl_plan_free_default?
    end

    def title
      if fnnl_cart_item_source.is_a?(Memorial::MemorialPlan)
        fnnl_cart_item_source.mmrl_plan_title
      # else
      #   ''
      end
    end

    def description
      if fnnl_cart_item_source.is_a?(Memorial::MemorialPlan)
        fnnl_cart_item_source.mmrl_plan_description
      # else
      #   ''
      end
    end

    def check_basic_rules
      if fnnl_cart_item_source.is_a?(Memorial::MemorialPlan) && fnnl_cart_item_source.mmrl_plan_free_default?
        if fnnl_cart_item_quantity > 1
          errors.add(:fnnl_cart_item_quantity, "of this plan can't be greater then 1.")
        end
      end
    end

    private

    def set_basic_values
      self.fnnl_cart_item_discount ||= 0.00
    end
  end
end
