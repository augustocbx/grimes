# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_orders
#
#  id                :uuid             not null, primary key
#  security_user_id  :uuid             not null
#  financial_cart_id :uuid             not null
#  fnnl_ordr_number  :string           not null
#  fnnl_ordr_state   :string           not null
#  fnnl_ordr_total   :float            default(0.0), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#


module Financial
  class Order < ApplicationRecord
    has_many :financial_order_items, class_name: 'Financial::OrderItem', foreign_key: :financial_order_id, dependent: :destroy, autosave: true
    has_many :memorial_memorial_plan_coupons, -> { order('memorial_memorial_plan_coupons.created_at') }, class_name: 'Memorial::MemorialPlanCoupon', foreign_key: :financial_order_id, dependent: :destroy, autosave: true
    belongs_to :financial_cart, class_name: 'Financial::Cart'
    belongs_to :security_user, class_name: 'Security::User'

    validates :financial_cart_id, presence: true
    validates :security_user_id, presence: true
    validates :fnnl_ordr_number, presence: true
    validates :fnnl_ordr_state, presence: true, inclusion: {in: %w(N P C R A), message: '%{value} is not a valid value'}
    # N => New/Editing
    # P => Processing
    # C => Cancelled
    # R => Reproved
    # A => Approved

    validates :fnnl_ordr_total, numericality: {presence: true, greater_than_or_equal_to: 0.00}

    before_validation :generate_order_number, on: :create
    before_validation :set_order_defaults, on: :create
    before_validation :set_of_total_of_order
    validates_associated :financial_order_items

    # validates :financial_order_items, presence: true
    # before_create :set_of_total_of_order

    def state_description
      case fnnl_ordr_state
        when 'N'
          'New'
        # if self.created_at == self.updated_at
        #   'New'
        # else
        #   'Editing'
        # end
        when 'P'
          'Processing'
        when 'C'
          'Cancelled'
        when 'R'
          'Reproved'
        when 'A'
          'Aproved'
        else
          raise 'Invalid status'
      end
    end

    def check_free_order
      set_of_total_of_order
      approve! if !self.financial_order_items.empty? && fnnl_ordr_total == 0.00
    end

    def approve!
      if changed?
        save!
        raise 'Verificar o comportamento nesta linha'
      end
      if financial_order_items.empty?
        errors.add(:financial_order_items, 'Não é possível aprovar um pedido vazio')
        self.fnnl_ordr_state = 'C'
        save!
        return
      end
      financial_order_items.each do |order_item|
        next unless order_item.fnnl_ordr_item_source.is_a?(Memorial::MemorialPlan)
        qtd_coupons = memorial_memorial_plan_coupons.where(financial_order: self, memorial_plan: order_item.fnnl_ordr_item_source).count
        ((qtd_coupons + 1)..order_item.fnnl_ordr_item_quantity).each do |_i|
          memorial_memorial_plan_coupons.build(security_user: security_user, memorial_plan: order_item.fnnl_ordr_item_source, memorial: order_item.fnnl_ordr_item_receiver, mmrl_plan_cpon_used: false, mmrl_plan_cpon_active: false)
        end
        memorial_memorial_plan_coupons.each do |memorial_plan_coupon|
          memorial_plan_coupon.mmrl_plan_cpon_active = true
          memorial_plan_coupon.mmrl_plan_cpon_active_date = DateTime.now
        end
      end
      self.fnnl_ordr_state = 'A'
      save!
    end

    def cancel!
      self.fnnl_ordr_state = 'C'
      save!
    end

    def reprove!
      self.fnnl_ordr_state = 'R'
      save!
    end

    def itens_count
      financial_order_items.count
    end

    def approved?
      fnnl_ordr_state == 'A'
    end

    def reproved?
      fnnl_ordr_state == 'R'
    end

    def activate_coupons!
      memorial_memorial_plan_coupons.includes([:financial_order, :security_user, :memorial, :memorial_plan]).each do |memorial_plan_coupom|
        if !memorial_plan_coupom.memorial.nil? && memorial_plan_coupom.memorial.current_memorial_plan.mmrl_plan_free_default?
          memorial_plan_coupom.set_default_activated_values!
        end
        memorial_plan_coupom.save!
      end
      financial_order_items.each do |order_item|
        memorial = order_item.fnnl_ordr_item_receiver
        if !memorial.nil? && (memorial.memorial_plan.nil? || memorial.memorial_plan.mmrl_plan_free_default?)
          memorial_plan_coupom_to_activate = memorial_memorial_plan_coupons.first
          memorial_plan_coupom_to_activate.apply_to!(memorial)
        end
      end
    end

    def inactivate_coupons!
      memorial_memorial_plan_coupons.includes([:memorial, :memorial_plan, :financial_order, :security_user]).each do |memorial_plan_coupom|
        memorial_plan_coupom.mmrl_plan_cpon_active = false
        memorial_plan_coupom.save!
      end
    end

    private

    def set_of_total_of_order
      if fnnl_ordr_state == 'N' && !financial_order_items.empty?
        self.fnnl_ordr_total = financial_order_items.map(&:item_price).inject { |sum, item| sum + item }
      end
    end

    def set_order_defaults
      self.fnnl_ordr_total ||= 0.00
      self.fnnl_ordr_state ||= 'N'
    end

    def generate_order_number
      self.fnnl_ordr_number = loop do
        random_order = "I#{Date.today.strftime('%Y%m%d')}#{rand(1000..9999)}"
        break random_order unless Financial::Order.exists?(fnnl_ordr_number: random_order)
      end
    end
  end
end
