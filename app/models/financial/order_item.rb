# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_order_items
#
#  id                              :uuid             not null, primary key
#  financial_order_id              :uuid             not null
#  memorial_plan_discount_range_id :uuid
#  fnnl_ordr_item_source_type      :string           not null
#  fnnl_ordr_item_source_id        :uuid             not null
#  fnnl_ordr_item_receiver_type    :string
#  fnnl_ordr_item_receiver_id      :uuid
#  fnnl_ordr_item_quantity         :integer          default(0), not null
#  fnnl_ordr_item_unity_price      :float            default(0.0), not null
#  fnnl_ordr_item_discount         :float            default(0.0), not null
#  fnnl_ordr_item_total            :float            default(0.0), not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

module Financial
  class OrderItem < ApplicationRecord
    belongs_to :fnnl_ordr_item_source, polymorphic: true
    belongs_to :fnnl_ordr_item_receiver, polymorphic: true, optional: true # Memorial
    belongs_to :memorial_memorial_plan, class_name: 'Memorial::MemorialPlan', optional: true
    belongs_to :memorial_memorial_plan_discount_range, class_name: 'Memorial::MemorialPlanDiscountRange', optional: true
    belongs_to :financial_order, class_name: 'Financial::Order'

    validates :fnnl_ordr_item_quantity, numericality: { only_integer: true, greater_than_or_equal_to: 1 }
    validates :fnnl_ordr_item_unity_price, numericality: { greater_than_or_equal_to: 0.00 }
    validates :fnnl_ordr_item_discount, numericality: { greater_than_or_equal_to: 0.00 }
    validates :fnnl_ordr_item_total, numericality: { greater_than_or_equal_to: 0.00 }

    before_validation :set_of_values

    def subtotal
      item_price * fnnl_ordr_item_quantity
    end

    def discount
      fnnl_ordr_item_discount
    end

    def item_price
      fnnl_ordr_item_source.price
    end

    def final_price
      fnnl_ordr_item_unity_price - fnnl_ordr_item_discount
    end

    def quantity
      fnnl_ordr_item_quantity
    end

    def price
      fnnl_ordr_item_source.price
    end

    def free?
      fnnl_ordr_item_source.mmrl_plan_free_default?
    end

    def title
      fnnl_ordr_item_source.mmrl_plan_title
    end

    def description
      fnnl_ordr_item_source.mmrl_plan_description
    end

    private

    def set_of_values
      self.fnnl_ordr_item_discount ||= 0.00
      if financial_order.fnnl_ordr_state == 'N'
        self.fnnl_ordr_item_total = fnnl_ordr_item_unity_price * fnnl_ordr_item_quantity
      end
    end

    def set_basic_values
      self.fnnl_ordr_item_discount ||= 0.00
    end
  end
end
