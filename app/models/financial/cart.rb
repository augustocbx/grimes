# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_carts
#
#  id                :uuid             not null, primary key
#  security_user_id  :uuid
#  session_id        :uuid
#  cnnl_cart_current :boolean          default(TRUE), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
module Financial
  class Cart < ApplicationRecord
    has_many :financial_cart_items, class_name: 'Financial::CartItem', foreign_key: :financial_cart_id, dependent: :destroy, autosave: true
    has_one :financial_order, -> { where(fnnl_ordr_state: ['N']) }, class_name: 'Financial::Order', foreign_key: :financial_cart_id, dependent: :destroy, autosave: true, validate: true
    belongs_to :security_user, class_name: 'Security::User', optional: true

    scope :current, -> { where(cnnl_cart_current: true) }

    def items_count
      financial_cart_items.size
    end

    def clear
      financial_cart_items.delete_all
    end

    def update_item_cart(fnnl_cart_item_source, quantity, fnnl_cart_item_receiver = nil)
      unless [Memorial::MemorialPlan].include?(fnnl_cart_item_source.class)
        raise "'fnnl_cart_item_source' is of a invalid plan"
      end
      if quantity.negative?
        raise "'quantity' can't be minor then 0"
      elsif quantity.zero?
        remove_item_cart(fnnl_cart_item_source)
        return fnnl_cart_item_source
      end


      discount = 0.00
      memorial_plan_discount_range = Memorial::MemorialPlanDiscountRange.where(memorial_plan: fnnl_cart_item_source, mmrl_plan_dsct_rnge_status: true).where(["mmrl_plan_dsct_rnge_min <= :mmrl_plan_dsct_rnge_min", {mmrl_plan_dsct_rnge_min: quantity}]).order('mmrl_plan_dsct_rnge_discount desc').first
      if memorial_plan_discount_range.present?
        discount = memorial_plan_discount_range.mmrl_plan_dsct_rnge_discount
      end

      if financial_cart_items.where(fnnl_cart_item_source: fnnl_cart_item_source).empty?
        financial_cart_item1 = Financial::CartItem.new(fnnl_cart_item_source: fnnl_cart_item_source, fnnl_cart_item_receiver: fnnl_cart_item_receiver, fnnl_cart_item_quantity: quantity, fnnl_cart_item_unity_price: fnnl_cart_item_source.mmrl_plan_price, memorial_plan_discount_range: memorial_plan_discount_range, fnnl_cart_item_discount: discount)
        financial_cart_items << financial_cart_item1
      else
        financial_cart_items.select { |item| item.fnnl_cart_item_source == fnnl_cart_item_source }.each do |financial_cart_item2|
          financial_cart_item2.fnnl_cart_item_quantity = financial_cart_item2.fnnl_cart_item_quantity + quantity
          financial_cart_item2.memorial_plan_discount_range = memorial_plan_discount_range
          financial_cart_item2.fnnl_cart_item_discount = discount
        end
      end
    end

    def add_item_cart(fnnl_cart_item_source, quantity = 1, fnnl_cart_item_receiver = nil)
      raise Exception, "'quantity' can't be minor then 1" if quantity < 1
      update_item_cart(fnnl_cart_item_source, quantity, fnnl_cart_item_receiver)
    end

    def remove_item_cart(fnnl_cart_item_source)
      financial_cart_item = financial_cart_items.where(fnnl_cart_item_source: fnnl_cart_item_source).first
      financial_cart_items.delete financial_cart_item
    end

    def empty?
      financial_cart_items.empty?
    end

    def subtotal
      if !financial_cart_items.empty?
        financial_cart_items.map(&:subtotal).inject { |sum, subtotal| sum + subtotal }
      else
        0.00
      end
    end

    def shipping_cost
      0.00
    end

    def taxes
      0.00
    end

    def total
      subtotal + shipping_cost + taxes
    end

    def itens_count
      financial_cart_items.count
    end

    def checkout
      if itens_count <= 0 || invalid?
        false
      else
        generate_order
      end
    end

    def checkout!
      raise errors.full_messages.to_s unless checkout
    end

    def generate_order
      unless financial_order.present?
        create_financial_order!(security_user: security_user)
      end
      financial_cart_items.each do |cart_item|
        hash_itens = {
          fnnl_ordr_item_source: cart_item.fnnl_cart_item_source,
          fnnl_ordr_item_receiver: cart_item.fnnl_cart_item_receiver,
          fnnl_ordr_item_quantity: cart_item.fnnl_cart_item_quantity,
          fnnl_ordr_item_unity_price: cart_item.fnnl_cart_item_unity_price,
          fnnl_ordr_item_discount: cart_item.fnnl_cart_item_discount,
          memorial_plan_discount_range_id: cart_item.memorial_plan_discount_range_id,
        }
        financial_order.financial_order_items.find_or_create_by(hash_itens)
      end
      self.cnnl_cart_current = false
      save!
      financial_order.check_free_order
      financial_order
    end
  end
end
