# frozen_string_literal: true
module Memorial
  def self.table_name_prefix
    'memorial_'
  end
end
