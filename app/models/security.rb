# frozen_string_literal: true
module Security
  def self.table_name_prefix
    'security_'
  end
end
