# frozen_string_literal: true
# == Schema Information
#
# Table name: security_invites
#
#  id          :uuid             not null, primary key
#  sender_id   :uuid
#  memorial_id :uuid
#  email       :string           not null
#  token       :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  sent_at     :datetime
#  accepted_at :datetime
#  rejected_at :datetime
#  deleted_at  :datetime
#  admin       :boolean          default(FALSE), not null
#
# Indexes
#
#  index_security_invites_on_deleted_at   (deleted_at)
#  index_security_invites_on_email        (email)
#  index_security_invites_on_memorial_id  (memorial_id)
#  index_security_invites_on_sender_id    (sender_id)
#  index_security_invites_on_token        (token)
#

module Security
  class Invite < ApplicationRecord
    attr_accessor :notice, :alert
    belongs_to :memorial, class_name: 'Memorial::Memorial'
    belongs_to :sender, class_name: 'Security::User', optional: true

    acts_as_paranoid

    validate :check_email_already_sent, on: :create
    validates :email, presence: true
    before_create :generate_token

    def generate_token
      self.token = Digest::SHA1.hexdigest([memorial_id, Time.now, rand].join)
    end

    # def error_message_for_create
    #   if !self.email.blank?
    #     message = String.new 'Não foi possível enviar um convite para '
    #     message << "#{self.email}. "
    #   else
    #     message = String.new
    #   end
    #   message << errors.full_messages.join(' ')
    #   message
    # end

    def sent?
      !sent_at.nil?
    end

    def allow_resend?
      sent_at.nil? || (DateTime.now.to_time - sent_at.to_time) / 60 / 60 > 1
    end

    def can_add_memorial?
      accepted_at.nil? && rejected_at.nil? & deleted_at.nil?
    end

    def self.create_invite(memorial, email, current_user)

      invite = Security::Invite.unscope(where: :deleted_at).find_by(email: email, memorial_id: memorial)

      if invite.nil?
        # Make a new Invite
        invite = Security::Invite.new(email: email, memorial_id: memorial.id)
        # set the sender to the current user
        invite.sender_id = current_user.id
        if invite.save
          invite.notice = "#{invite.email}"
        end

      else
        invite.restore if invite.deleted?
        if invite.accepted_at
          invite.alert = "Convite já foi aceito por #{email} neste memorial em #{I18n.l(invite.accepted_at, format: :short)}. "
        elsif !invite.allow_resend?
          invite.alert = "Convite já enviado para #{email} neste memorial em menos de 1 hora. "
        elsif memorial.users.map(&:email).include?(email) || memorial.user.email == email
          invite.alert = "O usuário #{email} já se encontra neste memorial"
        end
      end
      invite
    end

    private

    def check_email_already_sent
      @invite = Security::Invite.where(email: email, memorial_id: memorial_id, sender_id: sender_id).order('created_at desc').first
      if !(@invite.nil? || @invite.allow_resend?)
        errors.add(:email, "já enviado em #{I18n.l(@invite.updated_at, format: :short)}")
      end
    end
  end
end
