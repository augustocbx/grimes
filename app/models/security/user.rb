# frozen_string_literal: true
# == Schema Information
#
# Table name: security_users
#
#  id                     :uuid             not null, primary key
#  username               :string
#  name                   :string
#  about                  :text
#  location               :string
#  image                  :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  superuser              :boolean          default(FALSE), not null
#
# Indexes
#
#  index_security_users_on_email                 (email) UNIQUE
#  index_security_users_on_reset_password_token  (reset_password_token) UNIQUE
#

module Security
  class User < ApplicationRecord
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable, :omniauthable, omniauth_providers: [:google_oauth2, :facebook]
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    # acts_as_token_authenticatable

    mount_uploader :image, UserPhotoUploader

    has_many :security_authorizations, class_name: 'Security::Authorization'
    has_many :financial_carts, -> { order 'financial_carts.id desc' }, class_name: 'Financial::Cart', foreign_key: :security_user_id

    # INVITES
    has_many :invitations, class_name: 'Security::Invite', foreign_key: 'recipient_id'
    has_many :sent_invites, class_name: 'Invite', foreign_key: 'sender_id'
    has_many :memorial_users, class_name: 'Security::MemorialUser'
    has_many :memorials, class_name: 'Memorial::Memorial', through: :memorial_users

    validates_presence_of :username, allow_nil: true

    after_create :check_invites_and_add_memorials

    def self.from_omniauth(auth)
      authorization = Security::Authorization.where(provider: auth.provider, uid: auth.uid.to_s, token: auth.credentials.token, secret: auth.credentials.secret).first_or_initialize
      if authorization.user.blank?
        user = Security::User.where('email = ?', auth['info']['email']).first
        user = create_user(auth) if user.nil?
        authorization.username = auth.info.nickname
        authorization.user_id = user.id
        authorization.save!
      end
      user.reload
      user
    end

    def self.from_flutter_by_token(flutter_by_token)
      # authorization = Security::Authorization.where(provider: flutter_by_token.provider, uid: flutter_by_token.client_id, token: flutter_by_token.token).first_or_initialize
      if flutter_by_token.provider == 'email'
        authorization = Security::Authorization.where(provider: 'email', username: flutter_by_token.email, token: flutter_by_token.token).first_or_initialize
      else
        authorization = Security::Authorization.where(uid: flutter_by_token.client_id, token: flutter_by_token.token).first_or_initialize
      end
      if authorization.user.blank?
        user = Security::User.where('email = ?', flutter_by_token.email).first
        user = create_user_with_flutter(flutter_by_token) if user.nil?
        # authorization.username = flutter_by_token.nickname
        authorization.user_id = user.id
        authorization.save!
        user.reload
      end
      authorization.user
    end

    def self.from_firebase_by_token(token, info_by_token, name, device_id, password)
      # authorization = Security::Authorization.where(provider: flutter_by_token.provider, uid: flutter_by_token.client_id, token: flutter_by_token.token).first_or_initialize
      username = info_by_token[:email]
      username ||= info_by_token[:user_id]
      authorization = Security::Authorization.where(provider: 'firebase', username: username, token: token, device_id: device_id).first_or_initialize
      if authorization.user.blank?
        user = Security::User.where('email = ?', username).first if info_by_token[:email].present?
        user ||= Security::User.where('username = ?', username).first
        user = create_user_with_firebase(info_by_token, name, password) if user.nil?
        # authorization.username = flutter_by_token.nickname
        authorization.user_id = user.id
        authorization.uid = info_by_token[:user_id]
        authorization.device_id = device_id
        authorization.save!
        user.reload
      end
      authorization.user
    end

    def self.user_with_token(token)
      return nil if token.blank?
      # authorization = Security::Authorization.where(provider: flutter_by_token.provider, uid: flutter_by_token.client_id, token: flutter_by_token.token).first_or_initialize
      authorization = Security::Authorization.where(token: token).first
      return nil if authorization.nil?
      authorization.user
    end

    def self.create_from_flutter_by_email(flutter_with_email)
      # authorization = Security::Authorization.where(provider: flutter_by_token.provider, uid: flutter_by_token.client_id, token: flutter_by_token.token).first_or_initialize
      user = Security::User.where(email: flutter_with_email.email).first
      if user.present?
        raise StandardError, 'Já existe um usuário com este email'
      end
      user = create_user_with_mail_and_password(flutter_with_email)
      user
    end

    def self.signin_from_flutter_by_email(flutter_with_email)
      # authorization = Security::Authorization.where(provider: flutter_by_token.provider, uid: flutter_by_token.client_id, token: flutter_by_token.token).first_or_initialize
      user = Security::User.where(email: flutter_with_email.email).first
      user.security_authorizations.build(provider: 'email', username: flutter_with_email.email, device_id: flutter_with_email.device_id, token: SecureRandom.hex(86))
      user.save
      if user.nil?
        raise StandardError, 'Usuário não encontrado.'
      end
      # user = create_user_with_mail_and_password(flutter_with_email)
      user
    end

    def current_cart
      @current_cart ||= financial_carts.current.first
      if @current_cart.nil?
        financial_carts << Financial::Cart.new(cnnl_cart_current: true)
        financial_carts.reload
        @current_cart = financial_carts.current.first
      end
      @current_cart
    end

    def add_item_cart(fnnl_cart_item_source, quantity = 1, fnnl_cart_item_receiver = nil)
      current_cart.add_item_cart(fnnl_cart_item_source, quantity, fnnl_cart_item_receiver)
    end

    def checkout
      current_cart.checkout
    end

    # def token_validation_response
    #   as_json(except: [
    #             :tokens, :created_at, :updated_at
    #           ])
    # end

    def add_to_memorial_by_token(invite_token)
      invite = Security::Invite.find_by(token: invite_token)
      if !invite.nil? && !memorials.include?(invite.memorial) && invite.can_add_memorial?
        memorial_users.build(memorial_id: invite.memorial_id, admin: invite.admin?)
        if save
          invite.accepted_at = DateTime.now
          invite.admin = invite.admin?
          invite.save!
        end
      end
    end

    def title
      if !name.blank?
        name
      elsif !username.blank?
        username
      elsif !email.blank?
        email
      end
    end

    def memorial_added?(memorial)
      memorial.user == self || memorial_users.map(&:memorial).include?(memorial)
    end

    def memorial_admin?(memorial)
      memorial.user == self || memorial_users.where(admin: true).map(&:memorial).include?(memorial)
    end

    def provider=(provider)
      puts provider
    end

    def confirmed?
      true
    end

    def image_url
      self.image.public_url
    end

    def last_token
      return nil if self.security_authorizations.order('created_at desc').first.nil?
      self.security_authorizations.order('created_at desc').first.token
    end

    def memorials_owner
      Memorial::Memorial.left_joins(:security_memorial_users).where('((security_memorial_users.user_id = :current_user and security_memorial_users.admin) or memorial_memorials.security_user_id = :current_user)', current_user: self.id, security_memorial_users: {user_id: self.id}).order("memorial_memorials.mmry_firstname asc, memorial_memorials.mmry_lastname asc").includes(:user).map(&:id).uniq
    end

    def memorials_following
      memorials_owner + Memorial::Memorial.left_joins(:security_memorial_users).where('(security_memorial_users.user_id = :current_user or memorial_memorials.security_user_id = :current_user) and photo is not null', current_user: self.id, security_memorial_users: {user_id: self.id}).order("memorial_memorials.mmry_firstname asc, memorial_memorials.mmry_lastname asc").includes(:user).map(&:id).uniq
    end

    private

    def check_invites_and_add_memorials
      Security::Invite.where('email = :email and rejected_at is null and deleted_at is null', email: email).each do |invite|
        add_to_memorial_by_token(invite.token)
      end
    end

    private_class_method

    def self.create_user(auth)
      user = Security::User.new
      user.password = Devise.friendly_token[0, 10]
      user.name = auth.info.name
      user.email = auth.info.email
      if auth.provider == 'twitter'
        user.save(validate: false)
      else
        user.save
      end
      user
    end

    def self.create_user_with_flutter(flutter_by_token)
      user = Security::User.new
      user.password = Devise.friendly_token[0, 10]
      user.name = flutter_by_token.name
      user.email = flutter_by_token.email
      if flutter_by_token.provider == 'twitter'
        user.save(validate: false)
      else
        user.save!
      end
      user
    end

    def self.create_user_with_firebase(info_by_token, name, password)
      username = info_by_token[:email]
      username ||= info_by_token[:user_id]
      user = Security::User.new
      user.password = password || Devise.friendly_token[0, 10]
      user.username = username
      user.name = name
      user.email = info_by_token[:email]
      user.save(validate: false)
      user
    end

    def self.create_user_with_mail_and_password(flutter_with_email)
      user = Security::User.new
      user.password = flutter_with_email.password
      user.name = flutter_with_email.name
      user.email = flutter_with_email.email
      user.security_authorizations.build(provider: 'email', username: flutter_with_email.email, token: SecureRandom.hex(86))
      user.save
      user
    end
  end
end
