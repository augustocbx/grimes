# frozen_string_literal: true
# == Schema Information
#
# Table name: security_authorizations
#
#  id         :uuid             not null, primary key
#  provider   :string
#  uid        :string
#  user_id    :uuid
#  username   :string
#  token      :string
#  secret     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  device_id  :string
#

module Security
  class Authorization < ApplicationRecord
    belongs_to :user

    default_scope { order('security_authorizations.created_at DESC') }
    scope :google, -> { where(provider: 'google_oauth2') }
    scope :facebook, -> { where(provider: 'facebook') }

    # after_create :fetch_details

    # def fetch_details
    #   self.send("fetch_details_from_#{self.provider.downcase}")
    # end
    #

    # def fetch_details_from_facebook
    #   graph = Koala::Facebook::API.new(self.token)
    #   facebook_data = graph.get_object("me")
    #   self.username = facebook_data['username']
    #   self.save
    # end
    #
    # def fetch_details_from_twitter
    #
    # end
    #
    #
    # def fetch_details_from_github
    # end
    #
    #
    # def fetch_details_from_linkedin
    #
    # end
    #
    # def fetch_details_from_google_oauth2
    #
    # end
  end
end
