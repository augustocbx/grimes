# frozen_string_literal: true
# == Schema Information
#
# Table name: security_memorial_users
#
#  id          :uuid             not null, primary key
#  user_id     :uuid             not null
#  memorial_id :uuid             not null
#  admin       :boolean          default(FALSE), not null
#  deleted_at  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_security_memorial_users_on_deleted_at   (deleted_at)
#  index_security_memorial_users_on_memorial_id  (memorial_id)
#  index_security_memorial_users_on_user_id      (user_id)
#

class Security::MemorialUser < ApplicationRecord
  belongs_to :memorial, class_name: 'Memorial::Memorial'
  belongs_to :user, class_name: 'Security::User'

  validates :user_id, uniqueness: { scope: [:memorial_id] }

  before_create :check_pending_invites

  acts_as_paranoid

  private

  def check_pending_invites
    admin = false
    Security::Invite.where(accepted_at: nil, rejected_at: nil, deleted_at: nil, email: user.email).each do |invite|
      if !admin && invite.admin?
        admin = true
        invite.accepted_at = DateTime.now
        invite.save
      end
      self.admin = admin
    end
  end
end
