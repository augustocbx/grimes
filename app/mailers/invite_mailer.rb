# frozen_string_literal: true
class InviteMailer < ApplicationMailer
  def new_user_invite(invite, invite_url)
    @invite = invite
    @invite_url = invite_url
    mail(
      subject: "Você foi Convidado para conhecer o Memorial de #{@invite.memorial.title}",
      to: invite.email,
      tag: 'invite',
      track_opens: 'true'
    )
    @invite.sent_at = DateTime.now
    @invite.save!
  end
end
