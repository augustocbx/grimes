# frozen_string_literal: true
class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@memorialvivo.com.br'
  layout 'mailer'
end
