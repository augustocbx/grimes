class FinadosMain
  BACKGROUND_PATH = Rails.root.join('docs', 'clouds-test.jpg')
  attr_reader :memorial, :main_text, :phrase, :annotation, :photo, :logo
  def initialize(memorial_id, main_text = nil, phrase = nil)
    # p Magick.fonts
    @memorial = Memorial::Memorial.find(memorial_id)
    @phrases = []
    @phrases << 'Ainda sabendo que a morte vem de Deus, quando nós não a provocarmos, não podemos, por enquanto, na Terra receber a morte com alegria porque ninguém recebe um adeus com felicidade, mas podemos receber a separação com fé em Deus, entendendo que um dia nos reencontraremos todos numa vida maior e essa esperança deve aquecer-nos o coração.'
    @phrases << 'Mesmo sabendo que um dia a vida acaba, a gente nunca está preparado para perder alguém.'
    @phrases << 'A morte é uma pétala que se solta da flor e deixa uma eterna saudade no coração.'
    @phrases << 'Ninguém está preparado para perder alguém e nem mesmo todo tempo do mundo me fará esquecer de quem partiu sem avisar!'
    if main_text.nil?
      @main_text = "Finados é dia de lembrar... \nSaudades de #{memorial.title}"
    else
      @main_text = main_text
    end
    if phrase.nil?
      # @phrase = @phrases.sample
      @phrase = @phrases.first
    end
    @annotation = Magick::Draw.new.tap do |text|
      text.font = 'Tahoma'
      text.pointsize = 60
      text.font_weight = Magick::BoldWeight
      text.fill = 'gray'
      text.gravity = Magick::SouthGravity
    end
    # The thumb version has 220px
    @photo = Magick::Image.read(memorial.photo.thumb_big.path).first
    @logo = Magick::Image.read(Rails.root.join('public/layouts/img/logo.png')).first
  end

  def perform
    draw_main_text
    draw_info
    draw_phrase
    append_picture
    append_logo
    save_image
  end

  def perform_output
    draw_main_text
    # draw_phrase
    append_picture
    image_output
  end

  private

  def draw_main_text
    annotation.annotate(background, 0, 0, 0, 80, main_text)
  end

  def draw_phrase
    img = Magick::Image.read("caption:#{phrase}") do
      # puts self.methods
      self.size = '600x'
      self.background_color = 'none'
      self.pointsize = 30
      self.font = 'Tahoma'
      self.fill = 'gray'
      self.gravity = Magick::CenterGravity
    end.first
    background.composite!(img, Magick::NorthEastGravity, 20, 10, Magick::OverCompositeOp)
  end

  def draw_info
    img = Magick::Image.read("caption: •#{I18n.l(memorial.mmry_birth_date)}  †#{I18n.l(memorial.mmry_death_date)}") do
      # puts self.methods
      self.size = '500x'
      self.background_color = 'none'
      self.pointsize = 27
      self.font = 'Arial'
    end.first
    background.composite!(img, Magick::NorthWestGravity, 20, 350, Magick::OverCompositeOp)
  end

  def append_picture
    background.composite!(photo, 20, 20, Magick::OverCompositeOp)
  end

  def append_logo
    background.composite!(logo, Magick::SouthGravity, 20, 20, Magick::OverCompositeOp)
  end

  def save_image
    background.write(Rails.root.join('tmp', 'finados_main.jpg'))
  end

  def image_output
    background.to_blob
  end

  def background
    if @background.nil?
      @background = Magick::Image.read(BACKGROUND_PATH).first
      @background = @background.resize_to_fill(1200,628)
    end
    @background
  end
end