# frozen_string_literal: true
module FrontendHelper
  def menu_link_to(title, path)
    class_activate = ''
    class_activate = 'active' if request.path == path.gsub(/\#.*/, '')
    content_tag(:li, { class: class_activate }, false) do
      content_tag(:a, { href: path }, false) do
        title.html_safe
      end
    end.html_safe
  end

  def menu_side_link_to(title, icon, path)
    class_activate = ''
    class_activate = 'active' if request.path == path
    li_class = ['list-group-item', class_activate]
    content_tag(:li, { class: li_class.join(' ') }, false) do
      content_tag(:a, { href: path }, false) do
        (content_tag(:i, '', class: ['fa', icon].join(' ')) + ' ' + title).html_safe
      end
    end.html_safe
  end

  def submenu_side_link_to(title, path)
    class_activate = ''
    class_activate = 'active' if request.path == path
    li_class = ['list-group-item', class_activate]
    content_tag(:ul, { class: li_class.join(' ') }, false) do
      content_tag(:li, { class: li_class.join(' ') }, false) do
        content_tag(:a, { href: path }, false) do
          (content_tag(:i) + ' ' + title).html_safe
        end
      end
    end.html_safe
  end
end
