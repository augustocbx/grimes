# frozen_string_literal: true
module Memorial::MemorialsHelper
  def menu_link_to(title, path)
    class_activate = 'dropdown'
    class_activate = 'dropdown active' if request.path == path.gsub(/\#.*/, '')
    content_tag(:li, {class: class_activate}, false) do
      content_tag(:a, {href: path}, false) do
        title.html_safe
      end
    end.html_safe
  end

  def memorial_added?
    current_user.present? && current_user.memorial_added?(@memorial)
  end

  def memorial_admin?
    current_user.present? && current_user.memorial_admin?(@memorial)
  end

  def memorial_user_id
    memorial_user = Security::MemorialUser.find_by(memorial_id: @memorial.id, user_id: current_user.id)
    if memorial_user.present?
      memorial_user.id
    else
      nil
    end
  end
end
