# frozen_string_literal: true
module CartHelper
  def current_cart
    cart = nil
    if session[:cart_id].present?
      begin
        cart = Financial::Cart.includes(:security_user).find(session[:cart_id])
      rescue ActiveRecord::RecordNotFound
        cart = nil
      end
      cart = nil if !cart.nil? && !cart.cnnl_cart_current?
    end
    if cart.nil?
      cart = if !current_user.nil?
               Financial::Cart.create(security_user_id: current_user.id)
             else
               Financial::Cart.create(session_id: session.id)
             end
      session[:cart_id] = cart.id
    end
    if cart.security_user.nil? && !current_user.nil?
      cart.update(security_user: current_user)
    end
    cart
  end

  def current_memorial
    if session[:memorial_id].present?
      Memorial::Memorial.find(session[:memorial_id])
    end
  end
end
