# encoding: utf-8
# frozen_string_literal: true

class MemorialPhotosUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  include CarrierWave::RMagick
  # include ImageOptimizer
  # include Piet::CarrierWaveExtension
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # process optimize: [{ quiet: true }]
  process :optimize
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    Rails.root.join('uploaded_files', 'memorial_photos_uploader', model.class.to_s.underscore, mounted_as.to_s, model.id.to_s).to_s
  end

  # Process files as they are uploaded:
  process resize_to_limit: [1920, 1080]

  # Create different versions of your uploaded files:
  version :thumb do
    process resize_to_fill: [220, 220]
    process optimize: [{ quiet: true }]
  end

  version :card_thumb do
    process resize_to_fill: [40, 50]
    process optimize: [{ quiet: true }]
  end

  version :thumb_big do
    process resize_to_fill: [320, 320]
    process optimize: [{ quiet: true }]
  end

  version :social_share do
    process resize_to_fill: [1200, 630]
    process optimize: [{ quiet: true }]
  end

  version :image_mobile do
    process resize_to_limit: [2960, 2960]
    process optimize: [{ quiet: true }]
  end

  version :image_optimized do
    process resize_to_limit: [4000, 4000]
    process optimize: [{ quiet: true }]
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def optimize(options = {})
    ::ImageOptimizer.new(current_path, options).optimize
  end

  def public_url(version = nil)
    params = {
      id: model.id, version: version
    }
    if model.is_a?(Memorial::MemorialPhoto)
      return Rails.application.routes.url_helpers.memorial_photos_image_url(params)
    elsif model.is_a?(Memorial::Memorial)
      return Rails.application.routes.url_helpers.memorial_main_photo_url(params)
    end
  end
end
