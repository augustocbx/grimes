// JavaScript Document
function enviaFormLogin(){
    var form = document.getElementById("form_login");
    if(form.email.value == ""){
        alert("Informe o E-mail.");
    }else if(form.senha.value == ""){
        alert("Informe a Senha.");
    }else{
        $("#form_login input[type=submit]").attr('disabled', 'disabled');
        $.post('php/login.php',$(form).serialize(),function(data){
			if(data.success == false){
				alert(data.error);
				$("#form_login input[type=submit]").removeAttr('disabled');
				form.reset();
			}else{
				window.location.href = "dashboard.php";
			}
        }, "json");
    }
}