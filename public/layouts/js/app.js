$(function(){
	$(".box-foto").fancybox();
	$('.curtir').on('click', function(){
		var qtdCurtir = parseInt($(this).text());
		if($(this).hasClass('ativo')){
			$(this).removeClass('ativo');
			$(this).text(qtdCurtir - 1);
		}else{
			$(this).addClass('ativo');
			$(this).text(qtdCurtir + 1);
		}
	});
	$(".assinar-livro").on("click", function(){
		$("#bg_lightbox, #lightbox_panel").fadeIn();
	});
	$("#bg_lightbox, #lightbox_panel .fa-close").on("click", function(){
		$("#bg_lightbox, #lightbox_panel").fadeOut();
	});
	$('.navbar-toggle').on('click', function(){
		if($(this).hasClass('collapsed')){
			$(this).removeClass('collapsed');
			$('nav .menu').slideDown();
		}else{
			$(this).addClass('collapsed');
			$('nav .menu').slideUp();
		}
	});
});