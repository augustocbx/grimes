SimpleCov.start do
    add_filter "config/initializers/devise.rb"
    add_filter "config/initializers/carrierwave.rb"
    add_filter "config/initializers/pagseguro.rb"
    add_filter "app/helpers/devise/devise_single_page_helper.rb"
end