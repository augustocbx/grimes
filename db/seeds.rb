# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Security::User.create(username: 'user', email: 'user@gmail.com', name: 'User test', password: '123456', password_confirmation: '123456')

free_default_plan = Memorial::MemorialPlan.where(mmrl_plan_free_default: true, mmrl_plan_active: true).first
if free_default_plan.nil?
  plan_params = {
    mmrl_plan_title: 'Gratuito',
    mmrl_plan_description: 'Plano básico gratuito',
    mmrl_plan_validade_days: -1,
    mmrl_plan_activation_days: -1,
    mmrl_plan_videos_limit: 5,
    mmrl_plan_photos_limit: 5,
    mmrl_plan_support: false,
    mmrl_plan_price: 0.00,
    mmrl_plan_active: true,
    mmrl_plan_vigence_start_date: Date.today,
    mmrl_plan_vigence_end_date: Date.today + 1000.years,
    mmrl_plan_free_default: true,
    mmrl_plan_color: 'green'
  }
  Memorial::MemorialPlan.create!(plan_params)
end

discount_params = {
  mmrl_plan_dsct_rnge_status: true,
  mmrl_plan_dsct_rnge_min: 1,
  mmrl_plan_dsct_rnge_discount: 70.00
}

memorial_plan = Memorial::MemorialPlan.where(mmrl_plan_title: '1 ano', mmrl_plan_active: true).first
if memorial_plan.nil?
  plan_params = {
    mmrl_plan_title: '1 ano',
    mmrl_plan_description: 'Premium por 1 ano',
    mmrl_plan_validade_days: ((Date.today + 1.year) - Date.today).to_i,
    mmrl_plan_activation_days: ((Date.today + 1.year) - Date.today).to_i,
    mmrl_plan_videos_limit: -1,
    mmrl_plan_photos_limit: -1,
    mmrl_plan_support: true,
    mmrl_plan_price: 70.00,
    mmrl_plan_active: true,
    mmrl_plan_vigence_start_date: Date.today,
    mmrl_plan_vigence_end_date: Date.today + 1000.years,
    mmrl_plan_color: 'blue'
  }
  memorial_plan = Memorial::MemorialPlan.create!(plan_params)
end
if memorial_plan.memorial_memorial_plan_discount_ranges.where(mmrl_plan_dsct_rnge_status: true, mmrl_plan_dsct_rnge_min: 1).empty?
  memorial_plan.memorial_memorial_plan_discount_ranges << Memorial::MemorialPlanDiscountRange.new(discount_params)
end

memorial_plan = Memorial::MemorialPlan.where(mmrl_plan_title: '5 anos', mmrl_plan_active: true).first
if memorial_plan.nil?
  plan_params = {
    mmrl_plan_title: '5 anos',
    mmrl_plan_description: 'Premium por 5 anos',
    mmrl_plan_validade_days: ((Date.today + 5.year) - Date.today).to_i,
    mmrl_plan_activation_days: ((Date.today + 5.year) - Date.today).to_i,
    mmrl_plan_videos_limit: -1, mmrl_plan_photos_limit: -1,
    mmrl_plan_support: true,
    mmrl_plan_price: 150.00,
    mmrl_plan_active: true,
    mmrl_plan_vigence_start_date: Date.today,
    mmrl_plan_vigence_end_date: Date.today + 1000.years,
    mmrl_plan_color: 'red'
  }
  memorial_plan = Memorial::MemorialPlan.create!(plan_params)
end
if memorial_plan.memorial_memorial_plan_discount_ranges.where(mmrl_plan_dsct_rnge_status: true, mmrl_plan_dsct_rnge_min: 1).empty?
  memorial_plan.memorial_memorial_plan_discount_ranges << Memorial::MemorialPlanDiscountRange.new(discount_params)
end

memorial_plan = Memorial::MemorialPlan.where(mmrl_plan_title: 'Ilimitado', mmrl_plan_active: true).first
if memorial_plan.nil?
  plan_params = {
    mmrl_plan_title: 'Ilimitado',
    mmrl_plan_description: 'Premium pagamento único',
    mmrl_plan_validade_days: -1,
    mmrl_plan_activation_days: -1,
    mmrl_plan_videos_limit: -1,
    mmrl_plan_photos_limit: -1,
    mmrl_plan_support: true,
    mmrl_plan_price: 200.00,
    mmrl_plan_active: true,
    mmrl_plan_vigence_start_date: Date.today,
    mmrl_plan_vigence_end_date: Date.today + 1000.years,
    mmrl_plan_color: 'brown'
  }
  memorial_plan = Memorial::MemorialPlan.create!(plan_params)
end
if memorial_plan.memorial_memorial_plan_discount_ranges.where(mmrl_plan_dsct_rnge_status: true, mmrl_plan_dsct_rnge_min: 1).empty?
  memorial_plan.memorial_memorial_plan_discount_ranges << Memorial::MemorialPlanDiscountRange.new(discount_params)
end
