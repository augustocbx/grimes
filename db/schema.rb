# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_15_050448) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "bootsy_image_galleries", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "bootsy_resource_type"
    t.uuid "bootsy_resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bootsy_images", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "image_file"
    t.uuid "image_gallery_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "financial_cart_items", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "financial_cart_id", null: false
    t.uuid "memorial_plan_discount_range_id"
    t.string "fnnl_cart_item_source_type", null: false
    t.uuid "fnnl_cart_item_source_id", null: false
    t.string "fnnl_cart_item_receiver_type"
    t.uuid "fnnl_cart_item_receiver_id"
    t.integer "fnnl_cart_item_quantity", null: false
    t.float "fnnl_cart_item_unity_price", null: false
    t.float "fnnl_cart_item_discount", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "financial_carts", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "security_user_id"
    t.uuid "session_id"
    t.boolean "cnnl_cart_current", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "financial_order_items", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "financial_order_id", null: false
    t.uuid "memorial_plan_discount_range_id"
    t.string "fnnl_ordr_item_source_type", null: false
    t.uuid "fnnl_ordr_item_source_id", null: false
    t.string "fnnl_ordr_item_receiver_type"
    t.uuid "fnnl_ordr_item_receiver_id"
    t.integer "fnnl_ordr_item_quantity", default: 0, null: false
    t.float "fnnl_ordr_item_unity_price", default: 0.0, null: false
    t.float "fnnl_ordr_item_discount", default: 0.0, null: false
    t.float "fnnl_ordr_item_total", default: 0.0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "financial_orders", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "security_user_id", null: false
    t.uuid "financial_cart_id", null: false
    t.string "fnnl_ordr_number", null: false
    t.string "fnnl_ordr_state", null: false
    t.float "fnnl_ordr_total", default: 0.0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "geonames_alternate_names", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.integer "alternate_name_id"
    t.integer "geonameid"
    t.string "isolanguage"
    t.string "alternate_name"
    t.boolean "is_preferred_name"
    t.boolean "is_short_name"
    t.boolean "is_colloquial"
    t.boolean "is_historic"
    t.string "alternate_name_first_letters"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["alternate_name_first_letters"], name: "index_geonames_alternate_names_on_alternate_name_first_letters"
    t.index ["alternate_name_id"], name: "index_geonames_alternate_names_on_alternate_name_id"
    t.index ["geonameid"], name: "index_geonames_alternate_names_on_geonameid"
    t.index ["isolanguage"], name: "index_geonames_alternate_names_on_isolanguage"
  end

  create_table "geonames_countries", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "iso"
    t.string "iso3"
    t.string "iso_numeric"
    t.string "fips"
    t.string "country"
    t.string "capital"
    t.integer "area"
    t.integer "population"
    t.string "continent"
    t.string "tld"
    t.string "currency_code"
    t.string "currency_name"
    t.string "phone"
    t.string "postal_code_format"
    t.string "postal_code_regex"
    t.string "languages"
    t.integer "geonameid"
    t.string "neighbours"
    t.string "equivalent_fips_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["geonameid"], name: "index_geonames_countries_on_geonameid"
  end

  create_table "geonames_features", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.integer "geonameid"
    t.string "name"
    t.string "asciiname"
    t.text "alternatenames"
    t.float "latitude"
    t.float "longitude"
    t.string "feature_class"
    t.string "feature_code"
    t.string "country_code"
    t.string "cc2"
    t.string "admin1_code"
    t.string "admin2_code"
    t.string "admin3_code"
    t.string "admin4_code"
    t.integer "population"
    t.integer "elevation"
    t.integer "dem"
    t.string "timezone"
    t.datetime "modification"
    t.string "type"
    t.string "asciiname_first_letters"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin1_code"], name: "index_geonames_features_on_admin1_code"
    t.index ["asciiname"], name: "index_geonames_features_on_asciiname"
    t.index ["asciiname_first_letters"], name: "index_geonames_features_on_asciiname_first_letters"
    t.index ["country_code"], name: "index_geonames_features_on_country_code"
    t.index ["geonameid"], name: "index_geonames_features_on_geonameid"
    t.index ["name"], name: "index_geonames_features_on_name"
    t.index ["population"], name: "index_geonames_features_on_population"
    t.index ["type"], name: "index_geonames_features_on_type"
  end

  create_table "geonames_iso_languagecodes", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "iso_639_3"
    t.string "iso_639_2"
    t.string "iso_639_1"
    t.string "language_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memorial_events", id: :serial, force: :cascade do |t|
    t.uuid "memorial_id", null: false
    t.uuid "user_id", null: false
    t.string "mmrl_evnt_title", null: false
    t.string "mmrl_evnt_headline"
    t.text "mmrl_evnt_description"
    t.string "mmrl_evnt_where"
    t.text "mmrl_evnt_address"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "mmrl_evnt_when_date"
    t.time "mmrl_evnt_when_time"
    t.index ["deleted_at"], name: "index_memorial_events_on_deleted_at"
    t.index ["memorial_id"], name: "index_memorial_events_on_memorial_id"
    t.index ["user_id"], name: "index_memorial_events_on_user_id"
  end

  create_table "memorial_guest_books", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "memorial_id", null: false
    t.uuid "user_id", null: false
    t.text "guest_book_message"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memorial_memorial_photos", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "memorial_id", null: false
    t.string "mmry_phto_title"
    t.string "mmry_phto_description"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memorial_memorial_plan_coupons", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "security_user_id", null: false
    t.uuid "financial_order_id"
    t.uuid "memorial_plan_id", null: false
    t.uuid "memorial_id"
    t.boolean "mmrl_plan_cpon_used", null: false
    t.date "mmrl_plan_cpon_used_date"
    t.boolean "mmrl_plan_cpon_active", null: false
    t.date "mmrl_plan_cpon_active_date"
    t.date "mmrl_plan_cpon_vigence_start_date"
    t.date "mmrl_plan_cpon_vigence_end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memorial_memorial_plan_discount_ranges", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "memorial_plan_id", null: false
    t.boolean "mmrl_plan_dsct_rnge_status", null: false
    t.integer "mmrl_plan_dsct_rnge_min", null: false
    t.float "mmrl_plan_dsct_rnge_discount", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memorial_memorial_plans", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.boolean "mmrl_plan_free_default", default: false, null: false
    t.string "mmrl_plan_title", null: false
    t.text "mmrl_plan_description", null: false
    t.integer "mmrl_plan_validade_days", null: false
    t.integer "mmrl_plan_activation_days", null: false
    t.integer "mmrl_plan_videos_limit", null: false
    t.integer "mmrl_plan_photos_limit", null: false
    t.boolean "mmrl_plan_support", default: false, null: false
    t.float "mmrl_plan_price", null: false
    t.boolean "mmrl_plan_active", null: false
    t.date "mmrl_plan_vigence_start_date", null: false
    t.date "mmrl_plan_vigence_end_date"
    t.string "mmrl_plan_color", default: "green", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memorial_memorials", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "security_user_id", null: false
    t.string "mmry_firstname", null: false
    t.string "mmry_lastname", null: false
    t.string "mmry_nicknames"
    t.string "mmry_gender"
    t.text "mmry_biografy"
    t.date "mmry_birth_date", null: false
    t.string "mmry_birth_location", null: false
    t.date "mmry_death_date", null: false
    t.string "mmry_death_location", null: false
    t.string "mmry_cause_of_death"
    t.string "mmry_tombstone_geo"
    t.integer "mmry_privacy"
    t.string "photo"
    t.boolean "enabled", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "mmry_visits", default: 0, null: false
    t.index ["deleted_at"], name: "index_memorial_memorials_on_deleted_at"
  end

  create_table "security_authorizations", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "provider"
    t.string "uid"
    t.uuid "user_id"
    t.string "username"
    t.string "token"
    t.string "secret"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "device_id"
  end

  create_table "security_invites", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "sender_id"
    t.uuid "memorial_id"
    t.string "email", null: false
    t.string "token", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "sent_at"
    t.datetime "accepted_at"
    t.datetime "rejected_at"
    t.datetime "deleted_at"
    t.boolean "admin", default: false, null: false
    t.index ["deleted_at"], name: "index_security_invites_on_deleted_at"
    t.index ["email"], name: "index_security_invites_on_email"
    t.index ["memorial_id"], name: "index_security_invites_on_memorial_id"
    t.index ["sender_id"], name: "index_security_invites_on_sender_id"
    t.index ["token"], name: "index_security_invites_on_token"
  end

  create_table "security_memorial_users", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "memorial_id", null: false
    t.boolean "admin", default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_security_memorial_users_on_deleted_at"
    t.index ["memorial_id"], name: "index_security_memorial_users_on_memorial_id"
    t.index ["user_id"], name: "index_security_memorial_users_on_user_id"
  end

  create_table "security_users", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "username"
    t.string "name"
    t.text "about"
    t.string "location"
    t.string "image"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.boolean "superuser", default: false, null: false
    t.index ["email"], name: "index_security_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_security_users_on_reset_password_token", unique: true
  end

  create_table "sessions", id: :serial, force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  add_foreign_key "bootsy_images", "bootsy_image_galleries", column: "image_gallery_id", name: "fk-plan_coupons-memorials"
  add_foreign_key "financial_cart_items", "financial_carts", name: "fk-cart_items-carts"
  add_foreign_key "financial_cart_items", "memorial_memorial_plan_discount_ranges", column: "memorial_plan_discount_range_id", name: "fk-cart_items-discount_ranges"
  add_foreign_key "financial_carts", "security_users", name: "fk-carts-users"
  add_foreign_key "financial_order_items", "financial_orders", name: "fk-order_items-orders"
  add_foreign_key "financial_order_items", "memorial_memorial_plan_discount_ranges", column: "memorial_plan_discount_range_id", name: "fk-order_items-discount_ranges"
  add_foreign_key "financial_orders", "financial_carts", name: "fk-orders-carts"
  add_foreign_key "financial_orders", "security_users", name: "fk-orders-users"
  add_foreign_key "memorial_events", "memorial_memorials", column: "memorial_id", name: "fk-guest_books-memorials"
  add_foreign_key "memorial_events", "memorial_memorials", column: "memorial_id", name: "fk-guest_books-users"
  add_foreign_key "memorial_memorial_photos", "memorial_memorials", column: "memorial_id", name: "fk-memorial_photos-memorials"
  add_foreign_key "memorial_memorial_plan_coupons", "financial_orders", name: "fk-plan_coupons-orders"
  add_foreign_key "memorial_memorial_plan_coupons", "memorial_memorial_plans", column: "memorial_plan_id", name: "fk-plan_coupons-plans"
  add_foreign_key "memorial_memorial_plan_coupons", "memorial_memorials", column: "memorial_id", name: "fk-plan_coupons-memorials"
  add_foreign_key "memorial_memorial_plan_coupons", "security_users", name: "fk-plan_coupons-users"
  add_foreign_key "memorial_memorial_plan_discount_ranges", "memorial_memorial_plans", column: "memorial_plan_id", name: "fk-discount_ranges-memorial_plans"
  add_foreign_key "memorial_memorials", "security_users", name: "fk-memorials-user"
  add_foreign_key "security_authorizations", "security_users", column: "user_id", name: "fk-authorizations-user"
  add_foreign_key "security_invites", "memorial_memorials", column: "memorial_id", name: "fk-invites-memorials"
  add_foreign_key "security_invites", "security_users", column: "sender_id", name: "fk-invites-users"
  add_foreign_key "security_memorial_users", "memorial_memorials", column: "memorial_id", name: "fk-memorial_users-memorial_id"
  add_foreign_key "security_memorial_users", "security_users", column: "user_id", name: "fk-memorial_users-user_id"
end
