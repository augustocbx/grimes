class ConvertPrimaryKeyToUuidInGuestBooks < ActiveRecord::Migration[5.0]
  def up
    add_column :memorial_guest_books, :uuid, :uuid, default: "uuid_generate_v4()", null: false

    change_table :memorial_guest_books do |t|
      t.remove :id
      t.rename :uuid, :id
    end
    execute "ALTER TABLE memorial_guest_books ADD PRIMARY KEY (id);"
  end

  def down
    raise ActiveRecord::IrreversibleMigration, "Can't revert uuid primary key"
  end
end
