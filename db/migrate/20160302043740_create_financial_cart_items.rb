# frozen_string_literal: true
class CreateFinancialCartItems < ActiveRecord::Migration[5.0]
  def change
    create_table :financial_cart_items, id: :uuid do |t|
      t.uuid :financial_cart_id, null: false
      t.uuid :memorial_plan_discount_range_id, null: true
      t.references :fnnl_cart_item_source, type: :uuid, polymorphic: true, null: false, index: {name: 'ind-fcart_items-item_source'}
      t.references :fnnl_cart_item_receiver, type: :uuid, polymorphic: true, null: true, index: {name: 'ind-fcart_items-item_receiver'}
      t.integer :fnnl_cart_item_quantity, null: false
      t.float :fnnl_cart_item_unity_price, null: false # Full price, without discount
      t.float :fnnl_cart_item_discount, null: false

      t.timestamps null: false

      t.foreign_key :financial_carts, column: 'financial_cart_id', name: 'fk-cart_items-carts'
      t.foreign_key :memorial_memorial_plan_discount_ranges, column: 'memorial_plan_discount_range_id', name: 'fk-cart_items-discount_ranges'
    end
  end
end
