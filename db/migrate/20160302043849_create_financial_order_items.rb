# frozen_string_literal: true
class CreateFinancialOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :financial_order_items, id: :uuid do |t|
      t.references :financial_order, type: :uuid, null: false
      t.references :memorial_plan_discount_range, type: :uuid, null: true
      t.references :fnnl_ordr_item_source, type: :uuid, polymorphic: true, null: false, index: {name: 'ind-financial_order_items-source_id'}
      t.references :fnnl_ordr_item_receiver, type: :uuid, polymorphic: true, null: true, index: {name: 'ind-financial_order_items-receiver_id'}

      t.integer :fnnl_ordr_item_quantity, null: false, default: 0
      t.float   :fnnl_ordr_item_unity_price, null: false, default: 0.00
      t.float   :fnnl_ordr_item_discount, null: false, default: 0.00
      t.float   :fnnl_ordr_item_total, null: false, default: 0.00

      t.timestamps null: false

      t.foreign_key :financial_orders, column: 'financial_order_id', name: 'fk-order_items-orders'
      t.foreign_key :memorial_memorial_plan_discount_ranges, column: 'memorial_plan_discount_range_id', name: 'fk-order_items-discount_ranges'
    end
  end
end
