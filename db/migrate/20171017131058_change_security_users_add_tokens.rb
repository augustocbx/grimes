class ChangeSecurityUsersAddTokens < ActiveRecord::Migration[5.0]
  def change
    change_table(:security_users) do |t|
      t.string :provider, :null => false, :default => "email"
      t.string :uid, :null => true, :default => ""
      t.remove :token
      t.json :tokens

    end
  end
end
