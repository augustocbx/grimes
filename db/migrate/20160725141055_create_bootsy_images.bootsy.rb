# frozen_string_literal: true
# This migration comes from bootsy (originally 20120624171333)
class CreateBootsyImages < ActiveRecord::Migration[5.0]
  def change
    create_table :bootsy_images, id: :uuid do |t|
      t.string :image_file
      t.uuid :image_gallery_id
      t.timestamps

      t.foreign_key :bootsy_image_galleries, column: 'image_gallery_id', name: 'fk-plan_coupons-memorials'
    end
  end
end
