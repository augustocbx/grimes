class CreateSecurityMemorialUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :security_memorial_users, id: :uuid do |t|
      t.uuid :user_id, null:false
      t.uuid :memorial_id, null:false
      t.boolean :admin, null: false, default: false
      t.datetime :deleted_at
      t.timestamps
      t.index :user_id
      t.index :memorial_id
      t.index :deleted_at
      t.foreign_key :security_users, column: :user_id, name: 'fk-memorial_users-user_id'
      t.foreign_key :memorial_memorials, column: :memorial_id, name: 'fk-memorial_users-memorial_id'
    end
  end
end
