class ChangeSecurityInvitesAddAdmin < ActiveRecord::Migration[5.0]
  def change
    change_table :security_invites do |t|
      t.boolean :admin, null:false, default: false
    end
  end
end
