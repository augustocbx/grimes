# frozen_string_literal: true
class CreateSecurityAuthorizations < ActiveRecord::Migration[5.0]
  def change
    create_table :security_authorizations, id: :uuid do |t|
      t.string :provider
      t.string :uid
      t.uuid :user_id
      t.string :username
      t.string :token
      t.string :secret

      t.timestamps null: false
      t.foreign_key :security_users, column: 'user_id', name: 'fk-authorizations-user'
    end
  end
end
