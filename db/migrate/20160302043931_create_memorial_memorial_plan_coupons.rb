# frozen_string_literal: true
class CreateMemorialMemorialPlanCoupons < ActiveRecord::Migration[5.0]
  def change
    create_table :memorial_memorial_plan_coupons, id: :uuid do |t|
      t.references  :security_user, type: :uuid, null: false
      t.references  :financial_order, type: :uuid, null: true
      t.references  :memorial_plan, type: :uuid, null: false
      t.references  :memorial, type: :uuid, null: true

      t.boolean     :mmrl_plan_cpon_used, null: false
      t.date        :mmrl_plan_cpon_used_date, null: true

      t.boolean     :mmrl_plan_cpon_active, null: false
      t.date        :mmrl_plan_cpon_active_date, null: true

      t.date        :mmrl_plan_cpon_vigence_start_date, null: true
      t.date        :mmrl_plan_cpon_vigence_end_date, null: true

      t.timestamps null: false

      t.foreign_key :security_users, column: 'security_user_id', name: 'fk-plan_coupons-users'
      t.foreign_key :financial_orders, column: 'financial_order_id', name: 'fk-plan_coupons-orders'
      t.foreign_key :memorial_memorial_plans, column: 'memorial_plan_id', name: 'fk-plan_coupons-plans'
      t.foreign_key :memorial_memorials, column: 'memorial_id', name: 'fk-plan_coupons-memorials'
    end
  end
end
