# frozen_string_literal: true
class CreateFinancialOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :financial_orders, id: :uuid do |t|
      t.uuid :security_user_id, null: false
      t.uuid :financial_cart_id, null: false
      t.string :fnnl_ordr_number, null: false
      t.string :fnnl_ordr_state, null: false

      t.float :fnnl_ordr_total, null: false, default: 0.00

      t.timestamps null: false

      t.foreign_key :security_users, column: 'security_user_id', name: 'fk-orders-users'
      t.foreign_key :financial_carts, column: 'financial_cart_id', name: 'fk-orders-carts'
    end
  end
end
