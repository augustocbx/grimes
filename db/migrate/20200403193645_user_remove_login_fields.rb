class UserRemoveLoginFields < ActiveRecord::Migration[5.2]
  def change
    remove_column :security_users, :provider, :string
    remove_column :security_users, :uid, type: :string
    remove_column :security_users, :authentication_token, type: :string, limit: 30
  end
end
