class ChangeSecurityUsersAddToken < ActiveRecord::Migration[5.0]
  def change
    change_table(:security_users) do |t|
      t.json :token
    end
  end
end
