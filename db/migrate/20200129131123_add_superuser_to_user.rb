class AddSuperuserToUser < ActiveRecord::Migration[5.2]
  def change
    change_table(:security_users) do |t|
      t.boolean :superuser, default: false, null: false
    end
  end
end
