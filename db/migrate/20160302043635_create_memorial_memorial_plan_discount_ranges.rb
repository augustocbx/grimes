# frozen_string_literal: true
class CreateMemorialMemorialPlanDiscountRanges < ActiveRecord::Migration[5.0]
  def change
    create_table :memorial_memorial_plan_discount_ranges, id: :uuid do |t|
      t.uuid :memorial_plan_id, null: false
      t.boolean :mmrl_plan_dsct_rnge_status, null: false
      t.integer :mmrl_plan_dsct_rnge_min, null: false
      t.float :mmrl_plan_dsct_rnge_discount, null: false

      t.timestamps null: false
      t.foreign_key :memorial_memorial_plans, column: 'memorial_plan_id', name: 'fk-discount_ranges-memorial_plans'
    end
  end
end
