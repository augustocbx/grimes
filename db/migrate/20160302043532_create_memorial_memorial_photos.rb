# frozen_string_literal: true
class CreateMemorialMemorialPhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :memorial_memorial_photos, id: :uuid do |t|
      t.uuid :memorial_id, null: false
      t.string :mmry_phto_title
      t.string :mmry_phto_description
      t.string :image

      t.timestamps null: false
      t.foreign_key :memorial_memorials, column: 'memorial_id', name: 'fk-memorial_photos-memorials'
    end
  end
end
