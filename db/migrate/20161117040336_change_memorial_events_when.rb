class ChangeMemorialEventsWhen < ActiveRecord::Migration[5.0]
  def change
    remove_column :memorial_events, :mmrl_evnt_when, :datetime
    change_table :memorial_events do |t|
      t.date :mmrl_evnt_when_date
      t.time :mmrl_evnt_when_time
    end
  end
end
