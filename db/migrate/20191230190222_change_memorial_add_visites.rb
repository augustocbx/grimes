class ChangeMemorialAddVisites < ActiveRecord::Migration[5.2]
  def change
    change_table :memorial_memorials do |t|
      t.integer :mmry_visits, null:false, default: 0
    end
  end
end
