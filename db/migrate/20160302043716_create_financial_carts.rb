# frozen_string_literal: true
class CreateFinancialCarts < ActiveRecord::Migration[5.0]
  def change
    create_table :financial_carts, id: :uuid do |t|
      t.uuid :security_user_id
      t.uuid :session_id
      t.boolean :cnnl_cart_current, null: false, default: true

      t.timestamps null: false
      t.foreign_key :security_users, column: 'security_user_id', name: 'fk-carts-users'
    end
  end
end
