class CreateMemorialEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :memorial_events, id: :uuid do |t|
      t.uuid :memorial_id, null: false
      t.uuid :user_id, null: false
      t.string :mmrl_evnt_title, null: false
      t.string :mmrl_evnt_headline
      t.text :mmrl_evnt_description
      t.string :mmrl_evnt_where
      t.text :mmrl_evnt_address
      t.datetime :mmrl_evnt_when
      t.datetime :deleted_at

      t.timestamps

      t.index :memorial_id
      t.index :user_id
      t.index :deleted_at

      t.foreign_key :memorial_memorials, column: :memorial_id, name: 'fk-guest_books-memorials'
      t.foreign_key :memorial_memorials, column: :memorial_id, name: 'fk-guest_books-users'
    end
  end
end
