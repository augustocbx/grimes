# frozen_string_literal: true
# This migration comes from bootsy (originally 20120628124845)
class CreateBootsyImageGalleries < ActiveRecord::Migration[5.0]
  def change
    create_table :bootsy_image_galleries, id: :uuid do |t|
      t.references :bootsy_resource, type: :uuid, polymorphic: true, index: {name: 'ind-bootsy_image_galleries-resource_id'}
      t.timestamps
    end
  end
end
