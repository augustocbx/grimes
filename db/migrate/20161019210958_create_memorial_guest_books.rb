class CreateMemorialGuestBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :memorial_guest_books do |t|
      t.uuid :memorial_id, null: false
      t.uuid :user_id, null: false
      t.text :guest_book_message
      t.datetime :deleted_at

      t.timestamps

      t.index :memorial_id
      t.index :user_id
      t.index :deleted_at

      t.foreign_key :memorial_memorials, column: :memorial_id, name: 'fk-guest_books-memorials'
      t.foreign_key :memorial_memorials, column: :memorial_id, name: 'fk-guest_books-users'
    end
  end
end
