# frozen_string_literal: true
class CreateSecurityInvites < ActiveRecord::Migration[5.0]
  def change
    create_table :security_invites, id: :uuid do |t|
      t.uuid :sender_id
      t.uuid :memorial_id
      t.string :email, null: false
      t.string :token, null: false
      t.timestamps
      t.datetime :sent_at
      t.datetime :accepted_at
      t.datetime :rejected_at
      t.datetime :deleted_at
      t.index :email
      t.index :token
      t.index :sender_id
      t.index :memorial_id
      t.index :deleted_at

      t.timestamps

      t.foreign_key :security_users, column: 'sender_id', name: 'fk-invites-users'
      t.foreign_key :memorial_memorials, column: 'memorial_id', name: 'fk-invites-memorials'
    end
  end
end
