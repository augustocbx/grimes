# frozen_string_literal: true
class CreateMemorialMemorials < ActiveRecord::Migration[5.0]
  def change
    create_table :memorial_memorials, id: :uuid do |t|
      t.uuid    :security_user_id, null: false
      t.string  :mmry_firstname, null: false
      t.string  :mmry_lastname, null: false
      t.string  :mmry_nicknames
      t.string  :mmry_gender
      t.text    :mmry_biografy
      t.date    :mmry_birth_date, null: false
      t.string  :mmry_birth_location, null: false
      t.date    :mmry_death_date, null: false
      t.string  :mmry_death_location, null: false
      t.string  :mmry_cause_of_death
      t.string  :mmry_tombstone_geo
      t.integer :mmry_privacy
      t.string  :photo
      t.boolean :enabled, null: false, default: true

      t.timestamps null: false
      t.datetime :deleted_at

      t.index :deleted_at
      t.foreign_key :security_users, column: 'security_user_id', name: 'fk-memorials-user'
    end
  end
end
