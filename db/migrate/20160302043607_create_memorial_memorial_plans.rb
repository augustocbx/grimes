# frozen_string_literal: true
class CreateMemorialMemorialPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :memorial_memorial_plans, id: :uuid do |t|
      t.boolean   :mmrl_plan_free_default, null: false, default: false
      t.string    :mmrl_plan_title, null: false
      t.text      :mmrl_plan_description, null: false
      t.integer   :mmrl_plan_validade_days, null: false
      t.integer   :mmrl_plan_activation_days, null: false
      t.integer   :mmrl_plan_videos_limit, null: false
      t.integer   :mmrl_plan_photos_limit, null: false
      t.boolean   :mmrl_plan_support, null: false, default: false
      t.float     :mmrl_plan_price, null: false
      t.boolean   :mmrl_plan_active, null: false
      t.date      :mmrl_plan_vigence_start_date, null: false
      t.date      :mmrl_plan_vigence_end_date, null: true
      t.string    :mmrl_plan_color, null: false, default: 'green'

      t.timestamps null: false
    end
  end
end
