class AddAuthenticationTokenToUsers < ActiveRecord::Migration[5.0]
  def change
    change_table(:security_users) do |t|
      t.string :authentication_token, limit: 30
      t.index :authentication_token, unique: true
      t.remove :tokens
    end
  end
end
