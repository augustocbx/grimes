class AddDeviceIdToAuthentication < ActiveRecord::Migration[5.2]
  def change
    change_table :security_authorizations do |t|
      t.string :device_id
    end
  end
end
