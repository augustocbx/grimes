# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorials
#
#  id                  :uuid             not null, primary key
#  security_user_id    :uuid             not null
#  mmry_firstname      :string           not null
#  mmry_lastname       :string           not null
#  mmry_nicknames      :string
#  mmry_gender         :string
#  mmry_biografy       :text
#  mmry_birth_date     :date             not null
#  mmry_birth_location :string           not null
#  mmry_death_date     :date             not null
#  mmry_death_location :string           not null
#  mmry_cause_of_death :string
#  mmry_tombstone_geo  :string
#  mmry_privacy        :integer
#  photo               :string
#  enabled             :boolean          default(TRUE), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  deleted_at          :datetime
#  mmry_visits         :integer          default(0), not null
#
# Indexes
#
#  index_memorial_memorials_on_deleted_at  (deleted_at)
#

FactoryBot.define do
  factory :memorial_memorial, class: Memorial::Memorial do |_f|
    trait(:minimal_valid) do
      mmry_firstname {Faker::Name.first_name}
      mmry_lastname {Faker::Name.last_name}
      mmry_gender { (rand(1..2) == 1 ? 'male' : 'female') }
      mmry_birth_date {Faker::Date.between(Date.today, Date.today - 100.years)}
      mmry_death_date {Faker::Date.between(Date.today, Date.today - 100.years)}
      mmry_birth_location {Faker::Address.city}
      mmry_death_location {Faker::Address.city}
    end
    trait(:full) do
      mmry_firstname {Faker::Name.first_name}
      mmry_lastname {Faker::Name.last_name}
      mmry_nicknames {[Faker::Name.name, Faker::Name.name, Faker::Name.name, Faker::Name.name, Faker::Name.name, Faker::Name.name].join(', ')}
      mmry_gender { (rand(1..2) == 1 ? 'male' : 'female') }
      mmry_biografy {Faker::Lorem.paragraphs}
      mmry_birth_date {Faker::Date.between(Date.today, Date.today - 100.years)}
      mmry_death_date {Faker::Date.between(Date.today, Date.today - 100.years)}
      mmry_birth_location {Faker::Address.city}
      mmry_death_location {Faker::Address.city}
      mmry_tombstone_geo {[Faker::Address.latitude, Faker::Address.longitude].join(', ')}
      mmry_privacy { rand(1..3) }
    end
  end
end
