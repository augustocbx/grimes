# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_guest_books
#
#  memorial_id        :uuid             not null
#  user_id            :uuid             not null
#  guest_book_message :text
#  deleted_at         :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  id                 :uuid             not null, primary key
#

FactoryBot.define do
  factory :guest_book, class: Memorial::GuestBook do |_f|
    guest_book_message {Faker::Lorem.paragraph(3)}
  end
end
