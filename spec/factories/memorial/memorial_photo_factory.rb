# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorial_photos
#
#  id                    :uuid             not null, primary key
#  memorial_id           :uuid             not null
#  mmry_phto_title       :string
#  mmry_phto_description :string
#  image                 :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

include ActionDispatch::TestProcess
FactoryBot.define do
  factory :memorial_memorial_photo, class: Memorial::MemorialPhoto do |_f|
    trait(:picture) do
      image { fixture_file_upload(Rails.root.join('spec/fixtures/photo1.jpg'), 'image/png') }
    end
    trait(:full) do
      mmry_phto_title {Faker::Lorem.paragraph}
      image { fixture_file_upload(Rails.root.join('spec/fixtures/photo1.jpg'), 'image/png') }
    end
  end
end
