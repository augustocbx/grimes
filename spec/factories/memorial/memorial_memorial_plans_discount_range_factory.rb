# frozen_string_literal: true
FactoryBot.define do
  factory :memorial_memorial_plan_discount_range, class: 'Memorial::MemorialPlanDiscountRange' do
    mmrl_plan_dsct_rnge_status {true}

    trait(:between_3_to_10_discount_of_15) do
      mmrl_plan_dsct_rnge_min {3}
      mmrl_plan_dsct_rnge_discount {15}
    end

    trait(:between_11_to_20_discount_of_20) do
      mmrl_plan_dsct_rnge_min {11}
      mmrl_plan_dsct_rnge_discount {20}
    end

    trait(:over_20_discount_of_25) do
      mmrl_plan_dsct_rnge_min {21}
      mmrl_plan_dsct_rnge_discount {25}
    end
  end
end
