# frozen_string_literal: true
FactoryBot.define do
  factory :memorial_memorial_plan, class: 'Memorial::MemorialPlan' do
    trait(:just_title) do
      mmrl_plan_title {Faker::Name.name}
    end
    trait(:memorial_plan_full) do
      mmrl_plan_title           {Faker::Name.name}
      mmrl_plan_description     {Faker::Lorem.paragraphs(5)}
      mmrl_plan_validade_days   { ((Date.today + rand(1..5).years) - Date.today).days }
      mmrl_plan_activation_days { ((Date.today + rand(1..2).years) - Date.today).days }
      mmrl_plan_videos_limit    { rand(-1..5) }
      mmrl_plan_photos_limit    { rand(-1..5) }
      mmrl_plan_support         { (rand(0..1) == 1 ? true : false) }
      mmrl_plan_price           { rand(7000..15_000) / 100 }
    end
    trait(:memorial_plan_free) do
      mmrl_plan_free_default    {true}
      mmrl_plan_title           {'Free plan'}
      mmrl_plan_description     {'This is a free plan'}
      mmrl_plan_validade_days   {-1}
      mmrl_plan_activation_days {-1}
      mmrl_plan_videos_limit    {0}
      mmrl_plan_photos_limit    {0}
      mmrl_plan_support         {false}
      mmrl_plan_price           {0}
    end
    trait(:memorial_plan_1_year) do
      mmrl_plan_title           {Faker::Name.name}
      mmrl_plan_description     {Faker::Lorem.paragraphs(5)}
      mmrl_plan_validade_days   { ((Date.today + 1.years) - Date.today).days }
      mmrl_plan_activation_days { ((Date.today + 1.year) - Date.today).days }
      mmrl_plan_videos_limit    {-1}
      mmrl_plan_photos_limit    {-1}
      mmrl_plan_support         {true}
      mmrl_plan_price           {70}
    end
    trait(:memorial_plan_3_years) do
      mmrl_plan_title           {Faker::Name.name}
      mmrl_plan_description     {Faker::Lorem.paragraphs(5)}
      mmrl_plan_validade_days   { ((Date.today + 3.years) - Date.today).days }
      mmrl_plan_activation_days { ((Date.today + 3.year) - Date.today).days }
      mmrl_plan_videos_limit    {-1}
      mmrl_plan_photos_limit    {-1}
      mmrl_plan_support         {true}
      mmrl_plan_price           {150}
    end
    trait(:memorial_plan_unlimited) do
      mmrl_plan_title           {Faker::Name.name}
      mmrl_plan_description     {Faker::Lorem.paragraphs(5)}
      mmrl_plan_validade_days   {-1}
      mmrl_plan_activation_days {-1}
      mmrl_plan_videos_limit    {-1}
      mmrl_plan_photos_limit    {-1}
      mmrl_plan_support         {true}
      mmrl_plan_price           {200}
    end
  end
end
