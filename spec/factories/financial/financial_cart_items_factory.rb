# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_cart_items
#
#  id                              :integer          not null, primary key
#  financial_cart_id               :integer          not null
#  memorial_plan_discount_range_id :integer
#  fnnl_cart_item_source_id        :integer          not null
#  fnnl_cart_item_source_type      :string           not null
#  fnnl_cart_item_receiver_id      :integer
#  fnnl_cart_item_receiver_type    :string
#  fnnl_cart_item_quantity         :integer          not null
#  fnnl_cart_item_unity_price      :float            not null
#  fnnl_cart_item_discount         :float            not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

FactoryBot.define do
  factory :financial_cart_item, class: Financial::CartItem do
  end
end
