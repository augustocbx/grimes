# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_carts
#
#  id                :integer          not null, primary key
#  security_user_id  :integer
#  session_id        :integer
#  cnnl_cart_current :boolean          default(TRUE), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

FactoryBot.define do
  factory :financial_cart, class: Financial::Cart do
    trait(:logged_current) do
      association :security_user, factory: [:security_user, :common_user]
      cnnl_cart_current {true}
    end
    trait(:logged_not_current) do
      association :security_user, factory: [:security_user, :common_user]
      cnnl_cart_current {false}
    end
  end
end
