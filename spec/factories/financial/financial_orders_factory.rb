# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_orders
#
#  id                :integer          not null, primary key
#  security_user_id  :integer          not null
#  financial_cart_id :integer          not null
#  fnnl_ordr_number  :string           not null
#  fnnl_ordr_state   :string           not null
#  fnnl_ordr_total   :float            not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

FactoryBot.define do
  factory :financial_order, class: Financial::Order do
    association       :security_user, factory: [:security_user, :common_user]
    association       :financial_cart, factory: [:financial_cart, :logged_not_current]
    trait(:approved) do
      fnnl_ordr_number {SecureRandom.uuid}
      fnnl_ordr_state {'A'}

      after(:create) do |financial_order, _evaluator|
        (1..(rand(2..5))).each do
          financial_order.financial_order_items << build(:financial_order_item)
        end
      end
    end
    trait(:reproved) do
      fnnl_ordr_number {SecureRandom.uuid}
      fnnl_ordr_state {'R'}

      after(:create) do |financial_order, _evaluator|
        (1..(rand(2..5))).each do
          financial_order.financial_order_items << build(:financial_order_item)
        end
      end
    end
    trait(:new) do
      fnnl_ordr_number {SecureRandom.uuid}
      fnnl_ordr_state {'N'}
    end
  end
end
