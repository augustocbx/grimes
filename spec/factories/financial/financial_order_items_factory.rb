# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_order_items
#
#  id                              :integer          not null, primary key
#  financial_order_id              :integer          not null
#  fnnl_ordr_item_source_id        :integer          not null
#  fnnl_ordr_item_source_type      :string           not null
#  fnnl_ordr_item_receiver_id      :integer
#  fnnl_ordr_item_receiver_type    :string
#  memorial_plan_discount_range_id :integer
#  fnnl_ordr_item_quantity         :integer          not null
#  fnnl_ordr_item_unity_price      :float            not null
#  fnnl_ordr_item_discount         :float            not null
#  fnnl_ordr_item_total            :float            not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

FactoryBot.define do
  factory :financial_order_item, class: Financial::OrderItem do
    association :fnnl_ordr_item_source, factory: [:memorial_memorial_plan, :memorial_plan_full]
    fnnl_ordr_item_quantity { rand(1..10) }
    fnnl_ordr_item_unity_price { rand(50.00..100.00) }
    fnnl_ordr_item_discount {0.00}
  end
end
