# frozen_string_literal: true
# == Schema Information
#
# Table name: security_users
#
#  id                     :uuid             not null, primary key
#  username               :string
#  name                   :string
#  about                  :text
#  location               :string
#  image                  :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  superuser              :boolean          default(FALSE), not null
#
# Indexes
#
#  index_security_users_on_email                 (email) UNIQUE
#  index_security_users_on_reset_password_token  (reset_password_token) UNIQUE
#

FactoryBot.define do
  factory :security_user, class: Security::User do |_f|
    name  { Faker::Name.name }
    email { Faker::Internet.email }
    trait(:google_sign_up) do
      password {Devise.friendly_token[0, 10]}
    end
    trait(:common_user) do
      password {Devise.friendly_token[0, 10]}
    end
  end
end
