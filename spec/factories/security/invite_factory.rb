# frozen_string_literal: true
# == Schema Information
#
# Table name: security_invites
#
#  id          :uuid             not null, primary key
#  sender_id   :uuid
#  memorial_id :uuid
#  email       :string           not null
#  token       :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  sent_at     :datetime
#  accepted_at :datetime
#  rejected_at :datetime
#  deleted_at  :datetime
#  admin       :boolean          default(FALSE), not null
#
# Indexes
#
#  index_security_invites_on_deleted_at   (deleted_at)
#  index_security_invites_on_email        (email)
#  index_security_invites_on_memorial_id  (memorial_id)
#  index_security_invites_on_sender_id    (sender_id)
#  index_security_invites_on_token        (token)
#

FactoryBot.define do
  factory :security_invite, class: Security::Invite do |f|
    email { Faker::Internet.email }
    token { f.generate_token }
  end
end
