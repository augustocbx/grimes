# frozen_string_literal: true
module ControllerMacros
  def login_user
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      @user_login = FactoryBot.create(:security_user, :common_user)
      sign_in @user_login
    end
  end
end
