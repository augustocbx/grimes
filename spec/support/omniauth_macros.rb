# frozen_string_literal: true
module OmniauthMacros
  def mock_auth_hash
    # The mock_auth configuration allows you to set per-provider (or default)
    # authentication hashes to return during integration testing.
    @hash_info = {
      name: Faker::Name.first_name,
      email: Faker::Internet.email
    }
    @hash_credentials = {
      token: 'ya29uAHqoGAyAa53BGCXXPqq-iphZ4cSHsNBr2Rl0DYulo8a5h7mWyov2hrbvatkwz1fSQHP'
    }

    OmniAuth.config.mock_auth[:google_oauth2] = OmniAuth::AuthHash.new(params_google)
    OmniAuth.config.mock_auth[:twitter] = OmniAuth::AuthHash.new(params_twitter)
    OmniAuth.config.mock_auth[:facebook] = OmniAuth::AuthHash.new(params_facebook)
  end

  private

  def params_google
    {
      provider: 'google_oauth2',
      uid: '123545',
      info: OmniAuth::AuthHash::InfoHash.new(@hash_info),
      credentials: OmniAuth::AuthHash::InfoHash.new(@hash_credentials)
    }
  end

  def params_twitter
    {
      provider: 'twitter',
      uid: '123545',
      info: OmniAuth::AuthHash::InfoHash.new(@hash_info),
      credentials: OmniAuth::AuthHash::InfoHash.new(@hash_credentials)
    }
  end

  def params_facebook
    {
      provider: 'facebook',
      uid: '123545',
      info: OmniAuth::AuthHash::InfoHash.new(@hash_info),
      credentials: OmniAuth::AuthHash::InfoHash.new(@hash_credentials)
    }
  end
end
