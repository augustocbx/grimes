# frozen_string_literal: true
shared_context 'order_shared' do
  let(:order_new) do
    create(:financial_order, :new)
  end
  let(:order_approved) do
    create(:financial_order, :approved)
  end
  # let(:order_reproved) do
  #   create(:financial_order, :reproved)
  # end
end
