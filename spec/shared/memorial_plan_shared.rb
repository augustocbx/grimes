# frozen_string_literal: true
shared_context 'memorial_plan_shared' do
  let(:memorial_plan_full) do
    create(:memorial_memorial_plan, :memorial_plan_full)
  end
  let(:memorial_plan_free) do
    create(:memorial_memorial_plan, :memorial_plan_free)
  end
  let(:memorial_plan_1_year) do
    create(:memorial_memorial_plan, :memorial_plan_1_year)
  end
  let(:memorial_plan_3_years) do
    create(:memorial_memorial_plan, :memorial_plan_3_years)
  end
  # let(:memorial_plan_unlimited) do
  #   create(:memorial_memorial_plan, :memorial_plan_unlimited)
  # end
end
