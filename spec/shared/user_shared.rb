# frozen_string_literal: true
shared_context 'user_shared' do
  let(:logged_user) do
    create(:security_user, :common_user)
  end
  let(:any_user) do
    create(:security_user, :common_user)
  end
end
