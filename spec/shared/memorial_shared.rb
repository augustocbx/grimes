# frozen_string_literal: true
require 'shared/user_shared'
shared_context 'memorial_shared' do
  include_context 'user_shared'
  let(:memorial_valid_basic) do
    if @user_login.present?
      create(:memorial_memorial, :minimal_valid, user: @user_login)
    else
      create(:memorial_memorial, :minimal_valid, user: create(:security_user, :common_user))
    end
  end
end
