require 'rails_helper'
require 'shared/order_shared'

RSpec.describe Pagseguro::NotificationsController, type: :controller do
  login_user
  describe 'POST #create' do
    include_context 'order_shared'
    let(:xml) { Nokogiri::XML(File.read('./spec/fixtures/pagseguro/transaction_success.xml')) }
    let(:response) do
      response = instance_double("PagSeguro::Transaction")
      allow(response).to receive(:reference) { order_approved.id }
      allow(response).to receive(:errors) { [] }
      allow(response).to receive(:status) { instance_double("PagSeguro::PaymentStatus", id: '3') }
      allow(response).to receive(:items) do
        order_approved.financial_order_items.map do |item|
          instance_double("PagSeguro::Item", id: item.id)
        end
      end
      response
    end
    it 'Payment OK' do
      allow(PagSeguro::Transaction).to receive(:find_by_notification_code) { response }
      notificationCode = '9E884542-81B3-4419-9A75-BCC6FB495EF1'
      post(:create, params: {notificationCode: notificationCode})
    end
    it 'Payment OK' do

      response = instance_double("PagSeguro::Transaction")
      allow(response).to receive(:reference) { order_approved.id }
      allow(response).to receive(:errors) { [] }
      allow(response).to receive(:status) { instance_double("PagSeguro::PaymentStatus", id: '6') }
      allow(response).to receive(:items) do
        order_approved.financial_order_items.map do |item|
          instance_double("PagSeguro::Item", id: item.id)
        end
      end
      response

      allow(PagSeguro::Transaction).to receive(:find_by_notification_code) { response }
      notificationCode = '9E884542-81B3-4419-9A75-BCC6FB495EF1'
      post(:create, params: {notificationCode: notificationCode})
    end
  end
end