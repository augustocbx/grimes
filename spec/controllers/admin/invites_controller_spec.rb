# frozen_string_literal: true
require 'rails_helper'
require 'shared/memorial_shared'

RSpec.describe Admin::InvitesController, type: :controller do
  login_user
  include_context 'memorial_shared'
  include_context 'user_shared'

  describe 'GET #index' do
    it 'there is not invites' do
      get :index, params: { memorial_id: memorial_valid_basic.id }
      expect(assigns(:invite)).to be_new_record
      expect(assigns(:invites)).to be_empty
    end

    it 'there are invites' do
      create_list(:security_invite, 2, memorial: memorial_valid_basic, sender: any_user, sent_at: DateTime.now)
      expect {create_list(:security_invite, 2, sent_at: DateTime.now)}.to raise_error ActiveRecord::RecordInvalid
      get :index, params: { memorial_id: memorial_valid_basic.id }
      expect(assigns(:invite)).to be_new_record
      expect(assigns(:invites).count).to be == 2
    end
  end

  describe 'POST #create' do
    it 'create a invite to a new email' do
      create(:security_invite, memorial: memorial_valid_basic, sender: any_user, sent_at: DateTime.now)
      post :create, params: { memorial_id: memorial_valid_basic.id, security_invite: { memorial_id: memorial_valid_basic.id, email: Faker::Internet.email } }
      expect(assigns(:invite)).to be_persisted
      expect(response).to redirect_to(admin_memorial_invites_path(memorial_id: memorial_valid_basic.id))
    end

    it 'create a invite to a email that was sent less than an hour' do
      email = Faker::Internet.email
      create(:security_invite, memorial: memorial_valid_basic, sender: any_user, sent_at: DateTime.now, email: email)
      post :create, params: { memorial_id: memorial_valid_basic.id, security_invite: { memorial_id: memorial_valid_basic.id, email: email } }
      expect(response).to redirect_to(admin_memorial_invites_path(memorial_id: memorial_valid_basic.id))
    end

    it 'create a invite to a email that already was accepted' do
      email = Faker::Internet.email
      create(:security_invite, memorial: memorial_valid_basic, sent_at: DateTime.now - 1.hour, sender: any_user, accepted_at: DateTime.now - 1.minute, email: email)
      post :create, params: { memorial_id: memorial_valid_basic.id, security_invite: { memorial_id: memorial_valid_basic.id, email: email } }
      expect(assigns(:invite).alert).to include('Convite já foi aceito por')
      expect(response).to redirect_to(admin_memorial_invites_path(memorial_id: memorial_valid_basic.id))
    end

    it 'try create a invite withuot email' do
      post :create, params: { memorial_id: memorial_valid_basic.id, security_invite: { memorial_id: memorial_valid_basic.id} }
      expect(flash[:alert]).to include('Você não pode criar um convite sem informar o email')
    end

    it 'try create a invite with 3 emails' do
      emails = [Faker::Internet.email, Faker::Internet.email, Faker::Internet.email]
      post :create, params: { memorial_id: memorial_valid_basic.id, emails: emails, security_invite: { memorial_id: memorial_valid_basic.id } }
      assigns(:invites).each do |invite|
        expect(invite).to be_persisted
      end
    end

    it 'try create a invite with 3 emails and one that already was accepted' do
      email = Faker::Internet.email
      create(:security_invite, memorial: memorial_valid_basic, sender: any_user, sent_at: DateTime.now, email: email)
      emails = [Faker::Internet.email, Faker::Internet.email, email]
      post :create, params: { memorial_id: memorial_valid_basic.id, emails: emails, security_invite: { memorial_id: memorial_valid_basic.id } }
      assigns(:invites).each do |invite|
        expect(invite).to be_persisted
      end
      expect(flash[:alert]).to include('Convite já enviado para')
    end
  end
  describe 'DELETE #destroy' do
    it 'destroy an existed invite' do
      invite = create(:security_invite, memorial: memorial_valid_basic, sender: any_user, sent_at: DateTime.now)
      delete :destroy, params: {memorial_id: memorial_valid_basic.id, id: invite.id}
      invite.reload
      expect(invite.deleted?).to be_truthy
    end

  end
  describe 'PUT #update' do
    it 'destroy an existed invite' do
      email = Faker::Internet.email
      invite = create(:security_invite, memorial: memorial_valid_basic, sent_at: DateTime.now)
      expect(invite.admin?).to be_falsey
      expect(invite.email).not_to be==email
      put :update, params: {memorial_id: memorial_valid_basic.id, id: invite.id, admin: true}
      invite.reload
      expect(invite.admin?).to be_truthy
    end

    it 'try to update an existed invite by a not authorized user' do
      memorial = create(:memorial_memorial, :minimal_valid, user: create(:security_user, :common_user))
      invite = create(:security_invite, memorial: memorial, sender: any_user, sent_at: DateTime.now)
      expect(invite.admin?).to be_falsey
      put :update, params: {memorial_id: memorial.id, id: invite.id, admin: true}
      expect(response.status).to eq 302
      invite.reload
      expect(invite.admin?).to be_falsey
    end

  end
end
