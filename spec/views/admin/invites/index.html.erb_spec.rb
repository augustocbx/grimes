# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'admin/invites/index.html.erb', type: :view do
  before(:all) do
    @user = create(:security_user, :common_user)
  end
  before(:each) do
    @memorial = create(:memorial_memorial, :minimal_valid, user: @user)
    assign(:invite, create(:security_invite, memorial: @memorial, sender: @user))
  end
  it 'Não existem convites' do
    assign(:invites, [])

    render

    expect(rendered).to match(/Não há nenhum convite para este memorial/)
  end

  it 'Existem convites registrados e não enviados' do
    invites = create_list(:security_invite, 2, sender: @user, memorial: @memorial)
    assign(:invites, invites)

    render

    # expect(rendered).to match(invites.first.email)
    expect(rendered).to match(invites.first.email)
    expect(rendered).to match(invites.last.email)
    expect(rendered).to match(/Convite a ser enviado por/)
  end

  it 'Existem convites registrados e enviados' do
    invites = create_list(:security_invite, 2, sender: @user, memorial: @memorial, sent_at: DateTime.now - 1.hour - 35.second)
    assign(:invites, invites)

    render
    expect(rendered).to match(invites.first.email)
    expect(rendered).to match(invites.last.email)
    expect(rendered).to match(/aproximadamente 1 hora/)
  end
end
