require "rails_helper"
require 'shared/user_shared'

RSpec.describe Memorial::MemorialsHelper, type: :helper do
  describe "#menu_link_to" do
    it "Check the creation of a simple menu link" do
      expect(helper.menu_link_to('My title menu', 'http://link.to.me')).to have_selector("a[href='http://link.to.me']")
    end
  end
  describe "#memorial_added?" do
    include_context 'user_shared'
    it "check when the current user has a memorial" do
      allow(view).to receive(:current_user) { logged_user }
      @memorial = create(:memorial_memorial, :minimal_valid, user: logged_user)
      # memorial.security_memorial_users << Security::MemorialUser.new(user_id: logged_user.id)
      expect(helper.memorial_added?).to be_truthy
    end
  end
  describe "#memorial_user_id" do
    include_context 'user_shared'
    it "check when the current user has a memorial" do
      allow(view).to receive(:current_user) { logged_user }
      @memorial = create(:memorial_memorial, :minimal_valid, user: logged_user)
      @memorial.security_memorial_users << Security::MemorialUser.new(user_id: logged_user.id)
      expect(helper.memorial_user_id).to be == @memorial.security_memorial_users.first.id
    end
  end
end