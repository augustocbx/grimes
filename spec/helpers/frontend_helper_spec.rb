require "rails_helper"

RSpec.describe FrontendHelper, type: :helper do
  describe "#menu_link_to" do
    it "Check the creation of a simple menu link" do
      expect(helper.menu_link_to('My title menu', 'http://link.to.me')).to have_selector("a[href='http://link.to.me']")
    end
  end
  describe "#menu_side_link_to" do
    it "Check the creation of a simple menu side link" do
      expect(helper.menu_side_link_to('My title menu', 'save', 'http://link.to.me')).to have_selector("a[href='http://link.to.me']")
    end
  end
  describe "#submenu_side_link_to" do
    it "Check the creation of a simple menu side link" do
      expect(helper.submenu_side_link_to('My title menu', 'http://link.to.me')).to have_selector("a[href='http://link.to.me']")
    end
  end
end