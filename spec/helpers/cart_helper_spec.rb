require "rails_helper"
require 'shared/user_shared'
require 'shared/memorial_shared'

RSpec.describe CartHelper, type: :helper do
  describe "my #current_cart" do
    include_context 'user_shared'
    # before(:each) do
    #   allow(view).to receive(:current_user) { logged_user }
    # end

    it "With a current_cart and without logged_user" do
      cart = create(:financial_cart, :logged_current)
      allow(view).to receive(:session) { {cart_id: cart.id} }
      expect(helper.current_cart).to be_an_instance_of(Financial::Cart)
    end
  end
  describe "#current_memorial" do
    include_context 'memorial_shared'


    it "create and return Security::Cart when is empty without logged_user" do
      allow(view).to receive(:session) { {memorial_id: memorial_valid_basic.id} }
      expect(helper.current_memorial).to be_an_instance_of(Memorial::Memorial)
    end
  end
end