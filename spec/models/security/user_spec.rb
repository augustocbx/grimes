# frozen_string_literal: true
# == Schema Information
#
# Table name: security_users
#
#  id                     :uuid             not null, primary key
#  username               :string
#  name                   :string
#  about                  :text
#  location               :string
#  image                  :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  superuser              :boolean          default(FALSE), not null
#
# Indexes
#
#  index_security_users_on_email                 (email) UNIQUE
#  index_security_users_on_reset_password_token  (reset_password_token) UNIQUE
#

require 'rails_helper'
require 'shared/user_shared'
require 'shared/memorial_shared'
require 'shared/memorial_plan_shared'
require 'support/omniauth_macros'

RSpec.describe Security::User, type: :model do
  context 'Creating a user' do
    it 'Creating a user from google' do
      expect(build(:security_user, :google_sign_up)).to be_valid
    end
    it 'Creating a user without name' do
      username = Faker::Internet.user_name
      email = Faker::Internet.email
      user = build(:security_user, :google_sign_up, name: nil, username: username, email: email)
      expect(user.title).to be == username
    end
    it 'Creating a user without name and username' do
      email = Faker::Internet.email
      user = build(:security_user, :google_sign_up, name: nil, username: nil, email: email)
      expect(user.title).to be == email
    end
  end
  context 'Omniauth' do
    include_context  'user_shared'
    it 'Creating a user from google' do
      mock_auth_hash
      user = Security::User.from_omniauth(OmniAuth.config.mock_auth[:google_oauth2])
      expect(user.title).not_to be_blank
    end
    it 'Creating a user from twitter' do
      mock_auth_hash
      user = Security::User.from_omniauth(OmniAuth.config.mock_auth[:twitter])
      expect(user.title).not_to be_blank
    end
    it 'Creating a user from facebook' do
      mock_auth_hash
      user = Security::User.from_omniauth(OmniAuth.config.mock_auth[:facebook])
      expect(user.title).not_to be_blank
    end
  end
  context 'Creating a cart from a user' do
    include_context  'user_shared'
    include_context  'memorial_shared'
    include_context  'memorial_plan_shared'
    it 'creating a cart with free plan and memorial' do
      logged_user.add_item_cart(memorial_plan_free, 1, memorial_valid_basic)
    end
  end
end
