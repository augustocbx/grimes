# frozen_string_literal: true
# == Schema Information
#
# Table name: security_invites
#
#  id          :uuid             not null, primary key
#  sender_id   :uuid
#  memorial_id :uuid
#  email       :string           not null
#  token       :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  sent_at     :datetime
#  accepted_at :datetime
#  rejected_at :datetime
#  deleted_at  :datetime
#  admin       :boolean          default(FALSE), not null
#
# Indexes
#
#  index_security_invites_on_deleted_at   (deleted_at)
#  index_security_invites_on_email        (email)
#  index_security_invites_on_memorial_id  (memorial_id)
#  index_security_invites_on_sender_id    (sender_id)
#  index_security_invites_on_token        (token)
#

require 'rails_helper'
require 'shared/user_shared'
require 'shared/memorial_shared'

RSpec.describe Security::Invite, type: :model do
  context 'Creating a invite' do
    include_context 'memorial_shared'
    it 'Creating a valid invite' do
      expect(build(:security_invite, memorial: memorial_valid_basic)).to be_valid
    end
    it 'Creating a invalid invite' do
      expect(build(:security_invite, email: nil)).to be_invalid
    end
  end

  context 'Adding an user to memorial by invite token' do
    include_context 'user_shared'
    include_context 'memorial_shared'
    it 'adding an user by a valid token' do
      invite = create(:security_invite, email: any_user.email, memorial: memorial_valid_basic)
      # expect(invite.error_message_for_create).to be_empty
      expect(memorial_valid_basic.users.empty?).to be_truthy
      expect(any_user.memorials.empty?).to be_truthy
      expect(memorial_valid_basic.email_added?(any_user.email)).to be_falsey

      any_user.add_to_memorial_by_token(invite.token)
      memorial_valid_basic.reload
      expect(memorial_valid_basic.users).to include(any_user)
      expect(any_user.memorials).to include(memorial_valid_basic)
      expect(memorial_valid_basic.email_added?(any_user.email)).to be_truthy


    end
  end
end
