# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_carts
#
#  id                :uuid             not null, primary key
#  security_user_id  :uuid
#  session_id        :uuid
#  cnnl_cart_current :boolean          default(TRUE), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'rails_helper'
require 'shared/user_shared'
require 'shared/memorial_plan_shared'

RSpec.describe Financial::Cart, type: :model do
  context 'Checkout a cart with plans' do
    include_context 'user_shared'
    include_context 'memorial_plan_shared'
    it 'Checkout a empty cart' do
      expect(logged_user.checkout).to be_falsey
      # expect(logged_user.current_cart.cnnl_cart_current?).to be_truthy
      expect { logged_user.current_cart.checkout! }.to raise_error(Exception)
    end
    it 'Buy a free plan' do
      logged_user.current_cart.add_item_cart(memorial_plan_free)
      expect(logged_user.current_cart.financial_cart_items.first.free?).to be_truthy
      expect(logged_user.current_cart.financial_cart_items.first.title).to include('Free plan')
      expect(logged_user.current_cart.financial_cart_items.first.description).to include('This is a free plan')
      expect(logged_user.current_cart.financial_cart_items.first.discount).to be_equal(0.00)
      expect(logged_user.current_cart.financial_cart_items.first.quantity).to be_equal(1)
      expect(logged_user.current_cart.financial_cart_items.first.final_price).to be_equal(0.00)
      expect(logged_user.current_cart.financial_cart_items.first.price).to be_equal(0.00)
      expect(logged_user.checkout).to be_truthy
      expect(logged_user.current_cart.total).to eq(0.00)
    end

    it 'Buy a free plan twice' do
      plan_free = memorial_plan_free
      logged_user.current_cart.add_item_cart(plan_free, 2)
      expect(logged_user.current_cart.financial_cart_items.first.quantity).to be_equal(2)
      # p logged_user.current_cart.financial_cart_items.first.check_basic_rules
      expect(logged_user.current_cart.financial_cart_items.first.errors.full_messages.to_s).to include("plan can't be greater then 1")
    end

    it 'Buy or update a plan with invalid quantity' do
      expect { logged_user.current_cart.add_item_cart(memorial_plan_free, 0) }.to raise_error(Exception)
      expect { logged_user.current_cart.update_item_cart(memorial_plan_free, -1) }.to raise_error(Exception)
    end
    it 'update a plan with quantity 0 should remove a item' do
      logged_user.current_cart.add_item_cart(memorial_plan_free)
      logged_user.current_cart.save
      expect(logged_user.current_cart.items_count).to eq(1)
      logged_user.current_cart.update_item_cart(memorial_plan_free, 0)
      expect(logged_user.current_cart.items_count).to eq(0)
    end
    it 'Buy a payed plan' do
      logged_user.current_cart.add_item_cart(memorial_plan_1_year)
      expect(logged_user.checkout).to be_truthy
      expect(logged_user.current_cart.cnnl_cart_current?).to be_falsey
      expect(logged_user.current_cart.total).to eq(70.00)
      expect(logged_user.current_cart.items_count).to eq(1)
    end
    it 'Buying an amount between 50 and 100 payed plans' do
      quantity = rand(50..100)
      create(:memorial_memorial_plan_discount_range, :over_20_discount_of_25, memorial_plan: memorial_plan_1_year)
      logged_user.current_cart.add_item_cart(memorial_plan_1_year, quantity)
      expect(logged_user.checkout).to be_truthy
      expect(logged_user.current_cart.cnnl_cart_current?).to be_falsey
      expect(logged_user.current_cart.total).to eq((70.00 - 25.0) * quantity)
    end
  end
  context 'Checking invalid carts' do
    include_context 'user_shared'
    include_context 'memorial_plan_shared'
    it 'generating order from a cart with invalid item' do
      quantity = rand(50..100)
      logged_user.current_cart.add_item_cart(memorial_plan_1_year, quantity)
      logged_user.current_cart.financial_cart_items.map do |item|
        item.fnnl_cart_item_discount = -1.11
        item
      end
      expect { logged_user.current_cart.generate_order }.to raise_error(Exception)
    end
  end
end
