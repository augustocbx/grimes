# == Schema Information
#
# Table name: financial_order_items
#
#  id                              :uuid             not null, primary key
#  financial_order_id              :uuid             not null
#  memorial_plan_discount_range_id :uuid
#  fnnl_ordr_item_source_type      :string           not null
#  fnnl_ordr_item_source_id        :uuid             not null
#  fnnl_ordr_item_receiver_type    :string
#  fnnl_ordr_item_receiver_id      :uuid
#  fnnl_ordr_item_quantity         :integer          default(0), not null
#  fnnl_ordr_item_unity_price      :float            default(0.0), not null
#  fnnl_ordr_item_discount         :float            default(0.0), not null
#  fnnl_ordr_item_total            :float            default(0.0), not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

require 'rails_helper'
require 'shared/user_shared'
require 'shared/memorial_plan_shared'
require 'shared/memorial_shared'

RSpec.describe Financial::OrderItem, type: :model do
  context 'Check simple item' do
    include_context 'user_shared'
    include_context 'memorial_shared'
    include_context 'memorial_plan_shared'
    it 'Simple order item' do
      logged_user.current_cart.add_item_cart(memorial_plan_1_year, 5, memorial_valid_basic)
      order = logged_user.current_cart.generate_order

      item = order.financial_order_items.first
      expect(item.discount).to be == 0.00
      expect(item.item_price).to be == memorial_plan_1_year.price
      expect(item.subtotal).to be == (memorial_plan_1_year.price * 5)
      expect(item.final_price).to be == (memorial_plan_1_year.price)
      expect(item.quantity).to be == (5)
      expect(item.free?).to be_falsey
      expect(item.title).to be == memorial_plan_1_year.title
      expect(item.description).to be == memorial_plan_1_year.description
    end
  end
end
