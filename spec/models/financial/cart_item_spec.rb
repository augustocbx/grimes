# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_cart_items
#
#  id                              :uuid             not null, primary key
#  financial_cart_id               :uuid             not null
#  memorial_plan_discount_range_id :uuid
#  fnnl_cart_item_source_type      :string           not null
#  fnnl_cart_item_source_id        :uuid             not null
#  fnnl_cart_item_receiver_type    :string
#  fnnl_cart_item_receiver_id      :uuid
#  fnnl_cart_item_quantity         :integer          not null
#  fnnl_cart_item_unity_price      :float            not null
#  fnnl_cart_item_discount         :float            not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

require 'rails_helper'
require 'shared/user_shared'
require 'shared/memorial_shared'
require 'shared/memorial_plan_shared'

RSpec.describe Financial::CartItem, type: :model do
  context 'Add a valid free plans' do
    include_context 'user_shared'
    include_context 'memorial_plan_shared'
    include_context 'memorial_shared'
    it 'Add a new free plan in the current cart' do
      expect(logged_user.current_cart.add_item_cart(memorial_plan_free)).to be_truthy
      expect(logged_user.current_cart.itens_count).to be(1)
      expect(logged_user.current_cart.subtotal).to eq(0.00)
      expect(logged_user.current_cart.shipping_cost).to eq(0.00)
      expect(logged_user.current_cart.taxes).to eq(0.00)
      expect(logged_user.current_cart.total).to eq(0.00)
    end
    it 'Add a new free plan in the current cart with a memorial' do
      expect(logged_user.current_cart.add_item_cart(memorial_plan_free, 1, memorial_valid_basic)).to be_truthy
      expect(logged_user.current_cart.itens_count).to be(1)
      expect(logged_user.current_cart.subtotal).to eq(0.00)
      expect(logged_user.current_cart.shipping_cost).to eq(0.00)
      expect(logged_user.current_cart.taxes).to eq(0.00)
      expect(logged_user.current_cart.total).to eq(0.00)
      expect(logged_user.current_cart.financial_cart_items.first.fnnl_cart_item_receiver).to be_an_instance_of(Memorial::Memorial)
    end
    it 'Add and clean a new free plan in the current cart' do
      expect(logged_user.current_cart.add_item_cart(memorial_plan_free)).to be_truthy
      expect(logged_user.current_cart.itens_count).to be(1)
      logged_user.current_cart.clear
      expect(logged_user.current_cart.itens_count).to be(0)
      expect(logged_user.current_cart.empty?).to be_truthy
    end
  end

  context 'Add a valid payed plan' do
    include_context 'user_shared'
    include_context 'memorial_plan_shared'
    it 'Add a new plan in the current cart' do
      expect(logged_user.current_cart.add_item_cart(memorial_plan_1_year)).to be_truthy # Is costs R$ 70,00
      expect(logged_user.current_cart.itens_count).to be(1)
      expect(logged_user.current_cart.subtotal).to eq(70.00)
      expect(logged_user.current_cart.shipping_cost).to eq(0.00)
      expect(logged_user.current_cart.taxes).to eq(0.00)
      expect(logged_user.current_cart.total).to eq(70.00)

      logged_user.current_cart.financial_cart_items.each do |cart_item|
        expect(cart_item.subtotal).to eq(70.00)
        expect(cart_item.final_price).to eq(70.00)
        expect(cart_item.quantity).to be(1)
        expect(cart_item.price).to eq(70.00)
        expect(cart_item.discount).to eq(0.00)
      end
    end
    it 'Add and clean a plan in the current cart' do
      expect(logged_user.current_cart.add_item_cart(memorial_plan_3_years)).to be_truthy
      expect(logged_user.current_cart.itens_count).to be(1)
      logged_user.current_cart.clear
      expect(logged_user.current_cart.itens_count).to be(0)
      expect(logged_user.current_cart.empty?).to be_truthy
      expect(logged_user.current_cart.total).to eq(0.00)
    end
  end

  context 'Add an invalid free plans' do
    include_context 'user_shared'
    include_context 'memorial_plan_shared'
    it 'Add a new free plan in the current cart with 0 of quantity should raise a error' do
      expect { logged_user.current_cart.add_item_cart(memorial_plan_free, 0) }.to raise_error(Exception)
      expect(logged_user.current_cart.itens_count).to be(0)
    end
    it 'Add a new free plan in the current cart with 2..9 of quantity should raise a error' do
      logged_user.current_cart.add_item_cart(memorial_plan_free, rand(2..9))
      expect(logged_user.current_cart.valid?).to be_falsey
      expect(logged_user.current_cart.itens_count).to be(0)
    end
    it 'Add a new cart_item in the current cart should raise a error' do
      cart_item = Financial::CartItem.new(fnnl_cart_item_source: memorial_plan_free, fnnl_cart_item_quantity: 1, fnnl_cart_item_unity_price: 0.00)
      expect { logged_user.current_cart.add_item_cart(cart_item) }.to raise_error(Exception)
    end
    it 'Add a null plan in the current cart should raise a error' do
      expect { logged_user.current_cart.add_item_cart(nil) }.to raise_error(Exception)
    end
    it 'Add two equal free plans should not be valid' do
      logged_user.current_cart.add_item_cart(memorial_plan_free)
      expect(logged_user.current_cart.valid?).to be_truthy
      logged_user.current_cart.add_item_cart(memorial_plan_free)
      expect(logged_user.current_cart.valid?).to be_falsey
    end
    it 'Add two different plans in a cart should not be valid' do
      logged_user.current_cart.add_item_cart(memorial_plan_free)
      expect(logged_user.current_cart.valid?).to be_truthy
      logged_user.current_cart.add_item_cart(memorial_plan_3_years, 10)
      expect(logged_user.current_cart.valid?).to be_truthy
      expect(logged_user.current_cart.itens_count).to eq(2)
      expect(logged_user.current_cart.subtotal).to eq(1500.00)
      expect(logged_user.current_cart.total).to eq(1500.00)
    end
  end
  context 'Remove cart item ' do
    include_context 'user_shared'
    include_context 'memorial_plan_shared'
    it 'Add a new free plan in the current cart' do
      expect(logged_user.current_cart.add_item_cart(memorial_plan_free)).to be_truthy
      expect(logged_user.current_cart.itens_count).to be(1)
      expect(logged_user.current_cart.remove_item_cart(memorial_plan_free)).to be_truthy
      expect(logged_user.current_cart.itens_count).to be(0)
      expect(logged_user.current_cart.empty?).to be_truthy
    end
  end
end
