# frozen_string_literal: true
# == Schema Information
#
# Table name: financial_orders
#
#  id                :uuid             not null, primary key
#  security_user_id  :uuid             not null
#  financial_cart_id :uuid             not null
#  fnnl_ordr_number  :string           not null
#  fnnl_ordr_state   :string           not null
#  fnnl_ordr_total   :float            default(0.0), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#


require 'rails_helper'
require 'shared/user_shared'
require 'shared/memorial_shared'
require 'shared/memorial_plan_shared'
require 'shared/order_shared'

RSpec.describe Financial::Order, type: :model do
  context 'Checkout a cart with plans' do
    include_context 'user_shared'
    include_context 'memorial_shared'
    include_context 'memorial_plan_shared'
    include_context 'order_shared'
    it 'Checkout a empty cart' do
      expect(logged_user.current_cart.checkout).to be_falsey
      expect(logged_user.current_cart.financial_order).to be_nil
    end
    it 'Checkout a valid free memorial_plan' do
      expect(logged_user.current_cart.add_item_cart(memorial_plan_free)).to be_truthy
      expect(logged_user.current_cart.checkout).to be_truthy
      expect(logged_user.current_cart.total).to eq(0.00)
    end
    it 'Checkout a valid free memorial_plan with a basic_memorial' do
      expect(logged_user.current_cart.add_item_cart(memorial_plan_free, 1, memorial_valid_basic)).to be_truthy
      expect(memorial_valid_basic.memorial_plan).to be_nil
      expect(logged_user.current_cart.checkout).to be_truthy
      expect(logged_user.current_cart.total).to eq(0.00)
      expect(logged_user.current_cart.financial_cart_items.first.fnnl_cart_item_receiver).to be_an_instance_of(Memorial::Memorial)
      expect(logged_user.current_cart.financial_order.financial_order_items.first.fnnl_ordr_item_receiver).to be_an_instance_of(Memorial::Memorial)
      expect(logged_user.current_cart.financial_order.state_description).to eq('Aproved')
      expect(logged_user.current_cart.financial_order.memorial_memorial_plan_coupons.size).to eq(1)
      memorial_valid_basic.reload
      expect(memorial_valid_basic.current_memorial_plan).to eq(memorial_plan_free)
    end
    it 'Checkout a paid memorial_plan and reprove the order' do
      expect(logged_user.current_cart.add_item_cart(memorial_plan_3_years)).to be_truthy
      expect(logged_user.current_cart.checkout).to be_truthy
      expect(logged_user.current_cart.total).to eq(150.00)
      expect(logged_user.current_cart.financial_order.state_description).to eq('New')
      logged_user.current_cart.financial_order.reprove!
      expect(logged_user.current_cart.financial_order.reproved?).to be_truthy
      expect(logged_user.current_cart.financial_order.approved?).to be_falsey
      expect(logged_user.current_cart.financial_order.state_description).to eq('Reproved')
      logged_user.current_cart.financial_order.update_attribute(:fnnl_ordr_state, 'Aprovado')
      expect { logged_user.current_cart.financial_order.state_description }.to raise_error(Exception)
    end

    it 'Approve an order with the status changed manually' do
      expect(order_new.state_description).to be=='New'
      order_new.fnnl_ordr_state = 'A'
      expect {order_new.approve!}.to raise_error('Verificar o comportamento nesta linha')
    end
  end

  context 'Cart without plans' do
    include_context 'order_shared'
    it 'Try to approve a empty order' do
      expect(order_new.state_description).to be=='New'
      order_new.approve!
      expect(order_new.state_description).to be=='Cancelled'
    end
  end
  context 'Another cases' do
    include_context 'order_shared'
    it 'Order cancelled' do
      expect(order_new.state_description).to be=='New'
      expect(order_new.itens_count).to be==0
      order_new.cancel!
      expect(order_new.state_description).to be=='Cancelled'
    end
    it 'Order processing' do
      expect(order_new.state_description).to be=='New'
      order_new.fnnl_ordr_state = 'P'
      expect(order_new.state_description).to be=='Processing'
    end
  end
end
