# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorials
#
#  id                  :uuid             not null, primary key
#  security_user_id    :uuid             not null
#  mmry_firstname      :string           not null
#  mmry_lastname       :string           not null
#  mmry_nicknames      :string
#  mmry_gender         :string
#  mmry_biografy       :text
#  mmry_birth_date     :date             not null
#  mmry_birth_location :string           not null
#  mmry_death_date     :date             not null
#  mmry_death_location :string           not null
#  mmry_cause_of_death :string
#  mmry_tombstone_geo  :string
#  mmry_privacy        :integer
#  photo               :string
#  enabled             :boolean          default(TRUE), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  deleted_at          :datetime
#  mmry_visits         :integer          default(0), not null
#
# Indexes
#
#  index_memorial_memorials_on_deleted_at  (deleted_at)
#

require 'rails_helper'
require 'shared/user_shared'

RSpec.describe Memorial::Memorial, type: :model do
  include_context 'user_shared'
  context 'Creating a valid memorial' do
    it 'Creating a new memorial with all filled felds' do
      expect(build(:memorial_memorial, :full)).to be_valid
    end
    it 'Creating a new memorial with minimal filled fields' do
      expect(build(:memorial_memorial, :minimal_valid)).to be_valid
    end
    it 'Creating a valid memorial with picture' do
      memorial = build(:memorial_memorial, :minimal_valid)
      memorial.memorial_photos.build attributes_for(:memorial_memorial_photo, :full)
      expect(memorial).to be_valid
    end
    it 'Check default memorial image' do
      memorial = build(:memorial_memorial, :minimal_valid)
      expect(memorial.main_photo_thumb_url).to match(/no_image.*\.png/)
    end
    it 'Check social thumb_url' do
      memorial = build(:memorial_memorial, :minimal_valid)
      expect(memorial.social_thumb_url).to match(/no_image.*\.png/)
    end
    it 'Check social description' do
      memorial = build(:memorial_memorial, :full)
      expect(memorial.social_description).to include(memorial.mmry_biografy)
    end
    it 'Check if it is ready to publish' do
      memorial = build(:memorial_memorial, :minimal_valid, mmry_biografy: 'fajdsfklja dslkfj adlsfj lka')
      expect(memorial.ready_to_publish?).to be_truthy
    end
    it 'Check if it is not ready to publish' do
      memorial = build(:memorial_memorial, :minimal_valid, mmry_biografy: '')
      expect(memorial.ready_to_publish?).to be_falsey
    end
    it 'Check the class method search' do
      memorial = create(:memorial_memorial, :full, user: logged_user)
      expect(Memorial::Memorial.search(any_user, memorial.mmry_firstname)).to be_empty
      expect(Memorial::Memorial.search(logged_user, memorial.mmry_firstname)).to include(memorial)
    end
  end

  context 'Creating a invalid memorial' do
    it 'Creating a new memorial without filled fields' do
      required_fields = [:mmry_firstname, :mmry_lastname, :mmry_birth_date, :mmry_birth_location, :mmry_death_date, :mmry_death_location]
      required_fields.each do |field|
        memorial = build(:memorial_memorial, :full)
        memorial[field] = ''
        expect(memorial).to be_invalid
      end
    end
  end
end
