# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorial_photos
#
#  id                    :uuid             not null, primary key
#  memorial_id           :uuid             not null
#  mmry_phto_title       :string
#  mmry_phto_description :string
#  image                 :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

require 'rails_helper'
require 'shared/user_shared'
require 'shared/memorial_shared'

RSpec.describe Memorial::MemorialPhoto, type: :model do
  context 'Adding a photo to a memorial' do
    include_context 'memorial_shared'
    include_context 'user_shared'
    it 'Insert a invalid profile picture' do
      expect(memorial_valid_basic).to be_valid
      memorial_valid_basic.memorial_photos << build(:memorial_memorial_photo)
      expect(memorial_valid_basic).to be_invalid
    end
    it 'Insert a valid profile picture' do
      expect(memorial_valid_basic).to be_valid
      memorial_valid_basic.memorial_photos << build(:memorial_memorial_photo, :full)
      expect(memorial_valid_basic).to be_valid
    end
  end
end
