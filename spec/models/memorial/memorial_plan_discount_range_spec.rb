# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorial_plan_discount_ranges
#
#  id                           :uuid             not null, primary key
#  memorial_plan_id             :uuid             not null
#  mmrl_plan_dsct_rnge_status   :boolean          not null
#  mmrl_plan_dsct_rnge_min      :integer          not null
#  mmrl_plan_dsct_rnge_discount :float            not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#

require 'rails_helper'
require 'shared/memorial_plan_shared'

RSpec.describe Memorial::MemorialPlanDiscountRange, type: :model do
  context 'Creating invalid discount ranges' do
    include_context 'memorial_plan_shared'

    it 'Creating a min existent discount min' do
      memorial_plan_3_years.memorial_memorial_plan_discount_ranges << build(:memorial_memorial_plan_discount_range, :between_3_to_10_discount_of_15, mmrl_plan_dsct_rnge_min: 10, mmrl_plan_dsct_rnge_discount: 10)
      memorial_plan_3_years.memorial_memorial_plan_discount_ranges << build(:memorial_memorial_plan_discount_range, :between_3_to_10_discount_of_15, mmrl_plan_dsct_rnge_min: 10, mmrl_plan_dsct_rnge_discount: 20)
      expect(memorial_plan_3_years).to be_invalid
    end

    it 'Creating a negative discount' do
      memorial_plan_3_years.memorial_memorial_plan_discount_ranges << build(:memorial_memorial_plan_discount_range, :between_3_to_10_discount_of_15, mmrl_plan_dsct_rnge_min: 10, mmrl_plan_dsct_rnge_discount: -10)
      expect(memorial_plan_3_years).to be_invalid
    end

    it 'Creating a smaller discount for bigger min' do
      memorial_plan_3_years.memorial_memorial_plan_discount_ranges << build(:memorial_memorial_plan_discount_range, :between_3_to_10_discount_of_15, mmrl_plan_dsct_rnge_min: 10, mmrl_plan_dsct_rnge_discount: 20)
      memorial_plan_3_years.memorial_memorial_plan_discount_ranges << build(:memorial_memorial_plan_discount_range, :between_3_to_10_discount_of_15, mmrl_plan_dsct_rnge_min: 11, mmrl_plan_dsct_rnge_discount: 19)
      expect(memorial_plan_3_years).to be_invalid
    end

    it 'Creating a negative min' do
      memorial_plan_3_years.memorial_memorial_plan_discount_ranges << build(:memorial_memorial_plan_discount_range, :between_3_to_10_discount_of_15, mmrl_plan_dsct_rnge_min: -10, mmrl_plan_dsct_rnge_discount: 20)
      expect(memorial_plan_3_years).to be_invalid
    end
  end

  context 'Creating valid discount ranges' do
    include_context 'memorial_plan_shared'
    it 'Creating a min existent but disabled discount min' do
      memorial_plan_3_years.memorial_memorial_plan_discount_ranges << build(:memorial_memorial_plan_discount_range, :between_3_to_10_discount_of_15, mmrl_plan_dsct_rnge_min: 10, mmrl_plan_dsct_rnge_discount: 10, mmrl_plan_dsct_rnge_status: false)
      memorial_plan_3_years.memorial_memorial_plan_discount_ranges << build(:memorial_memorial_plan_discount_range, :between_3_to_10_discount_of_15, mmrl_plan_dsct_rnge_min: 10, mmrl_plan_dsct_rnge_discount: 20)
      expect(memorial_plan_3_years).to be_valid
    end
  end
end
