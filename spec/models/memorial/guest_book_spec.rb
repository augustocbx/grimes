# == Schema Information
#
# Table name: memorial_guest_books
#
#  memorial_id        :uuid             not null
#  user_id            :uuid             not null
#  guest_book_message :text
#  deleted_at         :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  id                 :uuid             not null, primary key
#

require 'rails_helper'
require 'shared/user_shared'
require 'shared/memorial_shared'

RSpec.describe Memorial::GuestBook, type: :model do
  context 'Adding to a guest_book' do
    include_context 'user_shared'
    include_context 'memorial_shared'

    it 'Valid guest_book' do
      expect(build(:guest_book, user: logged_user, memorial: memorial_valid_basic)).to be_valid
    end
    it 'Invalid guest_book without user and memorial' do
      guest_book = build(:guest_book)
      expect(guest_book).to be_invalid
      expect(guest_book.error_messages).to include("não pode ficar em branco")

    end
    it 'Valid guest_book twice without messages' do
      create(:guest_book, user: logged_user, memorial: memorial_valid_basic, guest_book_message: Faker::Lorem.paragraph(3))
      guest_book = build(:guest_book, user: logged_user, memorial: memorial_valid_basic, guest_book_message: Faker::Lorem.paragraph(3))
      expect(guest_book).to be_valid
    end
    it 'Invalid guest_book twice without messages' do
      create(:guest_book, user: logged_user, memorial: memorial_valid_basic, guest_book_message: nil)
      guest_book = build(:guest_book, user: logged_user, memorial: memorial_valid_basic, guest_book_message: nil)
      expect(guest_book).to be_invalid
      expect(guest_book.error_messages).to include("Você só pode assinar o livro de visitas uma vez por dia. Deixe uma mensagem.")
    end
  end
end
