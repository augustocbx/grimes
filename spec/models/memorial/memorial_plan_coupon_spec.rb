# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorial_plan_coupons
#
#  id                                :uuid             not null, primary key
#  security_user_id                  :uuid             not null
#  financial_order_id                :uuid
#  memorial_plan_id                  :uuid             not null
#  memorial_id                       :uuid
#  mmrl_plan_cpon_used               :boolean          not null
#  mmrl_plan_cpon_used_date          :date
#  mmrl_plan_cpon_active             :boolean          not null
#  mmrl_plan_cpon_active_date        :date
#  mmrl_plan_cpon_vigence_start_date :date
#  mmrl_plan_cpon_vigence_end_date   :date
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#

require 'rails_helper'
require 'shared/user_shared'
require 'shared/memorial_shared'
require 'shared/memorial_plan_shared'
require 'shared/order_shared'

RSpec.describe Memorial::MemorialPlanCoupon, type: :model do
  context 'Checking the creation of the Memorial::MemorialPlanCoupon' do
    include_context 'user_shared'
    include_context 'memorial_shared'
    include_context 'memorial_plan_shared'
    include_context 'order_shared'
    it 'Creating a Memorial::MemorialPlanCoupon without memorial' do
      memorial_plan_coupon = Memorial::MemorialPlanCoupon.create(security_user: logged_user, financial_order: order_approved, memorial_plan: memorial_plan_free)
      memorial_plan_coupon.set_default_activated_values!
      expect(memorial_plan_coupon).to be_valid
      expect(memorial_plan_coupon.memorial).to be_nil
      expect(memorial_plan_coupon.mmrl_plan_cpon_used?).to be_falsey
      expect(memorial_plan_coupon.mmrl_plan_cpon_used_date).to be_nil
      memorial_plan_coupon.apply_to!(memorial_valid_basic)
    end
    it 'Creating a Memorial::MemorialPlanCoupon (free) with memorial' do
      memorial_plan_coupon = Memorial::MemorialPlanCoupon.create(security_user: logged_user, financial_order: order_approved, memorial: memorial_valid_basic, memorial_plan: memorial_plan_free)
      memorial_plan_coupon.set_default_activated_values!
      expect(memorial_plan_coupon).to be_valid
      expect(memorial_plan_coupon.memorial).to be_a(Memorial::Memorial)
      expect(memorial_plan_coupon.mmrl_plan_cpon_used?).to be_falsey
      expect(memorial_plan_coupon.mmrl_plan_cpon_used_date).to be_nil
      memorial_plan_coupon.apply_to!(memorial_valid_basic)
    end
    it 'Creating a Memorial::MemorialPlanCoupon with memorial' do
      memorial_plan_coupon = Memorial::MemorialPlanCoupon.create(security_user: logged_user, financial_order: order_approved, memorial: memorial_valid_basic, memorial_plan: memorial_plan_1_year)
      memorial_plan_coupon.set_default_activated_values!
      expect(memorial_plan_coupon).to be_valid
      expect(memorial_plan_coupon.memorial).to be_a(Memorial::Memorial)
      expect(memorial_plan_coupon.mmrl_plan_cpon_used?).to be_falsey
      expect(memorial_plan_coupon.mmrl_plan_cpon_used_date).to be_nil
      memorial_plan_coupon.apply_to!(memorial_valid_basic)
    end
  end
end
