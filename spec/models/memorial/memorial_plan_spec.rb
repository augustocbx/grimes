# frozen_string_literal: true
# == Schema Information
#
# Table name: memorial_memorial_plans
#
#  id                           :uuid             not null, primary key
#  mmrl_plan_free_default       :boolean          default(FALSE), not null
#  mmrl_plan_title              :string           not null
#  mmrl_plan_description        :text             not null
#  mmrl_plan_validade_days      :integer          not null
#  mmrl_plan_activation_days    :integer          not null
#  mmrl_plan_videos_limit       :integer          not null
#  mmrl_plan_photos_limit       :integer          not null
#  mmrl_plan_support            :boolean          default(FALSE), not null
#  mmrl_plan_price              :float            not null
#  mmrl_plan_active             :boolean          not null
#  mmrl_plan_vigence_start_date :date             not null
#  mmrl_plan_vigence_end_date   :date
#  mmrl_plan_color              :string           default("green"), not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#

require 'rails_helper'
require 'shared/memorial_plan_shared'

RSpec.describe Memorial::MemorialPlan, type: :model do
  context 'Creating valid plans' do
    include_context 'memorial_plan_shared'
    it 'Creating a new plan with all filled fields' do
      expect(build(:memorial_memorial_plan, :memorial_plan_full)).to be_valid
      memorial_plan = create(:memorial_memorial_plan, :memorial_plan_full, mmrl_plan_price: 10.0)
      expect(memorial_plan.price).to be == 10.0
    end

    it 'Creating a new plan with unlimited videos and photos' do
      memorial_plan = build(:memorial_memorial_plan, :memorial_plan_full, mmrl_plan_videos_limit: -1, mmrl_plan_photos_limit: -1)
      expect(memorial_plan.mmrl_plan_videos_limit_str).to be == 'Unlimited'
      expect(memorial_plan.mmrl_plan_photos_limit_str).to be == 'Unlimited'
    end

    it 'Creating a new plan with limited videos and photos' do
      memorial_plan = build(:memorial_memorial_plan, :memorial_plan_full, mmrl_plan_videos_limit: 8, mmrl_plan_photos_limit: 5)
      expect(memorial_plan.mmrl_plan_videos_limit_str).to be == 8
      expect(memorial_plan.mmrl_plan_photos_limit_str).to be == 5
    end

    it 'Creating a new plan with discount' do
      expect(memorial_plan_full.discount?).to be_falsey
      create(:memorial_memorial_plan_discount_range, memorial_plan: memorial_plan_full, mmrl_plan_dsct_rnge_min: 1, mmrl_plan_dsct_rnge_discount: 15.0, )
      expect(memorial_plan_full).to be_valid
      expect(memorial_plan_full.discount?).to be_truthy

    end

    it 'Checking the existent of a free plan' do
      expect(Memorial::MemorialPlan.free_plan).to be_nil
      memorial_plan_free
      expect(Memorial::MemorialPlan.free_plan).to be_an_instance_of(Memorial::MemorialPlan)
    end
  end

  context 'Creating invalid plans' do
    it 'Creating a new plan just with title field' do
      expect(build(:memorial_memorial_plan, :just_title)).to be_invalid
    end
    it 'Creating a new plan with vigence date in the future' do
      memorial_plan = build(:memorial_memorial_plan, :memorial_plan_full, mmrl_plan_vigence_start_date: DateTime.now + 1.day)
      expect(memorial_plan.actived?).to be_falsey
      expect { memorial_plan.disable! }.to raise_error(Exception)
    end
  end

  context 'Changing a plan' do
    include_context 'memorial_plan_shared'
    it 'Change a existent plan title, must be denied' do
      expect(memorial_plan_full.update_attribute(:mmrl_plan_title, Faker::Name.name)).to be_falsey
    end
    it 'Disable a existent plan, must be granted' do
      expect(memorial_plan_full.disable!).to be_truthy
    end
    it 'Set disabled date greater or minor then 1 second must be blocked' do
      memorial_plan = build(:memorial_memorial_plan, :memorial_plan_full, mmrl_plan_vigence_start_date: Date.today + 1.day)

      memorial_plan.mmrl_plan_vigence_end_date = Date.today - 1.day
      expect(memorial_plan.save).to be_falsey

      memorial_plan.mmrl_plan_vigence_end_date = Date.today
      expect(memorial_plan.save).to be_falsey

      memorial_plan.mmrl_plan_vigence_end_date = Date.today + 1.day
      expect(memorial_plan.save).to be_truthy

      memorial_plan = build(:memorial_memorial_plan, :memorial_plan_full, mmrl_plan_vigence_start_date: Date.today - 1.day)

      memorial_plan.mmrl_plan_vigence_end_date = Date.today - 2.day
      expect(memorial_plan.save).to be_falsey

      memorial_plan.mmrl_plan_vigence_end_date = Date.today
      expect(memorial_plan.save).to be_truthy

      memorial_plan.mmrl_plan_vigence_end_date = Date.today + 2.days
      expect(memorial_plan.save).to be_truthy
    end
    it 'Change a existent plan title, must be denied' do
      expect(memorial_plan_full.update_attribute(:mmrl_plan_title, Faker::Name.name)).to be_falsey
    end
  end

  context 'Checking active plans' do
    include_context 'memorial_plan_shared'
    it 'Start date in the past without end date' do
      create(:memorial_memorial_plan, :memorial_plan_full, mmrl_plan_vigence_start_date: Date.today - 1.year)
      expect(Memorial::MemorialPlan.active).to_not be_empty
    end
  end

  context 'Checking inactive plans' do
    include_context 'memorial_plan_shared'
    it 'Inactived by data range in the past' do
      memorial_plan = create(:memorial_memorial_plan, :memorial_plan_full, mmrl_plan_vigence_start_date: Date.today - 1.year,  mmrl_plan_vigence_end_date: Date.today - 1.month)
      expect(memorial_plan.actived?).to be_falsey
    end
  end


  context 'Memorial without any plan' do
    include_context 'memorial_plan_shared'
    it 'Defaul plan is free plan' do
      memorial_plan_free
      memorial = build(:memorial_memorial, :full)
      expect(memorial.current_memorial_plan).to be == Memorial::MemorialPlan.free_plan
    end
  end
end
