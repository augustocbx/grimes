# frozen_string_literal: true
source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.2.4.2 '
gem 'activerecord-session_store'
gem 'bootsnap'
# Use postgresql as the database for Active Record
gem 'pg'
gem 'will_paginate'
# Use SCSS for stylesheets
gem 'sass-rails'
gem 'bootstrap-sass'
gem "bootstrap-switch-rails"

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'simple_form'

# HTML Editor
gem 'bootsy'

# Logical delete
gem 'paranoia'#, :git => 'https://github.com/rubysherpas/paranoia', branch: 'rails5'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'
# Use Puma as the app server
gem 'puma'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development do
  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-bundler'
  # gem 'capistrano-ext'
  gem 'capistrano-rvm'
  # gem 'capistrano3-unicorn'
  gem 'capistrano3-puma'
  # gem 'rvm1-capistrano3', require: false
  # gem 'rvm-capistrano', '1.4.1'
  # gem 'net-ssh', '~>2.7.0'
  gem 'ed25519'
  gem 'bcrypt_pbkdf'
  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'listen'
end

group :development, :test do
  # Call 'byebug' in the code to stop execution and get a debugger console
  gem 'byebug'
end

gem 'annotate'
group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  # gem 'web-console', '~> 3.0'
  # Code analyzer
  gem 'rubocop', require: false
  gem 'rubocop-rspec', require: false
  gem 'traceroute'
end

# Bower
gem 'bower-rails'

gem 'font-awesome-sass', '~> 4.7.0'

# Authentication
gem 'devise', '~> 4'
# gem 'simple_token_authentication'
gem 'devise_token_auth'
# gem 'simple_token_authentication'
gem 'pundit'
gem 'omniauth'
gem 'omniauth-oauth2'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'google-api-client', '~> 0.37.2'
gem 'jwt'


# Pagamentos
gem 'pagseguro-oficial', '~> 2.6.0'

group :test, :development do
  gem 'rspec-rails'
  gem 'rails-controller-testing'
  gem 'ci_reporter_rspec'
  gem 'rspec_junit_formatter'
  gem 'ZenTest', '~> 4.11'
  gem 'autotest-rails'
  gem 'rspec-autotest'

  # Library to make matchers more easy => http://matchers.shoulda.io/docs/v2.8.0/
  gem 'shoulda-matchers', require: false

  gem 'factory_bot_rails'
  gem 'faker'

  gem 'cucumber-rails', require: false
  gem 'database_cleaner'

  gem 'capybara'
  gem 'selenium-webdriver'
  gem 'compass'

  gem 'simplecov', require: false
  gem "brakeman"
  # gem 'rcov'
  # gem 'spork', github: 'sporkrb/spork'

  gem 'i18n-tasks', '~> 0.9.4'
end

# Cities list around of world
gem 'geonames_dump', git: 'https://github.com/trimolesi/geonames_dump'

# Images libraries
gem 'rmagick'
gem 'carrierwave'
gem 'image_optimizer'
# gem 'piet'
# gem 'piet-binary'
# gem "mini_magick"

gem 'best_in_place', '~> 3.0.1'
gem 'dotiw'

gem 'postmark-rails'

# HEROKU LIBS
# gem 'rails_12factor', group: :production
gem 'whenever', require: false
gem 'sidekiq', require: false

# Monitoramento do app
gem 'bullet'
gem 'slack-notifier'

gem 'social-share-button'
gem 'sitemap'
gem 'exception_notification'

group :production, :production_local do
  gem 'dalli'
  gem 'htmlcompressor'
end
gem 'actionpack-action_caching'
gem 'rollbar'
gem 'html2text'
gem 'fcm'
gem 'google-cloud-firestore'
gem 'firebase_token_auth'

ruby '2.5.7'
